﻿using System.Web;
using System.Web.Optimization;

namespace SDSP
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/jqueryui").Include(
                     "~/Content/themes/base/all.css",
                     "~/Content/themes/base/selectable.css"));

            bundles.Add(new ScriptBundle("~/bundles/customingate").Include(
                        "~/Scripts/ingate.js*"));
            bundles.Add(new ScriptBundle("~/bundles/custompreoutgate").Include(
                        "~/Scripts/preoutgate.js*"));
            bundles.Add(new ScriptBundle("~/bundles/customoutgate").Include(
                        "~/Scripts/outgate.js*"));
            bundles.Add(new ScriptBundle("~/bundles/customprinthook").Include(
                        "~/Scripts/printhook.js*"));
            bundles.Add(new ScriptBundle("~/bundles/custonUpdateInvoice").Include(
                        "~/Scripts/updateInvoice.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customEditIngate").Include(
                        "~/Scripts/editIngate.js*"));

            bundles.Add(new ScriptBundle("~/bundles/preoutgatesReport").Include(
                        "~/Scripts/preoutgateReport.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customInTransitStatus").Include(
                        "~/Scripts/inTransitStatus.js*"));
            

            bundles.Add(new ScriptBundle("~/bundles/customspotmanager").Include(
                                    "~/Scripts/spotmanager.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customyardstatus").Include(
                                    "~/Scripts/yardstatus.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customexpected").Include(
                                    "~/Scripts/expectedArrivals.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customplanaudit").Include(
                                    "~/Scripts/planaudit.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customAddFromForecast").Include(
                                    "~/Scripts/addForecast.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customUpdatePriorities").Include(
                                    "~/Scripts/updatePriorities.js*"));

            bundles.Add(new ScriptBundle("~/bundles/ingatesReport").Include(
                                    "~/Scripts/ingatesReport.js*"));

            bundles.Add(new ScriptBundle("~/bundles/outgatesReport").Include(
                                    "~/Scripts/outgatesReport.js*"));

            bundles.Add(new ScriptBundle("~/bundles/history").Include(
                                    "~/Scripts/trailerHistory.js*"));

            bundles.Add(new ScriptBundle("~/bundles/tableSorter").Include(
                                    "~/Scripts/jquery.tablesorter.js",
                                    "~/Scripts/jquery.tablesorter.pager.js",
                                    "~/Scripts/tableIndex.js"
                                    ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.12.1.js*"));

            bundles.Add(new ScriptBundle("~/bundles/customLoading").Include(
                        "~/Scripts/loading.js*"));
        }
    }
}
