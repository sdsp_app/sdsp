﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SDSP.Startup))]
namespace SDSP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
