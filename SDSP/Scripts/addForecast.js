﻿$(document).ready(function () {
    var element;
    var tableSelected;
    var parentid;
    $("#plusone tr, #plustwo tr").dblclick(function () {
        element = this;
        $("#confirmModal").modal('toggle');
        console.log($(this).parent().attr('id'));

        parentid = $(this).parent().attr('id');      
        
    });

    $("#yes").click(function () {
        var routeMasterId = element.id;
        var value = $(element).closest('tr').children('td:first').text();
        console.log(element);
        console.log(value);
        if (routeMasterId > 0 && value != "") {
            $(element).remove();
            $.ajax({
                type: "POST",
                url: 'updatePullAhead',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ routeMasterId: routeMasterId }),
                dataType: "json",
                success: function (data) {
                    var id = data.updated.Id;
                    var tn = data.updated.TrailerNumber;
                    var route = data.updated.Route;
                    var run = data.updated.Run;
                    var RouteRun = data.updated.RouteRun;
                    var ud = new Date(data.updated.UnloadDate.match(/\d+/) * 1);
                    ud = ud.toLocaleString("en-US");
                    ud = ud.replace(',', "");
                    var priority = data.updated.Priority;
                    //Agregar el nuevo renglon a la tabla del dia.
                    var makeTr = "<tr id='" + id + "'>><td>" + priority + "</td><td>" + tn + "</td><td>" + RouteRun + "</td</tr>";

                    $('#current > tbody').append(makeTr);

                    $('#' + id).data('origin', parent);

                    $('#current-container').scrollTop($('#current-container')[0].scrollHeight - $('#current-container')[0].clientHeight);
                },
                error: function (jqXHR) {
                    console.log("Error updating...");
                    console.log(jqXHR.responseText);
                }
            });
        } else {
            $("#errorText").html("You cannot add that trailer from forecast, please use the <b>Add Record</b> option.");                        
            $("#errorModal").modal('toggle');
        }
    });

    //$("#current tbody > tr").dblclick(function () {
    $("#current tbody").on("dblclick", "tr", function(e) {
        //Editar intransit
        rMIdtoEdit = this.id;
        //console.log("Editando: " + rMIdtoEdit);        


        $("#saveRecord").hide();
        $("#saveChanges").show();
        $("#simpleEdit").show();
        
        loadData(rMIdtoEdit);
        disableFields();
        $("#tituloModal").text("Edit Trailer Number");
        $("#addRecordModal").modal('toggle');
    });

    var rMIdtoEdit = 0;

    function disableFields() {
        //$("#Route").prop('disabled', true);
        //$("#Origin").prop('disabled', true);
        $("#UnloadDate").prop('disabled', true);
        $("#Hazmat").prop('disabled', true);
        //$("#F1").prop('disabled', true);
        $("#PullAhead").prop('disabled', true);
        $("#Status_Comments").prop('disabled', true);
    }

    function enableFields() {
        //$("#Route").prop('disabled', false);
        //$("#Origin").prop('disabled', false);
        $("#UnloadDate").prop('disabled', false);
        $("#Hazmat").prop('disabled', false);
        //$("#F1").prop('disabled', false);
        $("#PullAhead").prop('disabled', false);
        $("#Status_Comments").prop('disabled', false);
    }

    function loadData(routeMasterId) {
        //Traer datos del registro en RouteMaster
        $.ajax({
            type: "POST",
            url: '../Ingates/getSingleRouteMasterData',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: routeMasterId }),
            dataType: "json",
            success: function (data) {
                clearfields();
                $("#Trailer").val(data.RouteMaster.TrailerNumber.trim());
                $("#Route").val(data.RouteMaster.Route.trim());
                $("#Origin").val(data.RouteMaster.Run.trim());
                var dateString = data.RouteMaster.UnloadDate.substr(6);
                var currentTime = new Date(parseInt(dateString));
                var month = currentTime.getMonth() + 1;
                var day = currentTime.getDate();
                var year = currentTime.getFullYear();
                var date = year+"-"+month+"-"+day;
                $("#UnloadDate").val(date);
                var hazmat = data.RouteMaster.Hazmat;
                //console.log("Hazmat: "+hazmat);
                hazmat = (hazmat != null && hazmat != "") ? 1 : 0;
                //console.log("Hazmat2: " + hazmat);
                $("#Hazmat").val(hazmat);
            },
            error: function (jqXHR) {
                console.log("Error retrieving data");
                console.log(jqXHR.responseText);
            }
        });
    }

    $("#searchPlusOne").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#plusone tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#searchPlusTwo").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#plustwo tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#add-intransit").click(function () {
        $("#tituloModal").text("Add Record");
        $("#saveRecord").show();
        $("#saveChanges").hide();
        $("#simpleEdit").hide();
        clearfields();
        enableFields();
        $("#addRecordModal").modal('toggle');
    });

    $("#saveChanges").click(function () {
        var trailer = $("#Trailer").val();
        $.ajax({
            type: "POST",
            url: 'editInTransit',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId : rMIdtoEdit,trailer: trailer}),
            dataType: "json",
            success: function (data) {
                if (data.saved.Id > 0) {
                    clearfields();
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function(){
                        $("#divMessage").slideUp(500);
                    });
                    location.reload();
                } else {
                    if(data.saved.Id == -1){
                        //La este caso se debe manejar con el modulo de update Priorities
                        $("#errorText").html("Both trailers are in today's in transit, please use the <b>Update Priorities</b> module.");
                    }else{
                        //si no hay datos quiere decir que no se pudo grabar (Ya se habia dado outgate)
                        $("#errorText").html("You cannot edit a trailer that has been dispatched by SDSP.");                        
                    }
                    $("#errorModal").modal('toggle');
                }
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
        $("#addRecordModal").modal('toggle');
    });

    $("#simpleEdit").click(function () {
        //console.log("Saving changes...");
        var trailer = $("#Trailer").val();
        var route = $("#Route").val();
        var origin = $("#Origin").val();
        $.ajax({
            type: "POST",
            url: 'simpleEditIntransit',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: rMIdtoEdit, trailer: trailer, route:route,run:origin }),
            dataType: "json",
            success: function (data) {
                if (data.saved.Id > 0) {
                    //Si hay datos quiere decir que se grabo correctamente
                    var id = data.saved.Id;
                    var tn = data.saved.TrailerNumber;
                    var route = data.saved.Route;
                    var run = data.saved.Run;
                    var RouteRun = data.saved.RouteRun;
                    var ud = new Date(data.saved.UnloadDate.match(/\d+/) * 1);
                    ud = ud.toLocaleString("en-US");
                    ud = ud.replace(',', "");
                    var priority = data.saved.Priority;
                    //Cambiar el nuevo renglon de la tabla
                    var makeTr = "<td>" + priority + "</td><td>" + tn + "</td><td>" + RouteRun + "</td>";
                    $("#" + rMIdtoEdit).html(makeTr);
                    $("#" + rMIdtoEdit).attr("id", id)
                    clearfields();
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                        //location.reload();
                    });                    
                } else {
                    //si no hay datos quiere decir que no se pudo grabar 
                    if(data.saved.Id == -1){
                        //La nueva caja ya estaba en el intransit
                        $("#errorText").html("That trailer was already in today's In transit. No record was saved!");
                    }else{
                        //Ya se habia dado outgate
                        $("#errorText").html("You cannot edit a trailer that has been dispatched by SDSP.");
                    }
                    $("#errorModal").modal('toggle');
                }
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
        $("#addRecordModal").modal('toggle');
    });

    $("#saveRecord").click(function () {
        var trailer = $("#Trailer").val();
        var route = $("#Route").val();
        var origin = $("#Origin").val();
        var unloadDate = $("#UnloadDate").val();
        var hazmat = $("#Hazmat").val();
        var pullAhead = $("#PullAhead").val();
        var comments = $("#Status_Comments").val();
        var f1 = $("#F1").val();
        $.ajax({
            type: "POST",
            url: 'saveSingleInTransit',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ trailer:trailer, route:route, origin:origin, unloadDate:unloadDate, hazmat:hazmat, pullahead:pullAhead, comments:comments , f1:f1 }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                if(data.saved.Id > 0){
                    //Se capturo la caja nueva
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    var id = data.saved.Id;
                    var tn = data.saved.TrailerNumber;
                    var route = data.saved.Route;
                    var run = data.saved.Run;
                    var ud = new Date(data.saved.UnloadDate.match(/\d+/) * 1);
                    ud = ud.toLocaleString("en-US");
                    ud = ud.replace(',', "");
                    var priority = data.saved.Priority;
                    //Agregar el nuevo renglon a la tabla del dia.
                    var makeTr = "<tr id='" + id + "'>><td>" + priority + "</td><td>" + tn + "</td><td>" + route + "</td><td>" + run + "</td</tr>";
                    $('#current > tbody').append(makeTr);
                    clearfields();
                }else{
                    //No se capturo el registro porque ya existia esa caja en el intransit
                    $("#errorText").html("That trailer was already in today's In transit. No record was saved!");
                    $("#errorModal").modal('toggle');
                }
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
        $("#addRecordModal").modal('toggle');
    });

    function clearfields() {
        $("#Trailer").val("");
        $("#Route").val("");
        $("#Origin").val("");
        $("#UnloadDate").val("");
        $("#Hazmat").val(0);
        $("#F1").val("");
        $("#PullAhead").val(0);
        $("#Status_Comments").val("");
    }

    //Codigo para el menu
    //const menu = document.querySelector(".menu");
    //const table = document.querySelector("#current tbody");
    //let menuVisible = false;

    //const toggleMenu = command => {
    //    menu.style.display = command === "show" ? "block" : "none";
    //    menuVisible = !menuVisible;
    //};

    //const setPosition = ({ top, left }) => {
    //    menu.style.left = `${left}px`;
    //    menu.style.top = `${top}px`;
    //    toggleMenu("show");
    //};
    
    //window.addEventListener("click", e => {
    //    if(menuVisible){
    //        toggleMenu("hide");
    //    }
    //})

    //window.addEventListener("keyup", e => {
    //    if(e.keyCode == 27 && menuVisible){
    //        toggleMenu("hide");
    //    }
    //})

    //$("#current tbody").on("contextmenu", "tr", function(e) {
        //e.preventDefault();
        //console.log(this.id);
        //const origin = {
        //    left: e.pageX,
        //    top: e.pageY
        //};
        //setPosition(origin);
        //return false;
    //});
    
});