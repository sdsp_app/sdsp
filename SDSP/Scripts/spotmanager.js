﻿$(document).ready(function () {
    var status = 0;
    var clicked = {};
    
    $("li[id^='spot-']").click(function () {
        $(this).toggleClass("selected");
        clicked = this;
        if ($(this).hasClass("selected")) {
            //Blocked
            status = 1;
            $("#comment-title").html("Reason for blocking spot number: "+$(clicked).text());
            $('#exampleModal').modal('toggle');
        } else {
            //Free
            $('#confirmModal').modal('toggle');            
        }
    });

    $("#yes").click(function () {
        status = 2;
        blockspot();
    });

    $("#no").click(function () {
        $(clicked).toggleClass("selected");
    });

    function blockspot() {        
        $.ajax({
            type: "POST",
            url: 'blockspots',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ spot: $(clicked).text(), status: status, comment: $("#Comment").val() }),
            dataType: "json",
            success: function (data) {
                if (data.saved == true) {
                    $("#success-message").text("Unblocked!");
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                }
            }
        });
    }

    $("li[id^='spot-']").contextmenu(function () {        
        if ($(this).hasClass("selected")) {
            var parts = $(this).attr("id").split("-");
            var spot = parts[1];
            $.ajax({
                type: "POST",
                url: 'getReason',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ spot: spot }),
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('#reasonText').html(data.reason);
                },
                error: function (jqXHR) {
                    console.log(jqXHR.responseText);
                }
            });
            $('#reasonModal').modal('toggle');
        }
        return false;
    });

    function clearComment() {
        $("#Comment").val("");
    }

    $('.cerrar').click(function () {
        $(clicked).toggleClass("selected");
    });

    $("#saveChange").click(function () {
        if ($("#Comment").val().length == 0) {
            alert("Please enter a reason");
            $("#Comment").focus();
        } else {
            $.ajax({
                type: "POST",
                url: 'blockspots',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ spot: $(clicked).text(), status: status, comment: $("#Comment").val() }),
                dataType: "json",
                success: function (data) {
                    if (data.saved == true) {
                        $("#success-message").text("Blocked!");
                        $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                            $("#divMessage").slideUp(500);
                        });
                    }
                }
            });
            $('#exampleModal').modal('toggle');
            clearComment();
        }
    });
    
});