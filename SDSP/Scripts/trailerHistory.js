﻿$(document).ready(function () {
    $("#btnSubmit").click(function () {
        var start = $("#start").val();
        var end = $("#end").val();
        var trailer= $("#TrailerNumber").val();
        window.location = "ExportTrailerHistory?start="+start+"&end="+end+"&trailer="+trailer;
    });

    $("#Trailers tbody").on("click", "tr", function () {
        var RouteMasterId = $(this).attr("id");
        $.ajax({
            type: "POST",
            url: 'getYardComments',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterid: RouteMasterId }),
            dataType: "json",
            success: function (data) {
                //console.log(data);
                $("#table-comments").find("tbody > tr").remove();
                var x = 0;
                $.each(data, function (index, item) {
                    var d = new Date(item.DateTime.match(/\d+/) * 1);
                    var t = d.toLocaleTimeString();
                    var comment = item.Comment;
                    var user = item.AspNetUsersId;
                    if (x == 0) {
                        $('#table-comments > tbody').append('<tr><td>' + comment + '</td><td>' + user + '</td><td>' + d.toLocaleDateString() + " " + t + '</td></tr>');
                    } else {
                        $('#table-comments > tbody:last-child').append('<tr><td>' + comment + '</td><td>' + user + '</td><td>' + d.toLocaleDateString() + " " + t + '</td></tr>');
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText);
            }
        });

        $('#commentsModal').modal('toggle');
    });
});