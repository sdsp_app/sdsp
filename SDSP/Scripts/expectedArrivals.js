﻿$(document).ready(function () {

    $("#tabs").tabs();
    var routeMasterId = {};
    var selected = {};
    $("#yardspots tbody tr").click(function () {
        var clase = $(this).attr("class");
        routeMasterId = this.id;       

        $.ajax({
            type: "POST",
            url: 'getYardComments',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterid: routeMasterId }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                $("#table-comments").find("tbody > tr").remove();
                var x = 0;
                $.each(data, function (index, item) {
                    var d = new Date(item.DateTime.match(/\d+/) * 1);
                    var t = d.toLocaleTimeString();
                    var comment = item.Comment;
                    var user = item.AspNetUsersId;
                    if (x == 0) {
                        $('#table-comments > tbody').append('<tr><td>' + comment + '</td><td>' + user + '</td><td>' + d.toLocaleDateString() + " " + t + '</td></tr>');
                    } else {
                        $('#table-comments > tbody:last-child').append('<tr><td>' + comment + '</td><td>' + user + '</td><td>' + d.toLocaleDateString() + " " + t + '</td></tr>');
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText);
            }
        });

        $('#addCommentsModal').modal('toggle');
    });

    $("#save-comment").click(function () {
        $.ajax({
            type: "POST",
            url: 'saveYardComment',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ comment: $("#Comment").val(), routeMasterid: routeMasterId }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                $("Comment").val("");
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText);
            }
        });
        $('#addCommentsModal').modal('toggle');
    });

    $("#Comment").change(function () {
        if ($(this).val() != "") {
            $("#save-comment").show();
        } else {
            $("#save-comment").hide();
        }
    });

    $("#Comment").keyup(function (e) {
        var value = $(this).val();
        if (value.length == 0) {
            $("#save-comment").hide();
        } else {
            $("#save-comment").show();
        }
    });

    $('#addCommentsModal').on('hidden.bs.modal', function () {
        $("#tabs").tabs({ active: 0 });
        $("#Comment").val("");
        $("#save-comment").hide();
    });

});
