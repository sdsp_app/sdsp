﻿$(document).ready(function () {
    $(function () {
        $("#datatables").accordion({
            heightStyle: "content",
            collapsible: true
        });
    });

    $("#btnSubmit").click(function () {
        var start = $("#startDate").val();
        var end = $("#endDate").val();
        var type = $("#type").val();
        window.location = "ExportPlanAudit?start="+start+"&end="+end+"&type="+type;
    });
});