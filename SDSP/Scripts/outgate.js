﻿$(document).ready(function () {

    $(function () {
        $.widget("custom.combobox", {
            _create: function () {
                this.wrapper = $("<span>")
                  .addClass("custom-combobox")
                  .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },

            _createAutocomplete: function () {
                var selected = this.element.children(":selected"),
                value = "";
                this.input = $("<input>")
                  .appendTo(this.wrapper)
                  .val(value)
                  .attr({ "title": "", "id": "input" + $(this.element).attr("id") })
                  .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                  .autocomplete({
                      delay: 0,
                      minLength: 0,
                      source: $.proxy(this, "_source"),
                      response: function (event, ui) {
                          // ui.content is the array that's about to be sent to the response callback.
                          if (ui.content.length === 0) {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("No matches found <a id='add-driver' style='cursor:pointer'>Add Driver</a>");
                              } else {
                                  $("#company-error").html("No matches found <a id='add-company' style='cursor:pointer'>Add Company</a>");
                              }
                          } else {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("");
                              } else {
                                  $("#company-error").html("");
                              }
                          }
                      }
                  })
                  .tooltip({
                      classes: {
                          "ui-tooltip": "ui-state-highlight"
                      }
                  });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function () {
                var input = this.input,
                  wasOpen = false;

                $("<a>")
                  .attr("tabIndex", -1)
                  .attr("title", "Show All Items")
                  .tooltip()
                  .appendTo(this.wrapper)
                  .button({
                      icons: {
                          primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                  })
                  .removeClass("ui-corner-all")
                  .addClass("custom-combobox-toggle ui-corner-right")
                  .on("mousedown", function () {
                      wasOpen = input.autocomplete("widget").is(":visible");
                  })
                  .on("click", function () {
                      input.trigger("focus");
                      // Close if already visible
                      if (wasOpen) {
                          return;
                      }
                      // Pass empty string as value to search for, displaying all results
                      input.autocomplete("search", "");
                  });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text))) {
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    }
                }));
            },

            _removeIfInvalid: function (event, ui) {
                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                  valueLowerCase = value.toLowerCase(),
                  valid = false;
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.element.val("");

                this.input.autocomplete("instance").term = "";
            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

            $("#DriverId").combobox();
            $("#CompanyId").combobox();
    });

    $("#driver-error").on("click", "a", function () {
        //Desplegar form para agregar driver
        $("#divAddDriver").show("fade");
    });

    $("#save-driver").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '../Ingates/saveDriver',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ driverName: $("#DriverName").val(), companyId: $("#CompanyId").val() }),
            dataType: "json",
            success: function (data) {
                $('#DriverId')
                         .append($('<option>', { value: data.Id })
                         .text(data.Name));
                $("#divAddDriver").hide("fade");
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#company-error").on("click", "a", function () {
        //Desplegar form para agregar company
        $("#divAddCompany").show("fade");
    });

    $("#save-company").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '../Ingates/saveCompany',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ companyName: $("#CompanyName").val() }),
            dataType: "json",
            success: function (data) {
                $('#CompanyId')
                         .append($('<option>', { value: data.Id })
                         .text(data.Name));
                $("#divAddCompany").hide("fade");
            },
            error: function (jqXHR) {
                console.log("Error saving company");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#closeDriver").click(function () {
        $("#divAddDriver").hide("fade");
    });

    $("#closeCompany").click(function () {
        $("#divAddCompany").hide("fade");
    });

    $("#btnTrailers").click(function () {
        event.preventDefault();
        showTable();
    });

    var maxindex = 0;

    function showTable() {
        $.ajax({
            type: "POST",
            url: 'GetTrailers',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#trailers").find("tbody > tr").remove();
                maxindex = 0;
                $.each(data, function (index, item) {
                    console.log(item);
                    var tn = item.RouteMaster.TrailerNumber.trim();
                    var tn_driver = item.RouteMaster.TrailerNumber.trim() + "&&" + item.Driver.Id + "&&" + item.PreOutgate.Id + "&&" + item.RouteMaster.Id;
                    var priority = (item.RouteMaster.Priority != null ) ? item.RouteMaster.Priority : "";
                    var express = "";
                    var spot = item.Ingate.YardSpotId;
                    if (item.RouteMaster.ExpressPriority == null) {
                        express = "N/A"
                    } else {
                        express = item.RouteMaster.ExpressPriority;
                    }
                    var driverName = "";
                    var comment = item.RouteMaster.Comments;
                    if (item.Driver.Id > 0) {                        
                        driverName = item.Driver.Name + " - " + item.Company.Name;
                    }
                    var makeTR = '<tr tabindex="' + maxindex + '" id="' + tn_driver + '"><td>' + tn + '</td><td>' + spot + '</td><td>' + priority + '</td><td>'+comment+'</td><td id="drivercell-' + item.RouteMaster.Id + '">' + driverName + '</td></tr>';
                    if (maxindex == 0) {
                        $('#trailers > tbody').append(makeTR);
                    } else {
                        $('#trailers > tbody:last-child').append(makeTR);
                    }
                    $("#trailers tbody tr[tabindex=0]").focus();
                    if (item.RouteMaster.Id in itemsSelected) {
                        $("#trailers tbody tr[tabindex="+maxindex+"]").addClass("selected");
                    }
                    maxindex++;
                });

                $("#divTrailers").show("fade");
            },
            error: function () {
                console.log("Error");
            }
        });
    }

    showTable();

    var itemsSelected = {};

    $("#trailers tbody").on("click", "tr", function () {        
        tblClick(this);        
    });

    function tblClick(elemento) {                
        var parts = $(elemento).attr("id").split("&&");
        //Parts[0] = trailernumber
        //Parts[1] = driver id
        //Parts[2[ = PreoutgateId
        //Parts[3] = RoutemasterId
        if (parts[1] > 0) {
            //Si tiene driver asignad
            $(elemento).toggleClass("selected");
            if ($(elemento).hasClass("selected")) {
                if (!(parts[0] in itemsSelected)) {
                    //agregamos el elemento al objeto
                    itemsSelected[parts[3]] = parseInt(parts[1]);
                }
            } else {
                //sacamos el elemento del objeto
                delete itemsSelected[parts[3]];
            }
        } else {
            //Si no tiene driver asignado
            alert("Please assign a driver first");
        }
    }

    var assignationToChange;

    $("#trailers tbody").on("contextmenu", "tr", function () {
        //console.log("click derecho");
        //tblDblClick(this);
        //return false;
    });

    var lastDblClicked;

    function tblDblClick(elemento) {
        var parts = $(elemento).attr("id").split("&&");
        $('#tblEditDriverModal').modal('toggle');
        $("#tblEditDriver-title").text("Change driver for: " + parts[0]);
        assignationToChange = parts[2];
        rmToSave = parts[3];
        lastDblClicked = elemento;
    }

    var rmToSave;
    var driversAssigned = {};

    $("#change-driver").click(function () {
        if (assignationToChange == 0) {
            //Si no hay PreoutgateId 
            $.ajax({
                type: "POST",
                url: 'assignDriver',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ routeMasterId: rmToSave, newDriver: $("#DriverId").val() }),
                dataType: "json",
                success: function (data) {
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    $(lastDblClicked).addClass("selected");
                    var parts = $(lastDblClicked).attr("id").split("&&");
                    //Parts[0] = trailernumber
                    //Parts[1] = driver id
                    //Parts[2[ = PreoutgateId
                    //Parts[3] = RoutemasterId
                    if (!(parts[0] in itemsSelected)) {
                        //Si el elemento no esta en el objeto lo agregamos
                        itemsSelected[parts[3]] = parseInt($("#DriverId").val());
                        var newId = parts[0] + "&&" + $("#DriverId").val() + "&&" + parts[2] + parts[3];
                        $(lastDblClicked).attr("id", newId);
                    }
                    showTable();
                },
                error: function (jqXHR) {
                    console.log("Error");
                    console.log(jqXHR.responseText);
                }
            });
        }
        else {
            //Si hay PreoutgateId
            $.ajax({
                type: "POST",
                url: 'updateDriver',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ preoutgateId: assignationToChange, newDriver: $("#DriverId").val() }),
                dataType: "json",
                success: function (data) {
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    showTable();
                },
                error: function (jqXHR) {
                    console.log("Error");
                    console.log(jqXHR.responseText);
                }
            });
        }
    });

    $("#trailers tbody").on('keydown', "tr", function (event) {
        //console.log(event.keyCode);
        switch (event.keyCode) {
            case 40:
                // Flecha abajo  
                var tabIndex = $(this).attr("tabindex");
                if (tabIndex < (maxindex - 1)) {
                    tabIndex++;
                }
                $("#trailers tbody tr[tabindex=" + tabIndex + "]").focus();
                $("#ingateId").val($("#trailers tbody tr[tabindex=" + tabIndex + "]").attr("id"));
                break;
            case 38:
                // Flecha arriba  
                var tabIndex = $(this).attr("tabindex");
                if (tabIndex > 0) {
                    tabIndex--;
                }
                $("#trailers tbody tr[tabindex=" + tabIndex + "]").focus();
                $("#ingateId").val($("#trailers tbody tr[tabindex=" + tabIndex + "]").attr("id"));
                break;
            case 9:
                // Tecla tab
                var inputs = $(this).closest('form').find(':input');
                //Mandamos el tab al siguiente elemento tipo input (En este caso el boton de dispatch)
                inputs.eq(inputs.index(this) + 1).focus();
                break;
            case 32:
                // Tecla espacio
                tblClick(this);
                return false;                
                break
            case 13:
                // Tecla enter
                //tblDblClick(this);
                break;
            //case 27:
            //    $('#skippedModal').modal('toggle');
            //    break;
        }                 
    });

    $("#dispatch").click(function (e) {
        e.preventDefault();
        if (!jQuery.isEmptyObject(itemsSelected)) {
            document.getElementById("dispatch").disabled = true;
            $.ajax({
                type: "POST",
                url: 'saveDispatch',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ trailers: itemsSelected }),
                dataType: "json",
                success: function (data) {
                    //console.log("Brincados: " + data.saved.length);
                    if (data.saved.length > 0) {
                        console.log(data);
                        var text = "Some trailers were dispatched before, they were not saved again, here is the list: ";
                        $.each(data.saved, function (index, item) {
                            text += item.TrailerNumber + ", ";
                        });
                        text += "All others were saved successfully";
                        $("#warningText").text(text);
                        console.log(text);

                        $("#divWarning").show("fade");
                        //$("#divWarning").fadeTo(2000, 500).slideUp(500, function () {                                                      
                        //    $("#divWarning").slideUp(500);
                        //});
                    //    //Pedir razon para salto de prioridad
                    //    $.each(data.saved, function (index, item) {
                    //        $("#trailers").find("tbody > tr").remove();
                    //        if (maxindex == 0) {
                    //            $('#reasons > tbody').append('<tr><td>' + item.TrailerNumber + '</td>><td><textarea id="' + item.Id + '" rows="4" cols="50"></textarea></td><td><button id="button-' + item.Id + '" type="button" class="submit-reasons btn btn-secondary">Save</button></td></tr>');
                    //        } else {
                    //            $('#reasons > tbody:last-child').append('<tr><td>' + item.TrailerNumber + '</td>><td><textarea id="' + item.Id + '" rows="4" cols="50"></textarea></td><td><button id="button-' + item.Id + '" type="button" class="submit-reasons btn btn-secondary">Save</button></td></tr>');
                    //        }
                    //    });
                    //    $('#skippedModal').modal('toggle');
                    } else {
                        $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                            $("#divMessage").slideUp(500);
                        });
                    }                                       
                    
                    document.getElementById("dispatch").disabled = false;
                    itemsSelected = {};
                    showTable();
                },
                error: function (jqXHR) {
                    console.log("Error");
                    console.log(jqXHR.responseText);
                }
            });
        } else {
            $("#divError").fadeTo(2000, 500).slideUp(500, function () {                
                //$("#errorText").html("No rows were selected");
                $("#divError").slideUp(500);
            });
            showTable();
        }
    });
    
    $("#closeMessage").click(function () {
        $('#divMessage').hide('fade');
    });
    $("#closeErrorMessage").click(function () {
        $('#divError').hide('fade');
    });
    $("#closeWarning").click(function () {
        $('#divWarning').hide('fade');
    });    

    $('#tblEditDriverModal').on('hidden.bs.modal', function (e) {
        $("#trailers tbody tr[tabindex=0]").focus();
        console.log($(lastDblClicked).attr("id"));
        $(lastDblClicked).focus();
    })

    $("#reasons tbody").on("click", "button", function () {
        var rowCount = $('#reasons tbody tr').length;
        var parts = this.id.split("-");
        text = $("#" + parts[1]).val();
        console.log(rowCount);
        $.ajax({
            type: "POST",
            url: 'saveSkipReasons',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: parts[1], reason: text }),
            dataType: "json",
            success: function (data) {                
                $("#" + parts[1]).closest("tr").remove();
                rowCount = $('#reasons tbody tr').length;
                if (rowCount == 0) {
                    $('#skippedModal').modal('toggle');
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                }
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#searchOutgate").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#trailers tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});