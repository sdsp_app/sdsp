﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class Ingate
    {
        public long Id { get; set; }
        public Nullable<long> RouteMasterId { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }

        [Range(1, 295, ErrorMessage = "Please select a spot")]
        public Nullable<int> YardSpotId { get; set; }

        [Required(ErrorMessage = "Please select a driver from the list")]
        public Nullable<long> DriverId { get; set; }

        [Required(ErrorMessage = "Please select bound")]
        public Nullable<int> Bound { get; set; }
        public string AspNetUsersId { get; set; }

        [Required(ErrorMessage = "Please select a trailer status")]
        public Nullable<int> TrailerStatusId { get; set; }

        [Required(ErrorMessage = "Please enter a trailer number")]
        public string TrailerNumber { get; set; }

        public string Hazmat { get; set; }
    }

    public class PrintDropVM
    {
        public Driver Driver { get; set; }
        public Company Company { get; set; }
        public Ingate Ingate { get; set; }
        public RouteMaster RouteMaster { get; set; }
    }

    public class IngateTrailersVM
    {
        public Ingate Ingate { get; set; }
        public RouteMaster RouteMaster { get; set; }
        public Comment Comment { get; set; }
    }

}