﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class PreOutgate
    {
        public long Id { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public string AspNetUsersId { get; set; }

        [Required(ErrorMessage = "Please select a driver from the list")]
        public Nullable<long> DriverId { get; set; }

        [Required(ErrorMessage = "Please select a trailer from the list")]
        public Nullable<long> RouteMasterId { get; set; }
    }

    public class PrintHookVM
    {
        public PreOutgate PreOutgate { get; set; }
        public Driver Driver { get; set; }
        public Company Company { get; set; }
        public Ingate Ingate { get; set; }
        public RouteMaster RouteMaster { get; set; }
    }

    public class PrintHooKTotalVM
    {
        public PrintHookVM PrintHookVM { get; set; }
        public PrintDropVM PrintDropVM { get; set; }
    }
}
