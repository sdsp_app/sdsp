﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class YardStatu
    {
        public int Id { get; set; }
        public Nullable<int> SpotStatusId { get; set; }
        public string Hazmat { get; set; }
        public string Route { get; set; }
        public string Run { get; set; }
        public string TrailerNumber { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<System.DateTime> IngateDateTime { get; set; }
        public Nullable<int> Bound { get; set; }
        public Nullable<long> IngateId { get; set; }
        public string Comment { get; set; }
        public Nullable<int> YardSpotId { get; set; }
        public Nullable<long> RouteMasterId { get; set; }
        public string Status { get; set; }
    }
}