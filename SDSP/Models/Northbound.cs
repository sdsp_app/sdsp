﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class Northbound
    {
        public int Id { get; set; }
        public Nullable<long> RouteMasterId { get; set; }
        public Nullable<int> DriverId { get; set; }
        public Nullable<int> TrailerStatusId { get; set; }
    }

    public class NorthTrailer{
        public RouteMaster RouteMaster { get; set; }
        public Northbound Northbound { get; set; }
    }
}