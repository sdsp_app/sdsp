﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public partial class Driver
    {
        public long Id { get; set; }
        [Required(ErrorMessage ="Please enter the drivers name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please choose a company")]
        public Nullable<long> IdCompany { get; set; }
        public string AspNetUsersId { get; set; }
        public string DriverCode { get; set; }
    }

    public class DriverCompanyVM
    {
        public Driver Driver { get; set; }
        public Company Company { get; set; }
    }
}
