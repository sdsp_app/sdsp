﻿using SDSP.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class ImportExcel
    {
        [Required(ErrorMessage = "Please select file")]
        [FileExt(Allow = ".xls,.xlsx", ErrorMessage = "Only excel file")]
        public HttpPostedFileBase file { get; set; }
    }

    public class RouteMasterExcel {
        [Required(ErrorMessage = "Please select file")]
        [FileExt(Allow = ".xls,.xlsx", ErrorMessage = "Only excel file")]
        public HttpPostedFileBase file { get; set; }

        [Required]
        [RegularExpression("([0-9]+(\\.[0-9]+)?)", ErrorMessage = "Period must be numeric")]
        public float period { get; set; }

        [Required]
        public DateTime StartDate { get; set; }
    }
}