﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{

    public partial class Company
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Please enter the company's name")]
        public string Name { get; set; }
        public string AspNetUsersId { get; set; }
        public Nullable<int> CompanySourceId { get; set; }
    }
}
