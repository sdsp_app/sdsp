﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class YardComment
    {
        public int Id { get; set; }
        public Nullable<long> RouteMasterId { get; set; }
        public string Comment { get; set; }
        public string AspNetUsersId { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
    }
}