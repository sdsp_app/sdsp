﻿using SDSP.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{

        public partial class YardSpot
        {
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
            public YardSpot()
            {
                this.Ingates = new HashSet<Ingate>();
            }

            public int Id { get; set; }
            public Nullable<int> SpotNumber { get; set; }
            public Nullable<int> SpotStatusId { get; set; }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
            public virtual ICollection<Ingate> Ingates { get; set; }
        }

        public class InvoicesImportExcel
        {
            [Required(ErrorMessage = "Please select file")]
            [FileExt(Allow = ".xls,.xlsx", ErrorMessage = "Only excel file")]
            public HttpPostedFileBase file { get; set; }

            public List<SpotsVM> listSpotsVM { get; set; }
        }

        public class SpotsVM
        {
            public int? yardSpotId { get; set; }
            public string trailerNumber { get; set; }
        }

        public class InvoiceUpdate
        {
            [Required(ErrorMessage = "Please enter a trailer number")]
            public string TrailerNumber { get; set; }
            public string Invoice { get; set; }
            public long RouteMasterId { get; set; }
            public SpotsVM SpotsVM { get; set; }
        }

        public class YardStatusVM
        {
            public List<YardSpot> listSpots { get; set; }
            public List<PrintHookVM> listContenedoresenYarda { get; set; }
        }
    
}