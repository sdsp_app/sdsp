﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class Outgate
    {
        public long Id { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public Nullable<long> DriverId { get; set; }
        public Nullable<long> RouteMasterId { get; set; }
    }

    public class DispatchInfo{
        public Driver Driver { get; set; }
        public RouteMaster RouteMaster { get; set; }
    }
}
