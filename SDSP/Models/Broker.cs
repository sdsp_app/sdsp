﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class Broker
    {
        public long Id { get; set; }
        public string TrailerNumber { get; set; }
        public string Route { get; set; }
        public string Sequence { get; set; }
        public Nullable<System.DateTime> UnloadDate { get; set; }
        public string Invoice { get; set; }
    }
}
