﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class Inventory
    {
        public int Spot { get; set; }
        public string Status { get;set;}
        public string Bound { get; set; }
        public string TrailerNumber { get; set; }
        public string Route { get; set; }    
        public DateTime ATA { get; set; }
        public int Priority { get; set; }
        public int Days { get; set; }
        public int Hours { get; set; }
    }
}