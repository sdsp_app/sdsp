﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class Issue
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public string Involved { get; set; }
        public string Solution { get; set; }
        public int IssueStatusId { get; set; }
        public Nullable<System.DateTime> DateTime { get; set; }
        public string AspNetUsersId { get; set; }
    }
}
