﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class RouteMaster
    {
        [Key]
        public long Id { get; set; }
        public string Hazmat { get; set; }
        public Nullable<System.DateTime> UnloadDate { get; set; }
        public string RouteRun { get; set; }
        public string Route { get; set; }
        public string Run { get; set; }
        public string PuRoute2 { get; set; }
        public string RouteType { get; set; }
        public string RouteMiles { get; set; }
        public string LegMiles { get; set; }
        public string Seq { get; set; }
        public string SName { get; set; }
        public string LDK { get; set; }
        public string LogisticsPointId { get; set; }
        public Nullable<System.DateTime> ArrvDate { get; set; }
        public Nullable<System.TimeSpan> ArrvTime { get; set; }
        public Nullable<System.DateTime> DepDate { get; set; }
        public Nullable<System.TimeSpan> DepTime { get; set; }
        public string Timezone { get; set; }
        public string RoutesMilestone { get; set; }
        public string Day { get; set; }
        public int PeriodId { get; set; }
        public Nullable<System.DateTime> RegisterDate { get; set; }
        public string TrailerNumber { get; set; }
        public string Comments { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<int> DataSourceId { get; set; }
        public Nullable<int> ExpressPriority { get; set; }
        public string Invoice { get; set; }
        public Nullable<System.DateTime> PullAhead { get; set; }
    }

    public class PlanAuditVM
    {
        public int type { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public List<RouteMaster> ListRouteMaster {get;set;}
    }
    
    public class ForecastVM
    {
        public List<RealTime_InTransit> current { get; set; }
        public List<RealTime_InTransit> plusone { get; set; }
        public List<RealTime_InTransit> plustwo { get; set; }
    }

    public class RealTimeInTransit
    {
        public List<RouteMaster> inTransit { get; set; }
        public List<Ingate> ingates { get; set; }
        public List<Outgate> outgates { get; set; }
        public List<PreOutgate> preOutgates { get; set; }
    }

    public class TrailerHistory
    {
        public long? RouteMasterId { get; set; }
        public string Route { get; set; }
        public DateTime DateTime { get; set; }
        public String Driver { get; set; }
        public int? Spot { get; set; }
        public int Type { get; set; }
        public string TrailerStatus { get; set; }
        public int? Bound { get; set; }
        public string Hazmat { get; set; }
    }

    public class TrailerMovements
    {
        public List<TrailerHistory> THistory { get; set; }
        public string TrailerNumber { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
}
