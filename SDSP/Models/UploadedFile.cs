﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class UploadedFile
    {
        public string FileName { get; set; }
        public Nullable<int> FileTypeId { get; set; }
        public Nullable<System.DateTime> UploadDate { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class FilesReportVM
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public List<UploadedFile> fileList { get; set; }
    }
}