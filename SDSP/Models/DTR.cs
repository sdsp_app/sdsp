﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class DTR
    {
        public int Id { get; set; }
        public string TrailerNumber { get; set; }
        public string Route_Seq { get; set; }
        public string Renban { get; set; }
        public Nullable<System.DateTime> UnloadDate { get; set; }
        public Nullable<System.DateTime> SecondUnloadDate { get; set; }
        public Nullable<System.DateTime> ShipDate { get; set; }
        public Nullable<System.DateTime> ETAbyXPO { get; set; }
        public string Hazmat { get; set; }
        public Nullable<System.DateTime> XBorder { get; set; }
        public string CLoadLinesMROS { get; set; }
        public Nullable<System.DateTime> SystemUploadDate { get; set; }
        public Nullable<int> AspNetUsersId { get; set; }
    }
}
