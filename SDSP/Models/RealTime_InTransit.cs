﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDSP.Models
{
    public class RealTime_InTransit
    {
        public long Id { get; set; }
        public string Hazmat { get; set; }
        public Nullable<System.DateTime> UnloadDate { get; set; }
        public string RouteRun { get; set; }
        public string Route { get; set; }
        public string Run { get; set; }
        public string PuRoute2 { get; set; }
        public string RouteType { get; set; }
        public string RouteMiles { get; set; }
        public string LegMiles { get; set; }
        public string Seq { get; set; }
        public string SName { get; set; }
        public string LDK { get; set; }
        public string LogisticsPointId { get; set; }
        public Nullable<System.DateTime> ArrvDate { get; set; }
        public Nullable<System.TimeSpan> ArrvTime { get; set; }
        public Nullable<System.DateTime> DepDate { get; set; }
        public Nullable<System.TimeSpan> DepTime { get; set; }
        public string Timezone { get; set; }
        public string RoutesMilestone { get; set; }
        public string Day { get; set; }
        public int PeriodId { get; set; }
        public Nullable<System.DateTime> RegisterDate { get; set; }
        public Nullable<int> Priority { get; set; }
        public string TrailerNumber { get; set; }
        public string Comments { get; set; }
        public Nullable<int> DataSourceId { get; set; }
        public Nullable<int> ExpressPriority { get; set; }
        public string Invoice { get; set; }
        public Nullable<System.DateTime> PullAhead { get; set; }
        public Nullable<System.DateTime> ingateTime { get; set; }
        public Nullable<int> ingatespot { get; set; }
        public Nullable<long> ingatedriver { get; set; }
        public Nullable<System.DateTime> preTime { get; set; }
        public Nullable<long> predriver { get; set; }
        public Nullable<System.DateTime> outTime { get; set; }
        public Nullable<long> outDriver { get; set; }
        public Nullable<int> IdPreingateBC { get; set; }
        public Nullable<int> IdRouteMaster { get; set; }
        public Nullable<System.DateTime> PreIngateBCDate { get; set; }
        public string PreIngateBCSpot { get; set; }
        public Nullable<int> IngateBCId { get; set; }
        public Nullable<int> IngateBCRouteMasterId { get; set; }
        public Nullable<System.DateTime> IngateBcDateTime { get; set; }
        public string IngateBCSpot { get; set; }
        public string DriverName { get; set; }
        public Nullable<long> DriverIdCompany { get; set; }
        public Nullable<long> CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string DriverCode { get; set; }
    }
}
