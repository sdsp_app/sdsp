﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SDSP.Models;
using Microsoft.AspNet.Identity;

namespace SDSP.Controllers
{
    [Authorize]
    public class CompaniesController : Controller
    {

        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();

        private SDSPEntities db = new SDSPEntities();

        // GET: Companies
        public ActionResult Index()
        {
            List<Company> cpList = new List<Company>();

            var result = wcf.GetCompaniesList();

            foreach (var item in result) {
                cpList.Add(new Company {
                    Name = item.Name,
                    Id = item.Id
                });
            }

            return View(cpList);
        }

        public JsonResult reloadIndex()
        {
            List<Company> cpList = new List<Company>();

            var result = wcf.GetCompaniesList();

            foreach (var item in result)
            {
                cpList.Add(new Company
                {
                    Name = item.Name,
                    Id = item.Id
                });
            }

            return Json(cpList,JsonRequestBehavior.AllowGet);
        }

        // GET: Companies/Details/5
        public ActionResult Details(long? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Company company = db.Companies.Find(id);
            //if (company == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(company);
            return View();
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Company company)
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.GetUserId();
                var result = wcf.SaveCompany(company.Name, user);

                if (result.Id == -1)
                {
                    //Compania repetida
                    ViewBag.Error = 1;
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }

            return View(company);
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(long? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Company company = db.Companies.Find(id);
            //if (company == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(company);
            return View();
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost]
        public JsonResult Delete(long? id)
        {
            string user = User.Identity.GetUserId();
            bool company = wcf.deactivateCompany((long)id, user);

            return Json(new { deleted = company });
        }

        // POST: Companies/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    Company company = db.Companies.Find(id);
        //    db.Companies.Remove(company);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult editCompany(string name, long id)
        {
            string user = User.Identity.GetUserId();
            bool response = wcf.editCompany(name, id, user);

            return Json(new { saved = true });
        }

        public JsonResult getInfo(long id) {

            var result = wcf.getCompanyInfo(id);

            return Json(result,JsonRequestBehavior.AllowGet);
        }
    }
}
