﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SDSP.Models;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using SDSP.Class;
using System.Text;
using SpreadsheetLight;

namespace SDSP.Controllers
{
    [Authorize]
    public class IngatesController : Controller
    {
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        //private SDSPEntities db = new SDSPEntities();
        DataSourceHelper dsh = new DataSourceHelper();

        // GET: Ingates
        public ActionResult Index()
        {
            return View();
        }

        // GET: Ingates/Details/5
        public ActionResult Details(long? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            Ingate ingate = new Ingate();
            //if (ingate == null)
            //{
            //    return HttpNotFound();
            //}
            return View(ingate);
        }       

        // GET: Ingates/Create
        public ActionResult Create(int? error)
        {
            ViewBag.YardSpotId = dsh.getSpots();

            ViewBag.DriverId = dsh.getDrivers();

            ViewBag.CompanyId = dsh.getCompanies();

            ViewBag.Bound = dsh.getBound();

            ViewBag.TrailerStatusId = dsh.getTrailerStatus();

            ViewBag.Hazmat = dsh.getHazmat();

            if (error == null)
            {
                ViewBag.Error = -1;
            }
            else
            {
                ViewBag.Error = (error > 0) ? error : 0;
            }

            return View();
        }

        // POST: Ingates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ingate ingate)
        {
            ingate.AspNetUsersId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                long? driverId = ingate.DriverId;
                long? ingateId = 0;

                ingateId = (ingate.RouteMasterId > 0) ? wcf.SaveIngate(ingate.RouteMasterId, ingate.AspNetUsersId, ingate.YardSpotId, ingate.DriverId, ingate.Bound, ingate.TrailerStatusId) : wcf.saveManualIngate(ingate.AspNetUsersId, ingate.YardSpotId, ingate.Hazmat, ingate.DriverId, Request["TrailerNumber"], Request["Route"], Request["Sequence"],"0", "", ingate.Bound, ingate.TrailerStatusId);
                if (ingateId > -1)
                {
                    if (wcf.SaveIngateComment(Request["Comments"], ingateId, ingate.AspNetUsersId))
                    {                                                
                        bool hook = (Request["Hook"] != "false");
                        bool print = (Request["Print"] != "false");

                        if (!hook)
                        {
                            return (!print) ? RedirectToAction("Create", new { error = 0 }) : RedirectToAction("PrintHook", "PreOutgates", new { id = 0, ingateid = ingateId });
                        }
                        else
                        {
                            return RedirectToAction("create", "PreOutgates", new { ingateid = ingateId, driverid = driverId, print = print });
                        }
                    }
                }
                else
                {
                    if (ingateId == -2)
                    {
                        //Spot repetido
                        ViewBag.Error = 2;
                    }
                    else if (ingateId == -1)
                    {
                        //Caja repetida
                        ViewBag.Error = 1;
                    }
                }
            }
            else {
                //Missing info
                ViewBag.Error = 3;
            }

            ViewBag.YardSpotId = dsh.getSpots();

            ViewBag.DriverId = dsh.getDrivers();

            ViewBag.CompanyId = dsh.getCompanies();

            ViewBag.Bound = dsh.getBound();

            ViewBag.TrailerStatusId = dsh.getTrailerStatus();

            ViewBag.Hazmat = dsh.getHazmat();

            return View(ingate);
        }

        // GET: Ingates/Edit/5
        public ActionResult Edit(long? id)
        {

            IngateTrailersVM itObj = new IngateTrailersVM();

            ViewBag.YardSpotId = dsh.getEditingateSpots(id);

            ViewBag.DriverId = dsh.getDrivers();

            ViewBag.CompanyId = dsh.getCompanies();

            ViewBag.Bound = dsh.getBound();

            ViewBag.TrailerStatusId = dsh.getTrailerStatus();

            var itResult = wcf.getIngatebyId(id);

            itObj.Ingate = new Ingate {
                Id = itResult.Ingate.Id,
                DateTime = itResult.Ingate.DateTime,
                DriverId = itResult.Ingate.DriverId,
                Bound = itResult.Ingate.Bound,
                TrailerStatusId = itResult.Ingate.TrailerStatusId
            };
            itObj.RouteMaster = new RouteMaster {
                Id = itResult.RouteMaster.Id,
                TrailerNumber = itResult.RouteMaster.TrailerNumber,
                Hazmat = itResult.RouteMaster.Hazmat,
                Route = itResult.RouteMaster.Route,
                Run = itResult.RouteMaster.Run,
                Priority = itResult.RouteMaster.Priority,
                Invoice = itResult.RouteMaster.Invoice
            };

            //ViewBag.Hazmat = dsh.getHazmat();

            return View(itObj);
        }

        // POST: Ingates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RouteMasterId,DateTime,UserId,YardSpotId")] Ingate ingate)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Entry(ingate).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //ViewBag.YardSpotId = new SelectList(db.YardSpots, "Id", "TrailerNumber", ingate.YardSpotId);
            return View(ingate);
        }

        // GET: Ingates/Delete/5
        public JsonResult Delete(long? id)
        {
            string user = User.Identity.GetUserId();
            int ingate = wcf.deleteIngate(id, user);

            return Json(new { deleted = ingate });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult FindDrivers(string term) {
            //var resultado = db.Drivers.Where(x => x.Name.Contains(term)).Select(x => x.Name).Take(5).ToList();
            return Json("kk", JsonRequestBehavior.AllowGet);
        }

        public JsonResult getRouteMasterData(string trailer) {
            List<NorthTrailer> lstNorthTrailer = new List<NorthTrailer>();
            string trailerWithSpaces = Regex.Replace(trailer, @"(\p{L})(\d)", "$1 $2");

            var wcfResult = wcf.getRouteMasterDatabyTrailerNumber(trailerWithSpaces);

            if (wcfResult.Count() == 0)
            {
                wcfResult = wcf.getRouteMasterDatabyTrailerNumber(trailer);
            }

            foreach (var item in wcfResult)
            {
                RouteMaster RouteMaster = new RouteMaster {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    Run = item.Run,
                    Route = item.Route,
                    Priority = item.Priority,
                    UnloadDate = item.UnloadDate,
                    Hazmat = item.Hazmat,
                    Invoice = item.Invoice,
                    DataSourceId = item.DataSourceId,
                };

                Northbound north = new Northbound { };
                if (RouteMaster.DataSourceId == 9)
                {
                    //Viene de LLP
                    var n = wcf.getNorthInfo(RouteMaster.Id);
                    north = new Northbound
                    {
                        RouteMasterId = n.RouteMasterId,
                        TrailerStatusId = n.TrailerStatusId,
                        DriverId = n.DriverId
                    };

                }
                else
                {
                    north = null;
                }

                NorthTrailer nTrailer = new NorthTrailer {
                    RouteMaster = RouteMaster,
                    Northbound = north
                };
                
               
                lstNorthTrailer.Add(nTrailer);
            }
            return Json(lstNorthTrailer, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSingleRouteMasterData(long routeMasterId)
        {
            var wcfResult = wcf.getSingleRouteMasterData(routeMasterId);
            RouteMaster rM = new RouteMaster {
                Id = wcfResult.Id,
                TrailerNumber = wcfResult.TrailerNumber,
                Run = wcfResult.Run,
                Route = wcfResult.Route,
                Priority = wcfResult.Priority,
                UnloadDate = wcfResult.UnloadDate,
                Hazmat = wcfResult.Hazmat,
                Invoice = wcfResult.Invoice,
                DataSourceId = wcfResult.DataSourceId,
            };

            Northbound north = new Northbound { };
            if (rM.DataSourceId == 9)
            {
                //Viene de LLP
                var n = wcf.getNorthInfo(rM.Id);
                north = new Northbound {
                    RouteMasterId = n.RouteMasterId,
                    TrailerStatusId = n.TrailerStatusId,
                    DriverId = n.DriverId
                };

            }
            else {
                north = null;
            }

            NorthTrailer trailerInfo = new NorthTrailer {
                RouteMaster = rM,
                Northbound = north
            };

            return Json(trailerInfo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveDriver(string driverName, string companyId) {
            Driver res = new Driver();
            string user = User.Identity.GetUserId();
            var queryres = wcf.SaveDriver(driverName, companyId, "SDSP", user);
            res.Id = queryres.Id;
            res.Name = queryres.Name;
            return Json(res, JsonRequestBehavior.AllowGet);                        
        }

        public JsonResult saveCompany(string companyName)
        {
            Company res = new Company();
            string user = User.Identity.GetUserId();
            var queryres = wcf.SaveCompany(companyName, user);
            res.Id = queryres.Id;
            res.Name = queryres.Name;
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ingatesReport() {
            IngatesReportVM ingatesReport = new IngatesReportVM();
            List<IngatesReport> iList = new List<IngatesReport>();
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1).Date;
            var result = wcf.IngatesReport(start, end);

            foreach (var item in result)
            {
                IngatesReport iObj = new IngatesReport
                {
                    YardSpotId = item.YardSpotId,
                    Id = item.Id,
                    IngateRouteMasterId = item.IngateRouteMasterId,
                    IngateDriverId = item.IngateDriverId,
                    IngateDateTime = item.IngateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound,
                    TrailerStatusName = item.TrailerStatusName
                };
                iList.Add(iObj);
            }

            ingatesReport.startDate = start;
            ingatesReport.endDate = end;
            ingatesReport.ingatesReport = iList;

            return View(ingatesReport);            
        }

        [HttpPost]
        public ActionResult ingatesReport(IngatesReportVM ingatesReport)
        {
            IngatesReportVM iReport = new IngatesReportVM();
            List<IngatesReport> iList = new List<IngatesReport>();
            DateTime end = ingatesReport.endDate.AddMinutes(1439);
            var result = wcf.IngatesReport(ingatesReport.startDate, end);

            foreach (var item in result)
            {
                IngatesReport iObj = new IngatesReport
                {
                    YardSpotId = item.YardSpotId,
                    Id = item.Id,
                    IngateRouteMasterId = item.IngateRouteMasterId,
                    IngateDriverId = item.IngateDriverId,
                    IngateDateTime = item.IngateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound,
                    TrailerStatusName = item.TrailerStatusName
                };

                iList.Add(iObj);
            }

            iReport.startDate = ingatesReport.startDate;
            iReport.endDate = ingatesReport.endDate;
            iReport.ingatesReport = iList;

            return View(iReport);
        }

        public FileResult Export(string start, string end)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            if (start == "" && end == "")
            {
                startDate = DateTime.Now.Date;
                endDate = startDate.AddDays(1).Date;
            }
            else
            {
                startDate = DateTime.Parse(start);
                endDate = DateTime.Parse(end);
                endDate = endDate.AddDays(1).Date;
            }
            var result = wcf.IngatesReport(startDate, endDate);
            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "IngateReport-" + dateStamp + ".xlsx";
            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument oSLDocument = new SLDocument();

            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Route", typeof(string));
            dt.Columns.Add("Sequence", typeof(string));
            dt.Columns.Add("Unload Date", typeof(string));
            dt.Columns.Add("Ingate Date & Time", typeof(string));
            dt.Columns.Add("Driver", typeof(string));
            dt.Columns.Add("Bound", typeof(string));
            dt.Columns.Add("Trailer Status", typeof(string));            

            foreach (var item in result)
            {
                string bound = (item.Bound == 1) ? "Southbound" : "Northbound";
                dt.Rows.Add(item.TrailerNumber, item.Route, item.Run, item.UnloadDate, item.IngateDateTime, item.DriverName, bound, item.TrailerStatusName);
            }

            oSLDocument.ImportDataTable(1, 1, dt, true);

            oSLDocument.SaveAs(path);
            
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public JsonResult reloadIngatesReport(string startDate, string endDate)
        {
            IngatesReportVM ingatesReport = new IngatesReportVM();
            List<IngatesReport> iList = new List<IngatesReport>();
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            if (startDate == "" && endDate == "")
            {
                start = DateTime.Now.Date;
                end = start.AddDays(1).Date;
            }
            else
            {
                start = DateTime.Parse(startDate);
                end = DateTime.Parse(endDate);
                end = end.AddDays(1).Date;
            }
            var result = wcf.IngatesReport(start, end);

            foreach (var item in result)
            {
                IngatesReport iObj = new IngatesReport
                {
                    YardSpotId = item.YardSpotId,
                    Id = item.Id,
                    IngateRouteMasterId = item.IngateRouteMasterId,
                    IngateDriverId = item.IngateDriverId,
                    IngateDateTime = item.IngateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound,
                    TrailerStatusName = item.TrailerStatusName
                };

                iList.Add(iObj);
            }

            ingatesReport.startDate = start;
            ingatesReport.endDate = end;
            ingatesReport.ingatesReport = iList;

            return Json(ingatesReport,JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkSpot(int yardspot) {
            var res = wcf.checkspot(yardspot);

            ViewBag.resultado = res;

            return View();
        }

    }
}
