﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SDSP.Class;
using SDSP.Models;
using Microsoft.AspNet.Identity;

namespace SDSP.Controllers
{
    [Authorize]
    public class IssuesController : Controller
    {
        DataSourceHelper dsh = new DataSourceHelper();
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        // GET: Issues
        public ActionResult Index()
        {
            List<Issue> lstRecord = new List<Issue>();
            var lst = wcf.GetIssuesList();
            var statuses = wcf.getIssueStatuses();

            Dictionary<int, string> statusNames = new Dictionary<int, string>();
            foreach (var item in statuses) {
                statusNames.Add(item.Id, item.Name);
            }

            ViewBag.statusNames = statusNames;

            foreach (var item in lst)
            {
                Issue issue = new Issue {
                    Id = item.Id,
                    Reason = item.Reason,
                    Involved = item.Involved,
                    Solution = item.Solution,
                    DateTime =  item.DateTime,
                    IssueStatusId = (int)item.IssueStatusId
                };

                lstRecord.Add(issue);
            }

            return View(lstRecord);
        }

        public ActionResult showResult()
        {
            List<Issue> lstRecord = new List<Issue>();
            var lst = wcf.GetIssuesList();
            var statuses = wcf.getIssueStatuses();

            Dictionary<int, string> statusNames = new Dictionary<int, string>();
            foreach (var item in statuses)
            {
                statusNames.Add(item.Id, item.Name);
            }

            ViewBag.statusNames = statusNames;

            foreach (var item in lst)
            {
                Issue issue = new Issue
                {
                    Id = item.Id,
                    Reason = item.Reason,
                    Involved = item.Involved,
                    Solution = item.Solution,
                    DateTime = item.DateTime,
                    IssueStatusId = (int)item.IssueStatusId
                };

                lstRecord.Add(issue);
            }

            return View(lstRecord);
        }

        // GET: Issues/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Issues/Create
        public ActionResult Create()
        {
            ViewBag.IssueStatusId = dsh.getIssueStatus();

            return View();
        }

        // POST: Issues/Create
        [HttpPost]
        public ActionResult Create(Issue formDate)
        {
            try
            {
                string user = User.Identity.GetUserId();
                bool result = wcf.saveIssue(formDate.Reason,formDate.Involved,formDate.Solution,formDate.IssueStatusId,user);
                    
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.IssueStatusId = dsh.getIssueStatus();
                return View();
            }            
        }

        // GET: Issues/Edit/5
        public ActionResult Edit(int id)
        {
            var record = wcf.getIssueRecord(id);            

            Issue issue = new Issue {
                Id = record.Id,
                Reason = record.Reason,
                Solution = record.Solution,
                Involved = record.Involved,
                IssueStatusId = (int)record.IssueStatusId,
                DateTime = record.DateTime
            };

            ViewBag.IssueStatusId = dsh.getSelectedIssueStatus(issue.IssueStatusId);

            return View(issue);
        }

        // POST: Issues/Edit/5
        [HttpPost]
        public ActionResult Edit(Issue formData)
        {
            bool updated = wcf.updateIssue(formData.Reason, formData.Involved, formData.Solution, formData.IssueStatusId, "Test", formData.Id);
            
            if (updated) {                
                return RedirectToAction("Index");
            } else {
                var record = wcf.getIssueRecord(formData.Id);

                Issue issue = new Issue
                {
                    Id = record.Id,
                    Reason = record.Reason,
                    Solution = record.Solution,
                    Involved = record.Involved,
                    IssueStatusId = (int)record.IssueStatusId,
                    DateTime = record.DateTime
                };

                ViewBag.IssueStatusId = dsh.getSelectedIssueStatus(issue.IssueStatusId);

                return View(formData);
            }
        }

        // GET: Issues/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Issues/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
