﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SDSP.Models;
using SDSP.Class;
using Microsoft.AspNet.Identity;

namespace SDSP.Controllers
{
    [Authorize]
    public class DriversController : Controller
    {
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        DataSourceHelper dsh = new DataSourceHelper();

        // GET: Drivers
        public ActionResult Index()
        {
            int type = 0;
            if (User.IsInRole("LLPMEMBER_TRANSPORTE")) {
                //Si es de bc mostrar todos?
                type = 3;
            } else {
                //Si es de SDSP mostrar solo SDSP
                type = 2;
            }

            var drivers = wcf.GetDriverCompanyList(type);

            List<DriverCompanyVM> driversList = new List<DriverCompanyVM>();
            foreach (var driver in drivers) {
                DriverCompanyVM driverObj = new DriverCompanyVM {
                    Driver = new Driver {
                        Id = driver.Driver.Id,
                        Name = driver.Driver.Name,
                        IdCompany = driver.Driver.IdCompany,
                        DriverCode = driver.Driver.DriverCode
                    },
                    Company = new Company {
                        Id = driver.Company.Id,
                        Name = driver.Company.Name
                    }
                };

                driversList.Add(driverObj);
            }

            ViewBag.IdCompany = dsh.getCompanies();

            return View(driversList);
        }

        public JsonResult reloadIndex()
        {
            int type = 0;
            if (User.IsInRole("LLPMEMBER_TRANSPORTE"))
            {
                //Si es de bc mostrar todos?
                type = 3;
            }
            else
            {
                //Si es de SDSP mostrar solo SDSP
                type = 2;
            }

            var drivers = wcf.GetDriverCompanyList(type);
            List<DriverCompanyVM> driversList = new List<DriverCompanyVM>();
            foreach (var driver in drivers)
            {
                DriverCompanyVM driverObj = new DriverCompanyVM
                {
                    Driver = new Driver
                    {
                        Id = driver.Driver.Id,
                        Name = driver.Driver.Name,
                        IdCompany = driver.Driver.IdCompany,
                        DriverCode = driver.Driver.DriverCode
                    },
                    Company = new Company
                    {
                        Id = driver.Company.Id,
                        Name = driver.Company.Name
                    }
                };

                driversList.Add(driverObj);
            }

            return Json(driversList,JsonRequestBehavior.AllowGet);
        }

        // GET: Drivers/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = new Driver();
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        // GET: Drivers/Create
        public ActionResult Create()
        {
            ViewBag.IdCompany = dsh.getCompanies();

            return View();
        }

        // POST: Drivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Driver driver)
        {
            if (ModelState.IsValid)
            {
                string user = User.Identity.GetUserId();
                var result =  wcf.SaveDriver(driver.Name,driver.IdCompany.ToString(),driver.DriverCode, user);

                if (result.Id == -1) {
                    //Chofer repetida
                    ViewBag.Error = 1;
                } else{
                    return RedirectToAction("Index");
                }
            }
            

            ViewBag.IdCompany = dsh.getCompanies();

            return View(driver);
        }

        // GET: Drivers/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Driver driver = new Driver();
            if (driver == null)
            {
                return HttpNotFound();
            }
            //ViewBag.IdCompany = new SelectList(db.Companies, "Id", "Name", driver.IdCompany);
            return View(driver);
        }

        // POST: Drivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdCamex,Name,IdCompany")] Driver driver)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(driver).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.IdCompany = new SelectList(db.Companies, "Id", "Name", driver.IdCompany);
            return View(driver);
        }

        // POST: Companies/Delete/5
        [HttpPost]
        public JsonResult Delete(long? id)
        {
            string user = User.Identity.GetUserId();
            bool driver = wcf.deactivateDriver((long)id, user);

            return Json(new { deleted = driver });
        }

        // GET: Drivers/Delete/5
        //public ActionResult Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Driver driver = new Driver();
        //    if (driver == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(driver);
        //}

        // POST: Drivers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    //Driver driver = db.Drivers.Find(id);
        //    //db.Drivers.Remove(driver);
        //    //db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult getInfo(long id) {

            var driver = wcf.getDriverInfo(id); 

            return Json(driver,JsonRequestBehavior.AllowGet);
        }

        public JsonResult editDriver(string name, string code, long id, int company) {
            string user = User.Identity.GetUserId();
            bool response = wcf.editDriver(name,code,id,company, user);

            return Json( new { saved= true} );
        }
    }
}
