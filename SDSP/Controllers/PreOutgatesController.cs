﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SDSP.Models;
using Microsoft.AspNet.Identity;
using SDSP.Class;
using SpreadsheetLight;

namespace SDSP.Controllers
{
    [Authorize]
    public class PreOutgatesController : Controller
    {
        private SDSPEntities db = new SDSPEntities();
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        DataSourceHelper dsh = new DataSourceHelper();
        // GET: PreOutgates
        public ActionResult Index()
        {
            //var preOutgates = db.PreOutgates;
            //return View(preOutgates.ToList());
            return View();
        }

        // GET: PreOutgates/Details/5
        public ActionResult Details(long? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //PreOutgate preOutgate = db.PreOutgates.Find(id);
            //if (preOutgate == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(preOutgate);
            return View();
        }

        // GET: PreOutgates/Create
        public ActionResult Create(long? ingateid, long? driverid, int? error, bool? print)
        {
            if (ingateid == null)
            {
                ViewBag.prevIngate = 0;
                ViewBag.DriverId = dsh.getDrivers();
                ViewBag.CompanyId = dsh.getCompanies();
                ViewBag.Bound = dsh.getBound();
            }
            else {
                //PrevIngate es porque el preoutgate viene directamente de la opcion Hook de Ingate.
                //Se usa el mismo driver ya que no es correcto pedirlo nuevamente
                ViewBag.Bound = dsh.getBound();
                ViewBag.prevIngate = ingateid;
                ViewBag.driverId = driverid;
            }

            ViewBag.Error = (error == null) ? 0 : -1;
            
            if (print != null)
            {
                ViewBag.Print = ((bool)print) ? 1 : 0;
            }

            return View();
        }

        // POST: PreOutgates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PreOutgate preOutgate)
        {            
            preOutgate.AspNetUsersId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                long? previousIngate = int.Parse(Request["prevIngate"]);
                long? preOutGateId = wcf.SavePreOutGate(preOutgate.RouteMasterId, preOutgate.DriverId, preOutgate.AspNetUsersId);
                if (preOutGateId > 0)
                {
                    return (Request["PrintInstruction"] == "false") ? RedirectToAction("create", new { error = -1 }) : RedirectToAction("PrintHook", new { id = preOutGateId, ingateid = previousIngate });
                }
                else
                {
                    //Se intento hacer un hook a una caja por segunda ocasion
                    ViewBag.Error = 1;
                }
            }
            else {
                //Validacion con errores
                ViewBag.Error = 2;
            }

            ViewBag.DriverId = dsh.getDrivers();
            ViewBag.CompanyId = dsh.getCompanies();
            ViewBag.Bound = dsh.getBound();

            return View(preOutgate);
        }

        public ActionResult PrintHook(long? id, long? ingateid) 
        {
            PrintHooKTotalVM vmObj = new PrintHooKTotalVM();
            if (id != null && id > 0) {
                
                var hookObj = wcf.printHook(id);

                PrintHookVM ph = new PrintHookVM {
                    Ingate = new Ingate { Id = hookObj.Ingate.Id, YardSpotId = hookObj.Ingate.YardSpotId },
                    Company = new Company { Id = hookObj.Company.Id, Name = hookObj.Company.Name },
                    Driver = new Driver { Id = hookObj.Driver.Id, Name = hookObj.Driver.Name },
                    RouteMaster = new RouteMaster { Id = hookObj.RouteMaster.Id, TrailerNumber = hookObj.RouteMaster.TrailerNumber }
                };

                vmObj.PrintHookVM = ph;
            }
            if (ingateid != null && ingateid > 0)
            {
                var dropObj = wcf.printDrop(ingateid);
                PrintDropVM pd = new PrintDropVM
                {
                    Ingate = new Ingate { Id = dropObj.Ingate.Id, YardSpotId = dropObj.Ingate.YardSpotId },
                    Company = new Company { Id = dropObj.Company.Id, Name = dropObj.Company.Name },
                    Driver = new Driver { Id = dropObj.Driver.Id, Name = dropObj.Driver.Name },
                    RouteMaster = new RouteMaster { Id = dropObj.RouteMaster.Id, TrailerNumber = dropObj.RouteMaster.TrailerNumber }
                };

                vmObj.PrintDropVM = pd;
            }

            return View(vmObj);
        }

        // GET: PreOutgates/Edit/5
        public ActionResult Edit(long? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //PreOutgate preOutgate = db.PreOutgates.Find(id);
            //if (preOutgate == null)
            //{
            //    return HttpNotFound();
            //}
            //ViewBag.DriverId = new SelectList(db.Drivers, "Id", "Name", preOutgate.DriverId);
            //return View(preOutgate);
            return View();
        }

        // POST: PreOutgates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DateTime,TrailerNumber,DriverId,AspNetUsersId")] PreOutgate preOutgate)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Entry(preOutgate).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //ViewBag.DriverId = new SelectList(db.Drivers, "Id", "Name", preOutgate.DriverId);
            //return View(preOutgate);
            return View();
        }

        // GET: PreOutgates/Delete/5
        public ActionResult Deleteoriginal(long? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //PreOutgate preOutgate = db.PreOutgates.Find(id);
            //if (preOutgate == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(preOutgate);
            return View();
        }

        // POST: PreOutgates/Delete/5
        //[HttpPost, ActionName("Delete")]        
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    PreOutgate preOutgate = db.PreOutgates.Find(id);
        //    db.PreOutgates.Remove(preOutgate);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult getTrailers(int tipo) {
            List<IngateTrailersVM> ingateTrailersList = new List<IngateTrailersVM>();
            var resultado = wcf.GetIngateTrailers(tipo);

            foreach (var item in resultado) {
                IngateTrailersVM ingateTrailersObj = new IngateTrailersVM();
                ingateTrailersObj.RouteMaster = new RouteMaster
                {
                    TrailerNumber = item.RouteMaster.TrailerNumber,
                    UnloadDate = item.RouteMaster.UnloadDate,
                    Priority = item.RouteMaster.Priority,
                    Route = item.RouteMaster.Route,
                    Run = item.RouteMaster.Run,
                    Id = item.RouteMaster.Id,
                    Invoice = item.RouteMaster.Invoice,
                    Hazmat = item.RouteMaster.Hazmat
                };
                ingateTrailersObj.Comment = new Comment {
                    Comment1 = item.Comment.Comment1
                };
                ingateTrailersObj.Ingate = new Ingate
                {
                    Id = item.Ingate.Id,
                    YardSpotId = item.Ingate.YardSpotId,
                    TrailerStatusId = item.Ingate.TrailerStatusId
                };
                ingateTrailersList.Add(ingateTrailersObj);
            }                           

            return Json(ingateTrailersList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkAssignments() {
            List<PreoutgateReport> pList = new List<PreoutgateReport>();

            var result = wcf.PreoutgatesReport();

            foreach (var item in result) {
                PreoutgateReport pObj = new PreoutgateReport {
                    YardSpotId = item.YardSpotId,
                    PreoutgateId = item.PreoutgateId,
                    PreoutgateRouteMasterId = item.PreoutgateRouteMasterId,
                    PreoutgateDriverId = item.PreoutgateDriverId,
                    PreoutgateDateTime = item.PreoutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound,
                    TrailerStatusName = item.TrailerStatusName
                };

                pList.Add(pObj);
            };

            return View(pList);
        }

        public JsonResult Delete(long? id)
        {
            string user = User.Identity.GetUserId();
            int hook = wcf.deleteHook(id, user);

            return Json(new { deleted = hook });
        }

        public JsonResult reloadPreoutgatesReport()
        {
            List<PreoutgateReport> pList = new List<PreoutgateReport>();

            var result = wcf.PreoutgatesReport();

            foreach (var item in result)
            {
                PreoutgateReport pObj = new PreoutgateReport
                {
                    YardSpotId = item.YardSpotId,
                    PreoutgateId = item.PreoutgateId,
                    PreoutgateRouteMasterId = item.PreoutgateRouteMasterId,
                    PreoutgateDriverId = item.PreoutgateDriverId,
                    PreoutgateDateTime = item.PreoutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound,
                    TrailerStatusName = item.TrailerStatusName
                };

                pList.Add(pObj);
            };

            return Json(pList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Export()
        {
            var result = wcf.PreoutgatesReport();

            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "HookReport-" + dateStamp + ".xlsx";
            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument oSLDocument = new SLDocument();

            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Route", typeof(string));
            dt.Columns.Add("Sequence", typeof(string));
            dt.Columns.Add("Unload Date", typeof(string));
            dt.Columns.Add("Hook Date & Time", typeof(string));
            dt.Columns.Add("Driver", typeof(string));
            dt.Columns.Add("Spot", typeof(int));
            dt.Columns.Add("Bound", typeof(string));
            dt.Columns.Add("Trailer Status", typeof(string));

            foreach (var item in result)
            {
                string bound = (item.Bound == 1) ? "Southbound" : "Northbound";
                dt.Rows.Add(item.TrailerNumber, item.Route, item.Run, item.UnloadDate, item.PreoutgateDateTime, item.DriverName,item.YardSpotId, bound, item.TrailerStatusName);
            }

            oSLDocument.ImportDataTable(1, 1, dt, true);

            oSLDocument.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}
