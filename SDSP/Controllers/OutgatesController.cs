﻿using Microsoft.AspNet.Identity;
using SDSP.Class;
using SDSP.Models;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SDSP.Controllers
{
    [Authorize]
    public class OutgatesController : Controller
    {
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        DataSourceHelper dsh = new DataSourceHelper();
        // GET: Outgates
        public ActionResult Index()
        {
            return View();
        }

        // GET: Outgates/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        
        // GET: Outgates/Create
        public ActionResult Create()
        {
            ViewBag.DriverId = dsh.getDrivers();

            ViewBag.CompanyId = dsh.getCompanies();

            return View();
        }        

        // POST: Outgates/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,RouteMasterId,DateTime")] Outgate outgate)
        {
            try
            {
                // TODO: Add insert logic here
                

                //return RedirectToAction("Index");
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Outgates/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Outgates/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        // GET: Outgates/Delete/5
        public JsonResult Delete(int id)
        {
            string user = User.Identity.GetUserId();
            bool outgate = wcf.deleteDispatch(id, user);
            return Json(new { deleted = outgate });
        }

        public JsonResult GetTrailers()
        {
            var trailerlst = wcf.GetPreoutgateTrailers();

            List<PrintHookVM> Trailers = new List<PrintHookVM>();

            foreach (var item in trailerlst)
            {
                PrintHookVM phObj = new PrintHookVM();

                phObj.Driver = new Driver
                {
                    Id = item.Driver.Id,
                    Name = item.Driver.Name
                };
                phObj.PreOutgate = new PreOutgate
                {
                    Id = item.PreOutgate.Id
                };
                phObj.Company = new Company
                {
                    Id = item.Company.Id,
                    Name = item.Company.Name
                };
                phObj.RouteMaster = new RouteMaster
                {
                    Id = item.RouteMaster.Id,
                    Route = item.RouteMaster.Route,
                    Seq = item.RouteMaster.Run,
                    Priority = item.RouteMaster.Priority,
                    UnloadDate = item.RouteMaster.UnloadDate,
                    TrailerNumber = item.RouteMaster.TrailerNumber,
                    ExpressPriority = item.RouteMaster.ExpressPriority,
                    Comments = item.RouteMaster.Comments
                };

                phObj.Ingate = new Ingate {
                    Id = item.Ingate.Id,
                    YardSpotId = item.Ingate.YardSpotId
                };
                Trailers.Add(phObj);
            }

            return Json(Trailers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveDispatch(IDictionary<string,int> trailers) {
            var newDict = new Dictionary<string, int>(trailers);

            string user = User.Identity.GetUserId();
            var savedOutgates = wcf.saveDispatches(newDict, user);                                               

            return Json(new { saved = savedOutgates });
        }

        public JsonResult updateDriver(int preoutgateId, int newDriver) {
            string user = User.Identity.GetUserId();
            var updated = wcf.updateAssignedDriver(preoutgateId, newDriver, user);

            return Json(new { wasupdated = updated});
        }

        public JsonResult assignDriver(long? routeMasterid, long? newDriver) {
            string AspNetUsersId = User.Identity.GetUserId();
            var assigned = wcf.SavePreOutGate(routeMasterid, newDriver, AspNetUsersId);

            return Json(new { wasupdated = assigned });
        }

        public JsonResult saveSkipReasons(int routeMasterId, string reason) {
            string user = User.Identity.GetUserId();
            bool result = wcf.saveSkipReasons(routeMasterId,reason, user);

            return Json(new { saved = result });
        }


        public ActionResult outgatesReport()
        {
            OutgatesReportVM oReport = new OutgatesReportVM();
            List<OutgatesReport> oList = new List<OutgatesReport>();
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1).Date;
            var result = wcf.OutgatesReport(start, end);

            foreach (var item in result)
            {
                OutgatesReport oObj = new OutgatesReport
                {
                    Id = item.Id,
                    OutgateRouteMasterId = item.OutgateRouteMasterId,
                    OutgateDriverId = item.OutgateDriverId,
                    OutgateDateTime = item.OutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound
                };

                oList.Add(oObj);
            }

            oReport.startDate = start;
            oReport.endDate = end;
            oReport.outgatesReport = oList;

            ViewBag.YardSpotId = dsh.getSpotstoChange();

            return View(oReport);
        }

        [HttpPost]
        public ActionResult outgatesReport(OutgatesReportVM outgatesReport)
        {
            OutgatesReportVM oReport = new OutgatesReportVM();
            List<OutgatesReport> oList = new List<OutgatesReport>();
            DateTime end = outgatesReport.endDate.AddMinutes(1439);
            var result = wcf.OutgatesReport(outgatesReport.startDate, end);

            foreach (var item in result)
            {
                OutgatesReport oObj = new OutgatesReport
                {
                    Id = item.Id,
                    OutgateRouteMasterId = item.OutgateRouteMasterId,
                    OutgateDriverId = item.OutgateDriverId,
                    OutgateDateTime = item.OutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound
                };

                oList.Add(oObj);
            }

            oReport.startDate = outgatesReport.startDate;
            oReport.endDate = outgatesReport.endDate;
            oReport.outgatesReport = oList;

            ViewBag.YardSpotId = dsh.getSpotstoChange();

            return View(oReport);
        }

        public JsonResult changeSpot(long? routeMasterId, int newSpot)
        {
            string user = User.Identity.GetUserId();
            bool saved = wcf.changeDispatchSpot(routeMasterId, newSpot, user);

            return Json(new { saved = saved });
        }

        public FileResult Export(string start, string end)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            if (start == "" && end == "")
            {
                startDate = DateTime.Now.Date;
                endDate = startDate.AddDays(1).Date;
            }            
            else
            {
                startDate = DateTime.Parse(start);
                endDate = DateTime.Parse(end);
                endDate = endDate.AddDays(1).Date;
            }
            var result = wcf.OutgatesReport(startDate, endDate);

            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "OutgateReport-"+dateStamp+".xlsx";
            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument oSLDocument = new SLDocument();

            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Route", typeof(string));
            dt.Columns.Add("Sequence", typeof(string));
            dt.Columns.Add("Unload Date", typeof(string));
            dt.Columns.Add("Outgate Date & Time", typeof(string));
            dt.Columns.Add("Driver", typeof(string));
            dt.Columns.Add("Bound", typeof(string));

            foreach (var item in result)
            {
                string bound = (item.Bound == 1) ? "Southbound" : "Northbound";
                dt.Rows.Add(item.TrailerNumber, item.Route, item.Run, item.UnloadDate, item.OutgateDateTime, item.DriverName, bound);
            }

            oSLDocument.ImportDataTable(1, 1, dt, true);

            oSLDocument.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public JsonResult reloadoutgatesReport(string startDate, string endDate)
        {
            OutgatesReportVM oReport = new OutgatesReportVM();
            List<OutgatesReport> oList = new List<OutgatesReport>();

            DateTime start = new DateTime();
            DateTime end = new DateTime();
            if (startDate == "" && endDate == "")
            {
                start = DateTime.Now.Date;
                end = start.AddDays(1).Date;
            }
            else
            {
                start = DateTime.Parse(startDate);
                end = DateTime.Parse(endDate);
                end = end.AddDays(1).Date;
            }
            var result = wcf.OutgatesReport(start, end);

            foreach (var item in result)
            {
                OutgatesReport oObj = new OutgatesReport
                {
                    Id = item.Id,
                    OutgateRouteMasterId = item.OutgateRouteMasterId,
                    OutgateDriverId = item.OutgateDriverId,
                    OutgateDateTime = item.OutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound
                };

                oList.Add(oObj);
            }

            oReport.startDate = start;
            oReport.endDate = end;
            oReport.outgatesReport = oList;

            return Json(oReport,JsonRequestBehavior.AllowGet);
        }

    }    
}
