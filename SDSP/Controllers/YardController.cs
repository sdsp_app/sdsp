﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SDSP.Models;
using SDSP.Class;
using SpreadsheetLight;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNet.Identity;

namespace SDSP.Controllers
{
    [Authorize]
    public class YardController : Controller
    {
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        DataSourceHelper dsh = new DataSourceHelper();
        // GET: Yard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Inventory()
        {
            return View();
        }

        public ActionResult SpotManager()
        {
            List<YardSpot> SpotsList = new List<YardSpot>();

            var spotlst = wcf.getYardSpots(0);

            foreach (var item in spotlst)
            {
                YardSpot lst = new YardSpot();
                lst.Id = item.Id;
                lst.SpotNumber = item.SpotNumber;
                lst.SpotStatusId = item.SpotStatusId;
                SpotsList.Add(lst);
            }

            return View(SpotsList);
        }

        public JsonResult blockspots(int spot, int status, string comment)
        {
            string user = User.Identity.GetUserId();

            bool result = wcf.blockspots(spot, status, comment, user);


            return Json(new { saved = result });
        }

        public JsonResult getReason(int spot)
        {

            string reason = wcf.getReason(spot);

            return Json(new { reason = reason });
        }

        public ActionResult YardStatus()
        {
            List<YardSpot> SpotsList = new List<YardSpot>();

            SpotsList = dsh.getAllSpots();

            List<PrintHookVM> lstContenedoresEnYarda = new List<PrintHookVM>();

            var activeIngates = wcf.getactiveIngates();

            foreach (var item in activeIngates)
            {
                PrintHookVM phObj = new PrintHookVM();

                phObj.Ingate = new Ingate
                {
                    YardSpotId = item.Ingate.YardSpotId,
                    RouteMasterId = item.Ingate.RouteMasterId,
                    DateTime = item.Ingate.DateTime,
                    TrailerStatusId = item.Ingate.TrailerStatusId,
                    Bound = item.Ingate.Bound
                };

                phObj.RouteMaster = new RouteMaster
                {
                    Id = item.RouteMaster.Id,
                    Route = item.RouteMaster.Route,
                    Run = item.RouteMaster.Run,
                    UnloadDate = item.RouteMaster.UnloadDate,
                    Hazmat = item.RouteMaster.Hazmat,
                    Invoice = item.RouteMaster.Invoice,
                    Priority = item.RouteMaster.Priority,
                    TrailerNumber = item.RouteMaster.TrailerNumber,
                    Comments = item.RouteMaster.Comments
                };

                if (item.PreOutgate != null)
                {
                    phObj.PreOutgate = new PreOutgate
                    {
                        RouteMasterId = item.PreOutgate.RouteMasterId,
                        Id = item.PreOutgate.Id
                    };

                    phObj.Driver = new Driver
                    {
                        Id = item.Driver.Id,
                        Name = item.Driver.Name
                    };
                    phObj.Company = new Company
                    {
                        Name = item.Company.Name
                    };
                }

                lstContenedoresEnYarda.Add(phObj);
            }

            ViewBag.DriverId = dsh.getDrivers();

            ViewBag.CompanyId = dsh.getCompanies();

            ViewBag.YardSpotId = dsh.getSpotstoChange();

            YardStatusVM viewObj = new YardStatusVM
            {
                listContenedoresenYarda = lstContenedoresEnYarda,
                listSpots = SpotsList
            };

            return View(viewObj);
        }

        public JsonResult saveYardComment(string comment, long routeMasterid)
        {

            string user = User.Identity.GetUserId();

            var result = wcf.saveYardComment(comment, routeMasterid, user);

            return Json(new { saved = result });
        }

        public JsonResult getYardComments(long routeMasterId)
        {
            List<YardComment> commentlist = new List<YardComment>();
            var results = wcf.getYardComments(routeMasterId);

            foreach (var item in results)
            {
                YardComment commentdata = new YardComment
                {
                    AspNetUsersId = item.AspNetUsersId,
                    Comment = item.Comment,
                    DateTime = item.DateTime
                };

                commentlist.Add(commentdata);
            }

            return Json(commentlist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult updateDriver(int preoutgateId, int newDriver)
        {
            string user = User.Identity.GetUserId();

            var updated = wcf.updateAssignedDriver(preoutgateId, newDriver, user);

            string driver = wcf.getDriverAssigned(updated);

            return Json(driver, JsonRequestBehavior.AllowGet);
        }

        public JsonResult assignDriver(long? routeMasterid, long? newDriver)
        {
            string AspNetUsersId = User.Identity.GetUserId();

            long? assigned = wcf.SavePreOutGate(routeMasterid, newDriver, AspNetUsersId);

            string driver = wcf.getDriverAssigned(assigned);

            return Json(driver, JsonRequestBehavior.AllowGet);
        }

        public ActionResult expectedArrivals()
        {
            var result = wcf.getExpected();
            List<RouteMaster> expected = new List<RouteMaster>();
            foreach (var item in result)
            {
                RouteMaster rmObj = new RouteMaster
                {
                    Id = item.Id,
                    UnloadDate = item.UnloadDate,
                    Priority = item.Priority,
                    Route = item.Route,
                    Run = item.Run,
                    Comments = item.Comments,
                    TrailerNumber = item.TrailerNumber,
                    Invoice = item.Invoice
                };

                expected.Add(rmObj);
            }

            return View(expected);
        }

        public ActionResult PlanAudit()
        {
            ViewBag.type = dsh.getMissingExceding();

            PlanAuditVM obj = new PlanAuditVM
            {
                ListRouteMaster = null
            };

            return View(obj);
        }

        [HttpPost]
        public ActionResult PlanAudit(PlanAuditVM planObj)
        {
            List<RouteMaster> lrmObj = new List<RouteMaster>();
            ViewBag.type = dsh.getMissingExceding();
            if (ModelState.IsValid)
            {
                var Trailers = wcf.AuditTrailers(planObj.startDate, planObj.endDate, planObj.type);

                List<RouteMaster> listRM = new List<RouteMaster>();
                foreach (var rm in Trailers)
                {
                    RouteMaster rmObj = new RouteMaster
                    {
                        Id = rm.Id,
                        Route = rm.Route,
                        Run = rm.Run,
                        UnloadDate = rm.UnloadDate,
                        Hazmat = rm.Hazmat,
                        Priority = rm.Priority,
                        TrailerNumber = rm.TrailerNumber,
                    };
                    listRM.Add(rmObj);
                }
                PlanAuditVM paObj = new PlanAuditVM
                {
                    ListRouteMaster = listRM,
                    startDate = planObj.startDate,
                    endDate = planObj.endDate,
                    type = planObj.type
                };
                return View(paObj);
            }

            PlanAuditVM obj = new PlanAuditVM
            {
                ListRouteMaster = null
            };

            return View(obj);
        }

        public ActionResult realtimeInTransit()
        {
            var getRealTime = wcf.getinTransitStatus();

            List<RealTime_InTransit> inTransitList = new List<RealTime_InTransit>();
            foreach (var item in getRealTime)
            {
                RealTime_InTransit rmObj = new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = item.Comments,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName,
                    ArrvTime = item.ArrvTime
                };

                inTransitList.Add(rmObj);
            }

            return View(inTransitList);
        }

        [AllowAnonymous]
        public ActionResult realtimeInTransitPublic()
        {
            var getRealTime = wcf.getinTransitStatus();

            List<RealTime_InTransit> inTransitList = new List<RealTime_InTransit>();
            foreach (var item in getRealTime)
            {
                RealTime_InTransit rmObj = new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = item.Comments,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName,
                    ArrvTime = item.ArrvTime
                };

                inTransitList.Add(rmObj);
            }

            return View(inTransitList);
        }


        public JsonResult reloadRealTime()
        {
            var getRealTime = wcf.getinTransitStatus();

            List<RealTime_InTransit> inTransitList = new List<RealTime_InTransit>();
            foreach (var item in getRealTime)
            {
                inTransitList.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = item.Comments,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName
                });
            }
            return Json(inTransitList, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportPlanAudit(string start, string end, int type)
        {
            DateTime startDate = DateTime.Parse(start);
            DateTime endDate = DateTime.Parse(end);
            DateTime ReportEndDate = endDate.AddMinutes(1439);

            var Trailers = wcf.AuditTrailers(startDate, ReportEndDate, type);

            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            string fileName = (type == 1) ? "Missing-" + dateStamp + ".xlsx" : "Exceeding-" + dateStamp + ".xlsx";

            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument oSLDocument = new SLDocument();

            int row = 1;

            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
            {
                int counter = 0;
                SLStyle style = oSLDocument.CreateStyle();
                style.FormatCode = "d mmm yyyy";
                oSLDocument.SetCellStyle(row, 1, style);
                oSLDocument.SetCellValue(row, 1, date);

                System.Data.DataTable dt = new System.Data.DataTable();

                //columnas
                dt.Columns.Add("Trailer", typeof(string));
                dt.Columns.Add("Route", typeof(string));
                dt.Columns.Add("Sequence", typeof(string));
                dt.Columns.Add("Unload Date", typeof(string));
                dt.Columns.Add("Priority", typeof(int));
                foreach (var item in Trailers)
                {
                    if (item.UnloadDate == date)
                    {
                        dt.Rows.Add(item.TrailerNumber, item.Route, item.Run, item.UnloadDate, item.Priority);
                        counter++;
                    }
                }
                row++;
                oSLDocument.ImportDataTable(row, 1, dt, true);
                row = row + counter + 2;
            }

            oSLDocument.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult ExportYardStatus()
        {
            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            string fileName = "YardStatus-" + dateStamp + ".xlsx";

            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument sl = new SLDocument();
            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Spot", typeof(string));
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Route", typeof(string));
            dt.Columns.Add("Hazmat", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Bound", typeof(string));
            dt.Columns.Add("ATA", typeof(string));
            dt.Columns.Add("Priority", typeof(string));
            dt.Columns.Add("Comments", typeof(string));

            List<YardSpot> SpotsList = new List<YardSpot>();

            SpotsList = dsh.getAllSpots();

            List<PrintHookVM> lstContenedoresEnYarda = new List<PrintHookVM>();

            var activeIngates = wcf.getactiveIngates();
            List<int?> blocked = new List<int?>();
            List<int?> inUse = new List<int?>();
            foreach (var spot in SpotsList)
            {
                int b = 0;
                foreach (var item in activeIngates)
                {
                    if (item.Ingate.YardSpotId == spot.Id)
                    {
                        //string driver = (item.PreOutgate != null) ? item.Driver.Name + " - " + item.Company.Name : "";
                        string route = item.RouteMaster.Route + " " + item.RouteMaster.Run;
                        string bound = (item.Ingate.Bound == 1) ? "SB" : "NB";
                        string status = "";
                        string hazmat = item.RouteMaster.Hazmat;
                        if (hazmat == null || hazmat == "N") { hazmat = ""; }
                        if (item.Ingate.TrailerStatusId == 1) { status = "L"; }
                        if (item.Ingate.TrailerStatusId == 2) { status = "E"; }
                        if (item.Ingate.TrailerStatusId == 3) { status = "R"; }
                        string comment = item.RouteMaster.Comments;
                        dt.Rows.Add(spot.SpotNumber, item.RouteMaster.TrailerNumber, route, hazmat, status, bound, item.Ingate.DateTime, item.RouteMaster.Priority, comment);
                        b = 1;
                        //inUse.Add(spot.SpotNumber);
                    }
                }
                if (b == 0)
                {
                    //if (spot.SpotStatusId == 1)
                    //{
                    //    blocked.Add(spot.SpotNumber);
                    //}
                    dt.Rows.Add(spot.Id, "", "", "", "", "", "", "", "");
                }
            }

            //SLStyle inUseStyle = sl.CreateStyle();
            //inUseStyle.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Green, System.Drawing.Color.White);
            //inUseStyle.Font.FontColor = System.Drawing.Color.White;

            //SLStyle blockedStyle = sl.CreateStyle();
            //blockedStyle.Fill.SetPattern(PatternValues.Solid,System.Drawing.Color.Red, System.Drawing.Color.White);

            //foreach (int? spot in blocked) {
            //    sl.SetCellStyle((int)spot + 1, 1, blockedStyle);
            //    sl.SetCellStyle((int)spot + 1, 2, blockedStyle);
            //    sl.SetCellStyle((int)spot + 1, 3, blockedStyle);
            //    sl.SetCellStyle((int)spot + 1, 4, blockedStyle);
            //    sl.SetCellStyle((int)spot + 1, 5, blockedStyle);
            //    sl.SetCellStyle((int)spot + 1, 6, blockedStyle);
            //    sl.SetCellStyle((int)spot + 1, 7, blockedStyle);
            //}

            //foreach (int? spot in inUse)
            //{
            //    sl.SetCellStyle((int)spot + 1, 1, inUseStyle);
            //    sl.SetCellStyle((int)spot + 1, 2, inUseStyle);
            //    sl.SetCellStyle((int)spot + 1, 3, inUseStyle);
            //    sl.SetCellStyle((int)spot + 1, 4, inUseStyle);
            //    sl.SetCellStyle((int)spot + 1, 5, inUseStyle);
            //    sl.SetCellStyle((int)spot + 1, 6, inUseStyle);
            //    sl.SetCellStyle((int)spot + 1, 7, inUseStyle);
            //}

            sl.ImportDataTable(1, 1, dt, true);

            sl.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult TrailerHistory(TrailerMovements movements)
        {
            if (movements.TrailerNumber != null)
            {
                if (ModelState.IsValid)
                {
                    List<TrailerHistory> lTrailerHistory = new List<TrailerHistory>();

                    DateTime end = movements.end.AddMinutes(1439);
                    var listMovements = wcf.getTrailerHistory(movements.TrailerNumber, movements.start, end);

                    foreach (var item in listMovements)
                    {
                        lTrailerHistory.Add(new TrailerHistory
                        {
                            RouteMasterId = item.RouteMasterId,
                            DateTime = item.DateTime,
                            Route = item.Route,
                            Driver = item.Driver,
                            Spot = item.Spot,
                            Type = item.Type,
                            Bound = item.Bound,
                            TrailerStatus = item.TrailerStatus,
                            Hazmat = item.Hazmat
                        });
                    }

                    movements.THistory = lTrailerHistory;
                }
            }
            return View(movements);
        }

        public FileResult ExportTrailerHistory(string start, string end, string trailer)
        {
            DateTime startDate = DateTime.Parse(start);
            DateTime endDate = DateTime.Parse(end);
            DateTime ReportEndDate = endDate.AddMinutes(1439);
            List<TrailerHistory> lTrailerHistory = new List<TrailerHistory>();

            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "TrailerHistory-" + trailer.ToUpper() + "-" + dateStamp + ".xlsx";
            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument oSLDocument = new SLDocument();

            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Route", typeof(string));
            dt.Columns.Add("Hazmat", typeof(string));
            dt.Columns.Add("Bound", typeof(string));
            dt.Columns.Add("Trailer Status", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Date & Time", typeof(string));
            dt.Columns.Add("Driver", typeof(string));

            var listMovements = wcf.getTrailerHistory(trailer, startDate, ReportEndDate);

            foreach (var item in listMovements)
            {
                string bound = "";
                if (item.Bound == 1) { bound = "Southbound"; }
                else if (item.Bound == 2) { bound = "Northbound"; }
                string type = "";
                if (item.Type == 1) { type = "Ingate"; }
                else if (item.Type == 2) { type = "Hook"; }
                else if (item.Type == 3) { type = "Outgate"; }
                dt.Rows.Add(trailer.ToUpper(), item.Route, item.Hazmat, bound, item.TrailerStatus, type, item.DateTime, item.Driver);
            }

            oSLDocument.ImportDataTable(1, 1, dt, true);

            oSLDocument.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult ExportRealTime()
        {
            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "StatusInTransit-" + dateStamp + ".xlsx";
            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument oSLDocument = new SLDocument();

            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Priority", typeof(Int32));
            dt.Columns.Add("F1", typeof(string));
            dt.Columns.Add("Route-Origin", typeof(string));
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Unload Date", typeof(string));
            dt.Columns.Add("Comment", typeof(string));
            dt.Columns.Add("Driver", typeof(string));
            dt.Columns.Add("SDSP Spot", typeof(string));
            dt.Columns.Add("SDSP Dispatch", typeof(string));
            dt.Columns.Add("BC Spot", typeof(string));

            var getRealTime = wcf.getinTransitStatus();

            List<RealTime_InTransit> current = new List<RealTime_InTransit>();

            foreach (var item in getRealTime)
            {
                RealTime_InTransit obj = new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = item.Comments,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName
                };

                current.Add(obj);
            }

            current = current.OrderBy(x => x.Priority).ToList();

            List<int?> ontheway = new List<int?>();
            List<int?> insdsp = new List<int?>();
            List<int?> preBC = new List<int?>();
            List<int?> inBCyard = new List<int?>();

            foreach (var item in current)
            {
                string driver = (item.DriverCode == "N/A" || item.DriverCode == "" || item.DriverCode == null) ? item.DriverName : item.DriverCode;
                string clase = "";
                if (item.outTime != null)
                {
                    clase = "ontheway";
                    clase = (item.PreIngateBCDate != null) ? "preassigned" : clase;
                    clase = (item.IngateBcDateTime != null) ? "inbc" : clase;
                    if (item.IngateBcDateTime != null)
                    {
                        inBCyard.Add(item.Priority);
                    }
                    else if (item.PreIngateBCDate != null)
                    {
                        preBC.Add(item.Priority);
                    }
                    else
                    {
                        ontheway.Add(item.Priority);
                    }
                }
                else
                {
                    if (item.ingateTime != null)
                    {
                        insdsp.Add(item.Priority);
                    }
                }
                dt.Rows.Add(item.Priority, item.Invoice, item.Route + "-" + item.Run, item.TrailerNumber, item.UnloadDate.Value.ToString("MM-dd-yyyy"), item.Comments, driver, item.ingatespot, item.outTime, item.IngateBCSpot);
            }

            //Ordenamos por numero de prioridad
            dt.DefaultView.Sort = "Priority ASC";

            SLStyle heading = oSLDocument.CreateStyle();
            heading.SetFontBold(true);

            SLStyle sdspYard = oSLDocument.CreateStyle();
            sdspYard.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Green, System.Drawing.Color.White);
            sdspYard.Font.FontColor = System.Drawing.Color.White;

            SLStyle onTheWay = oSLDocument.CreateStyle();
            onTheWay.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Yellow, System.Drawing.Color.White);

            SLStyle preassigned = oSLDocument.CreateStyle();
            preassigned.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Orange, System.Drawing.Color.White);
            preassigned.Font.FontColor = System.Drawing.Color.White;

            SLStyle inBC = oSLDocument.CreateStyle();
            inBC.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Aqua, System.Drawing.Color.White);

            oSLDocument.SetCellStyle(1, 1, heading);
            oSLDocument.SetCellStyle(1, 2, heading);
            oSLDocument.SetCellStyle(1, 3, heading);
            oSLDocument.SetCellStyle(1, 4, heading);
            oSLDocument.SetCellStyle(1, 5, heading);
            oSLDocument.SetCellStyle(1, 6, heading);
            oSLDocument.SetCellStyle(1, 7, heading);
            oSLDocument.SetCellStyle(1, 8, heading);
            oSLDocument.SetCellStyle(1, 9, heading);
            oSLDocument.SetCellStyle(1, 10, heading);

            foreach (int? spot in insdsp)
            {
                oSLDocument.SetCellStyle((int)spot + 1, 1, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 2, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 3, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 4, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 5, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 6, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 7, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 8, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 9, sdspYard);
                oSLDocument.SetCellStyle((int)spot + 1, 10, sdspYard);
            }
            foreach (int? spot in ontheway)
            {
                oSLDocument.SetCellStyle((int)spot + 1, 1, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 2, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 3, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 4, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 5, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 6, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 7, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 8, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 9, onTheWay);
                oSLDocument.SetCellStyle((int)spot + 1, 10, onTheWay);
            }
            foreach (int? spot in preBC)
            {
                oSLDocument.SetCellStyle((int)spot + 1, 1, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 2, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 3, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 4, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 5, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 6, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 7, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 8, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 9, preassigned);
                oSLDocument.SetCellStyle((int)spot + 1, 10, preassigned);
            }
            foreach (int? spot in inBCyard)
            {
                oSLDocument.SetCellStyle((int)spot + 1, 1, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 2, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 3, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 4, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 5, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 6, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 7, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 8, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 9, inBC);
                oSLDocument.SetCellStyle((int)spot + 1, 10, inBC);
            }

            oSLDocument.ImportDataTable(1, 1, dt, true);

            oSLDocument.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


        public JsonResult changeSpot(long? routeMasterId, int newSpot)
        {
            string user = User.Identity.GetUserId();

            bool saved = wcf.changeSpot(routeMasterId, newSpot, user);

            return Json(new { saved = saved });
        }

        public ActionResult viewYardStatus()
        {
            List<YardStatu> lstYard = new List<YardStatu>();

            var getstatus = wcf.getYardStatus();

            foreach (var item in getstatus)
            {
                lstYard.Add(new YardStatu
                {
                    Id = item.Id,
                    IngateDateTime = item.IngateDateTime,
                    IngateId = item.IngateId,
                    Bound = item.Bound,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    RouteMasterId = item.RouteMasterId,
                    Hazmat = item.Hazmat,
                    Status = item.Status,
                    SpotStatusId = item.SpotStatusId,
                    Priority = item.Priority,
                    Run = item.Run,
                    Comment = item.Comment
                });
            }

            ViewBag.YardSpotId = dsh.getSpotstoChange();

            return View(lstYard);
        }

        public FileResult ExportViewYardStatus()
        {
            string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            string fileName = "YardStatus-" + dateStamp + ".xlsx";

            string path = Server.MapPath("~/Content/Uploads/" + fileName);

            SLDocument sl = new SLDocument();
            System.Data.DataTable dt = new System.Data.DataTable();

            //columnas
            dt.Columns.Add("Spot", typeof(string));
            dt.Columns.Add("Trailer", typeof(string));
            dt.Columns.Add("Route", typeof(string));
            dt.Columns.Add("Hazmat", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Bound", typeof(string));
            dt.Columns.Add("ATA", typeof(string));
            dt.Columns.Add("Priority", typeof(string));
            dt.Columns.Add("Comments", typeof(string));

            var SpotsList = wcf.getYardStatus();

            foreach (var item in SpotsList)
            {
                int b = 0;
                if (item.IngateId != null)
                {
                    string route = item.Route + " " + item.Run;
                    string bound = (item.Bound == 1) ? "SB" : "NB";
                    string hazmat = item.Hazmat;
                    if (hazmat == null || hazmat == "N") { hazmat = ""; }
                    string priority = (item.Priority > 0) ? item.Priority.ToString() : "";
                    dt.Rows.Add(item.Id, item.TrailerNumber, route, hazmat, item.Status[0], bound, item.IngateDateTime, priority, item.Comment);
                    b = 1;
                }
                if (b == 0)
                {
                    dt.Rows.Add(item.Id, "", "", "", "", "", "", "", "");
                }
            }
            sl.ImportDataTable(1, 1, dt, true);

            sl.SaveAs(path);

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [AllowAnonymous]
        public ActionResult publicyard()
        {
            List<YardStatu> lstYard = new List<YardStatu>();

            var getstatus = wcf.getYardStatus();

            foreach (var item in getstatus)
            {
                lstYard.Add(new YardStatu
                {
                    Id = item.Id,
                    IngateDateTime = item.IngateDateTime,
                    IngateId = item.IngateId,
                    Bound = item.Bound,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    RouteMasterId = item.RouteMasterId,
                    Hazmat = item.Hazmat,
                    Status = item.Status,
                    SpotStatusId = item.SpotStatusId,
                    Priority = item.Priority,
                    Run = item.Run,
                    Comment = item.Comment
                });
            }

            ViewBag.YardSpotId = dsh.getSpotstoChange();

            return View(lstYard);
        }

        [AllowAnonymous]
        public ActionResult forecastDplus3()
        {
            var result = wcf.forecastDplus3();

            List<RealTime_InTransit> plusOneList = new List<RealTime_InTransit>();

            foreach (var item in result)
            {
                plusOneList.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    RouteRun = item.RouteRun,
                    Hazmat = item.Hazmat,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot
                });
            }

            return View(plusOneList);
        }

        [AllowAnonymous]
        public ActionResult forecastDplus4()
        {
            var result = wcf.forecastDplus4();

            List<RealTime_InTransit> plusTwoList = new List<RealTime_InTransit>();

            foreach (var item in result)
            {
                plusTwoList.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    RouteRun = item.RouteRun,
                    Hazmat = item.Hazmat,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot
                });
            }

            return View(plusTwoList);
        }
    }
}