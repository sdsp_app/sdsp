﻿using Microsoft.AspNet.Identity;
using SDSP.Class;
using SDSP.Models;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace SDSP.Controllers
{
    [Authorize]
    public class SourcesController : Controller
    {
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        DataSourceHelper dsh = new DataSourceHelper();
        public ActionResult UploadInTransit()
        {
            return View();
        }

        public DataTable ConvertToDatatable<T>(IList<T> data)
        {
            var props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable { TableName = "TempTable" };
            foreach (PropertyDescriptor prop in props)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in props)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public ActionResult UploadRouteMaster()
        {
            ViewBag.Error = 0;
            return View();
        }

        [HttpPost]
        public ActionResult UploadRouteMaster(RouteMasterExcel importExcel) {
            if (ModelState.IsValid)
            {
                int periodId = wcf.savePeriod(importExcel.period, importExcel.StartDate);

                if (periodId > 0)
                {
                    string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string path = Server.MapPath("~/Content/Uploads/" + dateStamp + "-" + importExcel.file.FileName);
                    importExcel.file.SaveAs(path);
                    using (SLDocument sl = new SLDocument(path))
                    {
                        sl.SelectWorksheet("RouteMaster");
                        List<RouteMaster> lRouteMaster = new List<RouteMaster>();

                        int iRow = 2;
                        string[] LogisticsPointsID = { "20TMB", "SDSPT", "MXOTM" };
                        while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 1)))
                        {
                            if (LogisticsPointsID.Contains(sl.GetCellValueAsString(iRow, 34).Trim()))
                            {                                
                                RouteMaster oRouteMaster = new RouteMaster()
                                {
                                    Hazmat = sl.GetCellValueAsString(iRow, 9),
                                    UnloadDate = sl.GetCellValueAsDateTime(iRow, 10),
                                    RouteRun = sl.GetCellValueAsString(iRow, 11),
                                    Route = sl.GetCellValueAsString(iRow, 12),
                                    Run = sl.GetCellValueAsString(iRow, 13),
                                    PuRoute2 = sl.GetCellValueAsString(iRow, 14),
                                    RouteType = sl.GetCellValueAsString(iRow, 15),
                                    RouteMiles = sl.GetCellValueAsString(iRow, 23),
                                    LegMiles = sl.GetCellValueAsString(iRow, 24),
                                    Seq = sl.GetCellValueAsString(iRow, 25),
                                    SName = sl.GetCellValueAsString(iRow, 29),
                                    LDK = sl.GetCellValueAsString(iRow, 31),
                                    LogisticsPointId = sl.GetCellValueAsString(iRow, 34),
                                    Timezone = sl.GetCellValueAsString(iRow, 39),
                                    RoutesMilestone = sl.GetCellValueAsString(iRow, 42),
                                    Day = sl.GetCellValueAsString(iRow, 40),
                                    ArrvDate = sl.GetCellValueAsDateTime(iRow, 35),
                                    ArrvTime = TimeSpan.Parse(sl.GetCellValueAsString(iRow, 36)),
                                    DepDate = sl.GetCellValueAsDateTime(iRow, 37),
                                    DepTime = TimeSpan.Parse(sl.GetCellValueAsString(iRow, 38)),
                                    PeriodId = periodId,
                                    DataSourceId = 1,
                                    RegisterDate = DateTime.Now.Date
                                };
                                lRouteMaster.Add(oRouteMaster);
                            }
                            iRow++;
                        }
                        DataTable dtblRouteMaster = ConvertToDatatable(lRouteMaster);
                        //Invocar metodo del WCF   
                        string user = User.Identity.GetUserId();
                        bool saved = wcf.saveRouteMaster(dtblRouteMaster, path, user);
                        //Response.Write("Saved: " + importExcel.file.FileName);
                        return RedirectToAction("fileReport");
                    }
                    //ViewBag.Error = 0;
                }
                else {
                    ViewBag.Error = 1;
                }
            }
            return View();
        }

        public ActionResult UploadDTR() {

            ViewBag.error = (wcf.wasFileUploaded(2)) ? 1 : 0;

            return View();
        }

        [HttpPost]
        public ActionResult UploadDTR(ImportExcel importExcel)
        {
            if (ModelState.IsValid)
            {
                string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string path = Server.MapPath("~/Content/Uploads/" + dateStamp + "-" + importExcel.file.FileName);
                importExcel.file.SaveAs(path);
                using (SLDocument sl = new SLDocument(path))
                {
                    sl.SelectWorksheet("DTR");
                    List<DTR> lDTR = new List<DTR>();

                    int iRow = 2;
                    while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 4)))
                    {
                        DTR oDTR = new DTR()
                        {
                            TrailerNumber = sl.GetCellValueAsString(iRow, 1),
                            Route_Seq = sl.GetCellValueAsString(iRow, 2),
                            Renban = sl.GetCellValueAsString(iRow, 3),
                            UnloadDate = sl.GetCellValueAsDateTime(iRow, 4),
                            SecondUnloadDate = sl.GetCellValueAsDateTime(iRow, 5),
                            ShipDate = sl.GetCellValueAsDateTime(iRow, 6),
                            ETAbyXPO = sl.GetCellValueAsDateTime(iRow, 7),
                            Hazmat = sl.GetCellValueAsString(iRow, 8),
                            XBorder = sl.GetCellValueAsDateTime(iRow, 9),
                            CLoadLinesMROS = sl.GetCellValueAsString(iRow, 10),                            
                        };

                        lDTR.Add(oDTR);
                        
                        iRow++;
                    }                    

                    DataTable dtblDTR = ConvertToDatatable(lDTR);
                    string user = User.Identity.GetUserId();
                    int saved = wcf.saveDTR(dtblDTR,path, user);
                    //que paso miiiii montoya
                    //Response.Write("Saved: " + saved +" Rows - "+ importExcel.file.FileName);
                    return RedirectToAction("fileReport");
                }
            }

            return View();
        }

        public ActionResult UploadInvoices() {
            InvoicesImportExcel oImportExcel = new InvoicesImportExcel { listSpotsVM = null };
            return View(oImportExcel);
        }

        [HttpPost]
        public ActionResult UploadInvoices(InvoicesImportExcel importExcel)
        {
            List<SpotsVM> lstSpots = new List<SpotsVM>();
            if (ModelState.IsValid)
            {
                string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string path = Server.MapPath("~/Content/Uploads/" + dateStamp + "-" + importExcel.file.FileName);
                importExcel.file.SaveAs(path);

                SLDocument sl = new SLDocument(path);
                
                List<Broker> lBroker = new List<Broker>();

                int iRow = 2;
                while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 1)))
                {
                    Broker oBroker = new Broker()
                    {
                        Route = sl.GetCellValueAsString(iRow, 1),
                        Sequence = sl.GetCellValueAsString(iRow, 2),
                        TrailerNumber = sl.GetCellValueAsString(iRow, 3),
                        UnloadDate = sl.GetCellValueAsDateTime(iRow, 4),
                        Invoice = sl.GetCellValueAsString(iRow, 5),
                    };

                    lBroker.Add(oBroker);
                    iRow++;
                }
                    
                DataTable dtblBroker = ConvertToDatatable(lBroker);
                string user = User.Identity.GetUserId();
                var saved = wcf.saveBrokerFile(dtblBroker, path, user);

                foreach (var item in saved) {                    
                    SpotsVM oSpotsVM = new SpotsVM();
                    oSpotsVM.trailerNumber = item.trailerNumber;
                    oSpotsVM.yardSpotId = item.yardSpotId;
                    lstSpots.Add(oSpotsVM);
                }                                
            }

            return View(new InvoicesImportExcel { listSpotsVM = lstSpots });
        }

        public ActionResult updateSingleInvoice() {
            InvoiceUpdate oInvoiceUpdate = new InvoiceUpdate
            {
                SpotsVM = null
            };

            ViewBag.Invoice = dsh.getYesNoSelect();

            return View(oInvoiceUpdate);
        }

        [HttpPost]
        public ActionResult updateSingleInvoice(InvoiceUpdate invoiceUpdate)
        {
            string user = User.Identity.GetUserId();
            var result = wcf.updateSingleInvoice(invoiceUpdate.Invoice, invoiceUpdate.RouteMasterId,user);

            InvoiceUpdate oInvoiceUpdate = new InvoiceUpdate {
                SpotsVM = new SpotsVM {
                    yardSpotId = result.yardSpotId,
                    trailerNumber = result.trailerNumber
                }
            };

            ViewBag.Invoice = dsh.getYesno();

            return View(oInvoiceUpdate);
        }

        public ActionResult updatePriorities() {
            //List<RouteMaster> rmList = new List<RouteMaster>();

            SDSPEntities db = new SDSPEntities();

            var result = wcf.getRouteMasterDatabyDate();

            //foreach (var item in result)
            //{
            //    rmList.Add(
            //        new RouteMaster
            //        {
            //            Id = item.Id,
            //            TrailerNumber = item.TrailerNumber,
            //            Route = item.Route,
            //            Run = item.Run,
            //            RouteRun = item.RouteRun,
            //            Priority = item.Priority,
            //            Invoice = item.Invoice,
            //            UnloadDate = item.UnloadDate,
            //            PullAhead = item.PullAhead,
            //            ExpressPriority = item.ExpressPriority
            //        }
            //    );
            //}
            List<RealTime_InTransit> current = new List<RealTime_InTransit>();

            foreach (var item in result)
            {
                current.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName
                });
            }

            return View(current);
        }

        public JsonResult saveOrder(IDictionary<string, int> priorities)
        {

            var newDict = new Dictionary<string, int>(priorities);

            var savedOutgates = wcf.saveOrder(newDict);

            return Json(new { saved = true });
        }

       
        public JsonResult savePriorityComment(int routeMasterId, string comment) {
            string user = User.Identity.GetUserId();
            var result = wcf.savePriorityComment(routeMasterId,comment, user);

            return Json(new { saved = true });
        }

        public ActionResult addFromForecast()
        {
            ViewBag.Hazmat = dsh.getYesno();
            ViewBag.PullAhead = dsh.getYesno();

            //DateTime date = new DateTime(2018, 11, 19, 0, 0, 0);

            DateTime date = DateTime.Now.Date;
            
            var result = wcf.getForecastData(date);

            ForecastVM obj = new ForecastVM();

            List<RealTime_InTransit> currentList = new List<RealTime_InTransit>();
            foreach (var item in result.current)
            {                
                currentList.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = item.Comments,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    PullAhead = item.PullAhead,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName
                });
            }

            obj.current = currentList;

            List<RealTime_InTransit> plusOneList = new List<RealTime_InTransit>();
            foreach (var item in result.plusone)
            {
                RealTime_InTransit rmObj = new RealTime_InTransit
                {
                    Id = item.Id,
                    Route = item.Route,
                    Run = item.Run,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    Hazmat = item.Hazmat
                };

                plusOneList.Add(rmObj);
            }
            obj.plusone = plusOneList;

            List<RealTime_InTransit> plusTwoList = new List<RealTime_InTransit>();
            foreach (var item in result.plustwo)
            {
                RealTime_InTransit rmObj = new RealTime_InTransit
                {
                    Id = item.Id,
                    Route = item.Route,
                    Run = item.Run,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    Hazmat = item.Hazmat
                };

                plusTwoList.Add(rmObj);
            }

            obj.plustwo = plusTwoList;

            return View(obj);
        }

        public JsonResult updatePullAhead(int routeMasterId) {

            var updated = wcf.updatePullAhead(routeMasterId);

            return Json(new { updated = updated});
        }

        public JsonResult saveSingleInTransit(string trailer, string route, string origin, string unloadDate, string hazmat, string pullahead, string comments) {

            var result = wcf.saveSingleInTransit(trailer,route,origin,unloadDate,hazmat,pullahead,comments);

            return Json(new { saved = result});
        }

        public JsonResult editInTransit(long routeMasterId, string trailer)
        {
            string user = User.Identity.GetUserId();

            //var result = wcf.editInTransit(routeMasterId, trailer,user);
            var result = wcf.updateInTransit(routeMasterId, trailer, user);

            return Json(new { saved = result });
        }

        public JsonResult simpleEditIntransit(long routeMasterId, string trailer, string route, string run)
        {
            string user = User.Identity.GetUserId();

            var result = wcf.simpleEditIntransit(routeMasterId, trailer, route, run, user);

            return Json(new { saved = result});
        }

        [HttpPost]
        public ActionResult UploadInTransit(ImportExcel importExcel)
        {
            if (ModelState.IsValid)
            {
                string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string path = Server.MapPath("~/Content/Uploads/"+ dateStamp + "-" + importExcel.file.FileName);
                importExcel.file.SaveAs(path);

                using (SLDocument sl = new SLDocument(path))
                {
                    var sheets = sl.GetWorksheetNames();
                    
                    foreach (var sheet in sheets)
                    {
                        sl.SelectWorksheet(sheet);
                        List<InTransit> lInTransit = new List<InTransit>();

                        int iRow = 2;
                        while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 1)))
                        {
                            int ispotSDSP = 0;
                            int.TryParse(sl.GetCellValueAsString(iRow, 26), out ispotSDSP);
                            int ispotTMMB = 0;
                            int.TryParse(sl.GetCellValueAsString(iRow, 30), out ispotTMMB);
                            DateTime? dtSDSPDispatch = (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 28))) ? sl.GetCellValueAsDateTime(iRow, 28) : (DateTime?)null;
                            DateTime? dtTMMBCArrives = (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 29))) ? sl.GetCellValueAsDateTime(iRow, 29) : (DateTime?)null;
                            DateTime? dtUnloadDate = (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 20))) ? sl.GetCellValueAsDateTime(iRow, 20) : (DateTime?)null;
                            InTransit oInTransit = new InTransit()
                            {
                                Priority = (int)sl.GetCellValueAsUInt32(iRow, 1),
                                Route = sl.GetCellValueAsString(iRow, 2),
                                Origin = sl.GetCellValueAsString(iRow, 3),
                                OrdersLines = sl.GetCellValueAsString(iRow, 4),
                                MainShuttle = sl.GetCellValueAsString(iRow, 5),
                                Lines = sl.GetCellValueAsString(iRow, 6),
                                L1 = sl.GetCellValueAsString(iRow, 7),
                                L2 = sl.GetCellValueAsString(iRow, 8),
                                L3 = sl.GetCellValueAsString(iRow, 9),
                                L4 = sl.GetCellValueAsString(iRow, 10),
                                L5 = sl.GetCellValueAsString(iRow, 11),
                                L6 = sl.GetCellValueAsString(iRow, 12),
                                L7 = sl.GetCellValueAsString(iRow, 13),
                                L8 = sl.GetCellValueAsString(iRow, 14),
                                L9 = sl.GetCellValueAsString(iRow, 15),
                                L10 = sl.GetCellValueAsString(iRow, 16),
                                L11 = sl.GetCellValueAsString(iRow, 17),
                                L12 = sl.GetCellValueAsString(iRow, 18),
                                TrailerNumber = sl.GetCellValueAsString(iRow, 19),
                                UnloadDate = dtUnloadDate,
                                Hazmat = sl.GetCellValueAsString(iRow, 21),
                                F1 = sl.GetCellValueAsString(iRow, 22),
                                OW = sl.GetCellValueAsString(iRow, 23),
                                PacerStatus = sl.GetCellValueAsString(iRow, 24),
                                PacerTime = sl.GetCellValueAsString(iRow, 25),
                                SpotSDSP = ispotSDSP,
                                StatusComments = sl.GetCellValueAsString(iRow, 27),
                                SDSPDispatch = dtSDSPDispatch,
                                TMMBCArrives = dtTMMBCArrives,
                                Spot = ispotTMMB,
                                D = sl.GetCellValueAsString(iRow, 31),
                                PullAhead = sl.GetCellValueAsString(iRow, 32),
                                SystemUploadDate = DateTime.Now.Date
                            };

                            lInTransit.Add(oInTransit);
                            iRow++;
                        }

                        DataTable dtblIntransit = ConvertToDatatable(lInTransit);

                        //Invocar metodo del WCF   
                        string user = User.Identity.GetUserId();
                        int saved = wcf.saveInTransitBulk(dtblIntransit, path,user);
                        //Response.Write("Saved: " + saved + " - " + importExcel.file.FileName);
                        return RedirectToAction("fileReport");
                    }
                }
            }
            return View();
        }

        public JsonResult getRouteMasterData(string trailer)
        {
            List<RouteMaster> lstRouteMaster = new List<RouteMaster>();
            string trailerWithSpaces = Regex.Replace(trailer, @"(\p{L})(\d)", "$1 $2");
            var wcfResult = wcf.searchTrailer(trailerWithSpaces);

            if (wcfResult.Count() == 0) {
                wcfResult = wcf.searchTrailer(trailer);
            }

            foreach (var item in wcfResult)
            {               
                lstRouteMaster.Add(new RouteMaster {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    Run = item.Run,
                    Route = item.Route,
                    Priority = item.Priority,
                    UnloadDate = item.UnloadDate,
                    DataSourceId = item.DataSourceId
                });
            }
            return Json(lstRouteMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult fileReport() {
            
            List<UploadedFile> fileList = new List<UploadedFile>();
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1).Date;

            var result = wcf.getFiles(start, end);

            foreach (var item in result)
            {
                fileList.Add(new UploadedFile
                {
                    UploadDate = item.UploadDate,
                    Name = item.Name,
                    FileName = item.FileName,
                    Id = item.Id
                });
            }
            FilesReportVM fReport = new FilesReportVM {
                startDate = start,
                endDate = end,
                fileList = fileList
            };
            
            return View(fReport);
        }

        [HttpPost]
        public ActionResult fileReport(FilesReportVM fReport)
        {
            List<UploadedFile> fileList = new List<UploadedFile>();
            DateTime start = fReport.startDate;
            DateTime end = fReport.endDate.AddDays(1).Date;
            var result = wcf.getFiles(start, end);

            foreach (var item in result)
            {
                UploadedFile fObj = new UploadedFile
                {
                    UploadDate = item.UploadDate,
                    Name = item.Name,
                    FileName = item.FileName,
                    Id = item.Id
                };

                fileList.Add(fObj);
            }

            FilesReportVM viewReport = new FilesReportVM {
                startDate = start,
                endDate = end,
                fileList = fileList
            };                       

            return View(viewReport);
        }

        public ActionResult UploadOriginalInTransit()
        {
            ViewBag.error = (wcf.wasFileUploaded(3)) ? 1:0;                 
            
            return View();
        }

        [HttpPost]
        public ActionResult UploadOriginalInTransit(ImportExcel importExcel)
        {
            if (ModelState.IsValid)
            {
                string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string path = Server.MapPath("~/Content/Uploads/" + dateStamp + "-" + importExcel.file.FileName);                
                importExcel.file.SaveAs(path);

                using (SLDocument sl = new SLDocument(path))
                {
                    var sheets = sl.GetWorksheetNames();
                    List<InTransit> lInTransit = new List<InTransit>();
                    int flag = 1;
                    foreach (var sheet in sheets)
                    {
                        //Response.Write(sheet);
                        string numbers = new String(sheet.Where(Char.IsDigit).ToArray());

                        sl.SelectWorksheet(sheet);

                        int? priority = null;
                        
                        string route = "";
                        string seq = "";
                        //Empezamos en el row 3 porque ahi comienza el in transit
                        int iRow = 3;
                        int iCol = 1;
                        if (numbers.Length > 0)
                        {
                            iCol = 2;
                            flag++;
                        }                        
                        while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, iCol)))
                        {
                            if (numbers.Length == 0)
                            {                                
                                //Pestaña In Transit
                                priority = (int)sl.GetCellValueAsUInt32(iRow, 1);
                                route = sl.GetCellValueAsString(iRow, 2);
                                seq = sl.GetCellValueAsString(iRow, 3);
                            }
                            else
                            {
                                //Pestañas Forecast
                                priority = null;
                                route = new String(sl.GetCellValueAsString(iRow, 2).Where(Char.IsLetter).ToArray());
                                seq = new String(sl.GetCellValueAsString(iRow, 2).Where(Char.IsDigit).ToArray());                                
                            }
                            int ispotSDSP = 0;
                            int.TryParse(sl.GetCellValueAsString(iRow, 10), out ispotSDSP);
                            int ispotTMMB = 0;
                            int.TryParse(sl.GetCellValueAsString(iRow, 14), out ispotTMMB);
                            DateTime? dtSDSPDispatch = (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 11))) ? sl.GetCellValueAsDateTime(iRow, 11) : (DateTime?)null;
                            DateTime? dtTMMBCArrives = (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 6))) ? sl.GetCellValueAsDateTime(iRow, 6) : (DateTime?)null;
                            DateTime? dtUnloadDate = (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 5))) ? sl.GetCellValueAsDateTime(iRow, 5) : (DateTime?)null;
                            InTransit oInTransit = new InTransit()
                            {
                                Priority = priority,
                                Route = route,
                                Origin = seq,
                                TrailerNumber = sl.GetCellValueAsString(iRow, 4),
                                UnloadDate = dtUnloadDate,
                                Hazmat = sl.GetCellValueAsString(iRow, 7),
                                F1 = sl.GetCellValueAsString(iRow, 8),
                                SpotSDSP = ispotSDSP,
                                StatusComments = sl.GetCellValueAsString(iRow, 9),
                                SDSPDispatch = dtSDSPDispatch,
                                TMMBCArrives = dtTMMBCArrives,
                                Spot = ispotTMMB,
                                D = sl.GetCellValueAsString(iRow, 15),
                                PullAhead = sl.GetCellValueAsString(iRow, 16),
                                SystemUploadDate = DateTime.Now.Date,
                                FlagOrder = flag
                            };

                            lInTransit.Add(oInTransit);
                            iRow++;
                        }
                    }
                    DataTable dtblIntransit = ConvertToDatatable(lInTransit);
                    string user = User.Identity.GetUserId();
                    //Invocar metodo del WCF   
                    int saved = wcf.saveInTransitBulk(dtblIntransit, path,user);
                    return RedirectToAction("fileReport");
                }
            }
            return View();
        }

        public JsonResult removePullAhead(long routeMasterId) {
            var obj = wcf.removePullAhead(routeMasterId);

            return Json( new { obj });
        }


        public ActionResult LoadInventory() {
            return View();        
        }

        [HttpPost]
        public ActionResult LoadInventory(ImportExcel importExcel)
        {
            if (ModelState.IsValid)
            {
                string dateStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string path = Server.MapPath("~/Content/Uploads/" + dateStamp + "-" + importExcel.file.FileName);
                importExcel.file.SaveAs(path);
                using (SLDocument sl = new SLDocument(path))
                {
                    sl.SelectWorksheet("rptYardAging_SDSP");
                    List<Inventory> lInventory = new List<Inventory>();
                    int iRow = 2;
                    while (!string.IsNullOrEmpty(sl.GetCellValueAsString(iRow, 1)))
                    {
                        string spot = sl.GetCellValueAsString(iRow, 1);
                        int spotNumber = Int32.Parse(spot);
                        Inventory oInventory = new Inventory()
                        {
                            Spot = spotNumber,
                            Status = sl.GetCellValueAsString(iRow, 2),
                            Bound = sl.GetCellValueAsString(iRow, 3),
                            TrailerNumber = sl.GetCellValueAsString(iRow, 4),
                            Route = sl.GetCellValueAsString(iRow, 5),
                            ATA = sl.GetCellValueAsDateTime(iRow,6),
                            //Priority = sl.GetCellValueAsInt32(iRow, 7),
                            Days = sl.GetCellValueAsInt32(iRow, 7),
                            Hours = sl.GetCellValueAsInt32(iRow, 8),                            
                        };
                        lInventory.Add(oInventory);
                        iRow++;
                    }

                    DataTable dtblDTR = ConvertToDatatable(lInventory);
                    string user = User.Identity.GetUserId();
                    //int saved = wcf.loadInventory(dtblDTR, path, user);

                    //Response.Write("Saved: " + saved +" Rows - "+ importExcel.file.FileName);
                    return RedirectToAction("fileReport");
                }
            }

            return View();
        }

    }
}
