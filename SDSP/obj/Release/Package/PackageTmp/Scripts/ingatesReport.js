﻿$(document).ready(function () {

    $("#btnSubmit").click(function () {
        var start = $("#startDate").val();
        var end = $("#endDate").val();
        window.location = "Export?start="+start+"&end="+end;
    });


    var selected;
    //$("a[id^='Delete-']").click(function () {
    //    var parts = this.id.split("-");
    //    selected = parseInt(parts[1]);
    //    console.log(selected);
    //    $('#confirmModal').modal('toggle');
    //});

    $("#Trailers tbody").on("click", "a", function () {
        //console.log(this.id);
        if ((this.id).includes("Delete")) {
            var parts = this.id.split("-");
            selected = parseInt(parts[1]);
            console.log(selected);
            $('#confirmModal').modal('toggle');
        }
    });

    $("#yes").click(function () {
        $.ajax({
            type: "POST",
            url: 'delete',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                if (data.deleted > 0) {
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                } else if(data.deleted == -1) {
                    //No se puede borrar porque ya se habia dado outgate
                    $('#errorModal').modal('toggle');
                }                                   
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    function loadTable() {
        $("#Trailers").find("tbody > tr").remove();
        $.ajax({
            type: "POST",
            url: 'reloadIngatesReport',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ startDate: $("#startDate").val(), endDate: $("#endDate").val() }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                var x = 0;
                $.each(data.ingatesReport, function (index, item) {
                    console.log(item);
                    var trailer = item.TrailerNumber;
                    var route = item.Route;
                    var run = item.Run;
                    if (item.UnloadDate != null) {
                        var uD = new Date(item.UnloadDate.match(/\d+/) * 1);
                        uD = uD.toLocaleString("en-US");
                    } else {
                        var uD = "";
                    }
                    var iD = new Date(item.IngateDateTime.match(/\d+/) * 1);

                    var bound = (item.Bound == 1) ? "Southbound" : "Northbound";

                    iD = iD.toLocaleString("en-US");
                    var driver = item.DriverName;
                    var makeTr = '<tr><td>' + trailer + '</td><td>' + route + '</td><td>' + run + '</td><td>' + uD + '</td><td>' + iD + '</td><td>' + driver + '</td><td>' + bound + '</td><td>' + item.TrailerStatusName + '</td><td><a id="Delete-' + item.Id + '" href="#">Delete</a></td></tr>';
                    if (x == 0) {
                        $('#Trailers > tbody').append(makeTr);
                    } else {
                        $('#Trailers > tbody:last-child').append(makeTr);
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    }

    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#Trailers tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

});