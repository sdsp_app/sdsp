﻿$(document).ready(function () {

    $(function () {
        $.widget("custom.combobox", {

            _create: function () {
                this.wrapper = $("<span>")
                  .addClass("custom-combobox")
                  .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },
            _createAutocomplete: function () {
                var selected = this.element.children(":selected"),
                value = "";
                this.input = $("<input>")
                  .appendTo(this.wrapper)
                  .val(value)
                  .attr({ "title": "", "id": "input" + $(this.element).attr("id") })
                  .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                  .autocomplete({
                      delay: 0,
                      minLength: 0,
                      source: $.proxy(this, "_source"),
                      response: function (event, ui) {
                          // ui.content is the array that's about to be sent to the response callback.
                          if (ui.content.length === 0) {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("No matches found <a id='add-driver' style='cursor:pointer'>Add Driver</a>");
                              } else {
                                  $("#company-error").html("No matches found <a id='add-company' style='cursor:pointer'>Add Company</a>");
                              }
                          } else {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("");
                              } else {
                                  $("#company-error").html("");
                              }
                          }
                      }
                  })
                  .tooltip({
                      classes: {
                          "ui-tooltip": "ui-state-highlight"
                      }
                  });
                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },
                    autocompletechange: "_removeIfInvalid"
                });
            },
            _createShowAllButton: function () {
                var input = this.input,
                  wasOpen = false;

                $("<a>")
                  .attr("tabIndex", -1)
                  .attr("title", "Show All Items")
                  .tooltip()
                  .appendTo(this.wrapper)
                  .button({
                      icons: {
                          primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                  })
                  .removeClass("ui-corner-all")
                  .addClass("custom-combobox-toggle ui-corner-right")
                  .on("mousedown", function () {
                      wasOpen = input.autocomplete("widget").is(":visible");
                  })
                  .on("click", function () {
                      input.trigger("focus");
                      // Close if already visible
                      if (wasOpen) {
                          return;
                      }
                      // Pass empty string as value to search for, displaying all results
                      input.autocomplete("search", "");
                  });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text))) {
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    }
                }));
            },
            _removeIfInvalid: function (event, ui) {
                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }
                // Search for a match (case-insensitive)
                var value = this.input.val(),
                  valueLowerCase = value.toLowerCase(),
                  valid = false;
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });
                // Found a match, nothing to do
                if (valid) {
                    return;
                }
                // Remove invalid value
                this.element.val("");
                this.input.autocomplete("instance").term = "";
            },
            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        if ($("#prevIngate").val() == 0) {
            $("#DriverId").combobox();
            $("#CompanyId").combobox();
        }
    });

    $("#driver-error").on("click", "a", function () {
        //Desplegar form para agregar driver
        $("#divAddDriver").show("fade");
    });

    $("#save-driver").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '../Ingates/saveDriver',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ driverName: $("#DriverName").val(), companyId: $("#CompanyId").val() }),
            dataType: "json",
            success: function (data) {
                if (data.Id == -1) {
                    //Chofer repetido
                    $('#errorModal').modal('toggle');
                } else {
                    $('#DriverId')
                         .append($('<option>', { value: data.Id })
                         .text(data.Name));
                    $("#DriverName").val("");
                    $("#divAddDriver").hide("fade");
                }
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#company-error").on("click", "a", function () {
        //Desplegar form para agregar company
        $("#divAddCompany").show("fade");
    });

    $("#save-company").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '../Ingates/saveCompany',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ companyName: $("#CompanyName").val() }),
            dataType: "json",
            success: function (data) {
                if (data.Id == -1) {
                    //Compania repetida
                    $('#errorModal').modal('toggle');
                } else {
                    $('#CompanyId')
                         .append($('<option>', { value: data.Id })
                         .text(data.Name));
                    $("#divAddCompany").hide("fade");
                    $("#CompanyName").val("");
                }
            },
            error: function (jqXHR) {
                console.log("Error saving company");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#closeDriver").click(function () {
        $("#divAddDriver").hide("fade");
    });

    $("#closeCompany").click(function () {
        $("#divAddCompany").hide("fade");
    });

    $("#btnTrailers").click(function () {
        event.preventDefault();
        loadTrailers($('input[name=Bound]:checked').val());
    });

    var maxindex = 0;    

    $("#trailers tbody").on("click", "tr", function () {
        $("tr").removeClass("selected");
        $(this).toggleClass("selected");
        $("#RouteMasterId").val($(this).attr("id"));
    });

    $("#trailers tbody").on('keydown', "tr", function (event) {
        //Manejar la tabla con teclado
        switch (event.keyCode) {
            case 40:
                //Flecha abajo
                var tabIndex = $(this).attr("tabindex");            
                if (tabIndex < (maxindex - 1)) {
                    $(this).toggleClass("selected");
                    tabIndex++;
                    $('#trailers tbody tr[tabindex="' + tabIndex + '"]').toggleClass("selected");
                }            
                $("#trailers tbody tr[tabindex=" + tabIndex + "]").focus();
                $("#RouteMasterId").val($("#trailers tbody tr[tabindex=" + tabIndex + "]").attr("id"));
                break;
            case 38:
                //Flecha arriba
                $(this).toggleClass("selected");
                var tabIndex = $(this).attr("tabindex");
                if (tabIndex > 0) {
                    tabIndex--;
                }
                $('#trailers tbody tr[tabindex="' + tabIndex + '"]').toggleClass("selected");
                $("#trailers tbody tr[tabindex=" + tabIndex + "]").focus();
                $("#RouteMasterId").val($("#trailers tbody tr[tabindex=" + tabIndex + "]").attr("id"));
                break;
            case 9:
                //tab
                var inputs = $(this).closest('form').find(':input');
                inputs.eq(inputs.index(this) + 1).focus();
                break;
        }
    });

    function loadTrailers(tipo) {
        $("#trailers").find("tbody > tr").remove();
        $.ajax({
            type: "POST",
            url: 'getTrailers',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ tipo: tipo }),
            dataType: "json",
            success: function (data) {
                var list = data;
                var x = 0;                
                maxindex = 0;
                $.each(list, function (index, item) {
                    var tn = item.RouteMaster.TrailerNumber.trim();
                    var priority = (item.RouteMaster.Priority == null) ? "" : item.RouteMaster.Priority;
                    var route = item.RouteMaster.Route.trim();
                    var id = item.RouteMaster.Id;
                    var spot = item.Ingate.YardSpotId;
                    var hazmat = item.RouteMaster.Hazmat;
                    if (hazmat == null || hazmat == "N") { hazmat = ""; }
                    var status = "";
                    if (item.Ingate.TrailerStatusId == 1) { status = "Loaded"; }
                    if (item.Ingate.TrailerStatusId == 2) { status = "Empty"; }
                    if (item.Ingate.TrailerStatusId == 3) { status = "Racks"; }                    
                    var comments = item.Comment.Comment1.trim();
                    if (comments.includes(" | ")) {
                        var cmntparts = comments.split("|");
                        comments = "<b>Position Change: </b>" + cmntparts[0];
                        comments += " <b>Yard comment: </b>" + cmntparts[1];
                    }
                    var makeTR = '<tr tabindex="' + maxindex + '" id="' + id + '"><td>' + tn + '</td>>><td>'+route+'</td><td class="hideable">' + priority + '</td><td class="hideable">' + hazmat + '</td><td>' + status + '</td><td>' + spot + '</td><td>' + comments + '</td></tr>';
                    if (maxindex == 0) {
                        $('#trailers > tbody').append(makeTR);
                        //$("#RouteMasterId").val(id);
                    } else {
                        $('#trailers > tbody:last-child').append(makeTR);
                    }
                    //$("#trailers tbody tr[tabindex=0]").focus();
                    //$("#trailers tbody tr[tabindex=0]").addClass("selected");
                    maxindex++;
                });

                if (tipo == 1) {
                    $(".hideable").show();                    
                } else {
                    $(".hideable").hide();
                    //if (tipo == 3) {
                    //    $(".route").show();
                    //}
                }
                $("#divTrailers").show("fade");
            },
            error: function () {
                console.log("Error");
            }
        });
    }

    loadTrailers(1);

    $(':radio[name=Bound]').change(function () {
        loadTrailers($(this).val());
    });

    $("#searchOutgate").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#trailers tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    if ($("#prevIngate").val() == 0) {
        $("#DriverId").val("");
    }

    //if ($("#Error").val() == 1) {
    //    $("#divError").show();
    //} else {
    //    $("#divMessage").show();
    //}
    
    var error = parseInt($("#Error").val());
    switch (error) {
        case -1:
            console.log("Switch 0");
            $("#divMessage").show("fadeup");
            break;
        case 1:
            console.log("Switch 1");
            $("#divError").show("fadeup");
            break;
        case 2:
            console.log("Switch 2");
            $("#errorText").text("Validation Errors! No record was saved");
            $("#divError").show("fadeup");
            break;
    }

    $('#assignDriver').submit(function () {
        document.getElementById("savehook").disabled = true;
        return true; // return false to cancel form action
    });
});