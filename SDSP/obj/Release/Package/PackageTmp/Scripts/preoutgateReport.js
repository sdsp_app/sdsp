﻿$(document).ready(function () {

    $("#btnSubmit").click(function () {
        var start = $("#startDate").val();
        var end = $("#endDate").val();
        window.location = "Export";
    });

    var selected;
    //$("a[id^='Delete-']").click(function () {
    //    var parts = this.id.split("-");
    //    selected = parseInt(parts[1]);
    //    console.log(selected);
    //    $('#confirmModal').modal('toggle');
    //});

    $("#Trailers tbody").on("click", "a", function () {
        //console.log(this.id);
        if ((this.id).includes("Delete")) {
            var parts = this.id.split("-");
            selected = parseInt(parts[1]);
            console.log(selected);
            $('#confirmModal').modal('toggle');
        }
    });

    $("#yes").click(function () {
        $.ajax({
            type: "POST",
            url: 'delete',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                if (data.deleted == 1) {
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                } else if(data.deleted == -1){
                    //No se puede borrar, ya se habia dado outgate
                    console.log("Si entro");
                    $('#errorModal').modal('toggle');
                    loadTable();
                }
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    function loadTable() {
        $("#Trailers").find("tbody > tr").remove();
        $.ajax({
            type: "POST",
            url: 'reloadPreoutgatesReport',
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify({ startDate: $("#startDate").val(), endDate: $("#endDate").val() }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                var x = 0;
                $.each(data, function (index, item) {
                    console.log(item);
                    var trailer = item.TrailerNumber;
                    var route = item.Route;
                    var run = item.Run;
                    if (item.UnloadDate != null) {
                        var uD = new Date(item.UnloadDate.match(/\d+/) * 1);
                        uD = uD.toLocaleString("en-US");
                    } else {
                        var uD = "";
                    }
                    var iD = new Date(item.PreoutgateDateTime.match(/\d+/) * 1);
                    iD = iD.toLocaleString("en-US");
                    var driver = item.DriverName;
                    var bound = (item.Bound == 1) ? "Southbound" : "Northbound";
                    var makeTr = '<tr><td>' + trailer + '</td><td>' + route + '</td><td>' + run + '</td><td>' + uD + '</td><td>' + iD + '</td><td>' + driver + '</td><td>' + bound + '</td><td>'+ item.YardSpotId +'</td><td>' + item.TrailerStatusName + '</td><td><a id="Edit-' + item.Id + '" href="Edit/' + item.Id + '" >Edit</a> |<a id="Delete-' + item.Id + '" href="#">Delete</a></td></tr>';
                    if (x == 0) {
                        $('#Trailers > tbody').append(makeTr);
                    } else {
                        $('#Trailers > tbody:last-child').append(makeTr);
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log("Error fetching data");
                console.log(jqXHR.responseText);
            }
        });
    }

});