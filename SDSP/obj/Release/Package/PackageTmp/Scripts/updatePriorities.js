﻿$(document).ready(function () {

    var arr = [];
    var temp = [];
    var lastId = 0;
    //var tbodyHTML = $('#current').prop('outerHTML');
    //console.log(tbodyHTML);
    var option = 0;
    var elem;
    //Arrastrar Record
    $("tbody").sortable({
        stop: function (event, ui) {
            //Mostrar el modal antes de hacer un cambio, en el modal se da la opcion de cancelar el cambio            
            option = 1;
            lastId = ui.item.attr("id");
            sender = ui.sender;
            $('#commentsModal').modal("show");            
            //saveMove();

        }
    });
    $("#sortable").disableSelection();

    //Boton Move to top
    $(".movetop").on("click", function () {
        //Mostrar el modal antes de hacer un cambio, en el modal se da la opcion de cancelar el cambio
        option = 2;
        elem = $(this).closest("tr");
        lastId = $(this).closest("tr").attr("id");
        $('#commentsModal').modal("show");
        //saveMove();
    });

    function saveMove() {
        console.log("Entro a savemove");
        if (option == 2) {
            
            //Si fue con el boton move to top, hacer el cambio visual (Mandar el registro hasta arriba)
            //Si se cancelo el movimiento no se hara el movimiento visual
            elem.siblings().first().before(elem);
            console.log("move to top");
        }
        getPositions();
        if (!arraysEqual(arr, temp)) {
            console.log("Hubo diferencia");            
            setOrder();            
        } else {
            console.log("No  hubo diferencia");
        }
    }

    getPositions();
    temp = arr;

    function getPositions() {
        arr = [];
        $("tbody tr").each(function () {
            arr.push(this.id);
        });        
    }

    function arraysEqual(arr1, arr2) {
        if (arr1.length !== arr2.length)
            return false;
        for (var i = arr1.length; i--;) {
            if (arr1[i] !== arr2[i])
                return false;
        }
        return true;
    }

    orderedItems = {};
    function setOrder() {
        console.log("Entro a setorder");
        for (var i = 0; i < arr.length; i++) {
            orderedItems[arr[i]] = (i + 1);
        }
        $.ajax({
            type: "POST",
            url: 'saveOrder',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ priorities: orderedItems }),
            dataType: "json",
            beforeSend: function () {
                $('#loadingModal').modal("show");
            },
            success: function (data) {
                $('#loadingModal').modal("hide");
                $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                    $("#divMessage").slideUp(500);
                });
                temp = arr;
                //tbodyHTML = $('#current').prop('outerHTML');
            },
            error: function (jqXHR) {
                console.log("Error");
                console.log(jqXHR.responseText);
            }
        });
    }

    $("#saveChange").click(function () {
        if ($("#Comment").val().length == 0) {
            alert("Please enter a reason");
            $("#Comment").focus();
        } else {
            $.ajax({
                type: "POST",
                url: 'savePriorityComment',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ routeMasterId: lastId, comment: $("#Comment").val() }),
                dataType: "json",
                success: function (data) {
                    $('#commentsModal').modal("hide");                    
                    //Se debe hace el cambio
                    saveMove();

                    $("#Comment").val("");
                },
                error: function (jqXHR) {
                    console.log("Error");
                    console.log(jqXHR.responseText);
                }
            });
        }
    });

    $("#cancelChange").click(function () {
        //Si presiona el boton para cancelar el movimiento
        $('#commentsModal').modal("hide");
        if (option == 1) {
            //Si se arrastro se debe regresar al estatus anterior, lo mas facil es recargar la pagina            
            location.reload();
            //$("#tbody").sortable('cancel');
            //$(sender).sortable('cancel');            
        }
    });

    console.log("Nuevo con reload");

});