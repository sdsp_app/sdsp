﻿$(document).ready(function () {

    $(function () {
        $.widget("custom.combobox", {

            _create: function () {
                this.wrapper = $("<span>")
                  .addClass("custom-combobox")
                  .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },

            _createAutocomplete: function () {
                var selected = this.element.children(":selected"),
                //value = selected.val() ? selected.text() : "";
                value = "";
                this.input = $("<input>")
                  .appendTo(this.wrapper)
                  .val(value)
                  .attr({ "title": "", "id": "input" + $(this.element).attr("id") })
                  .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                  .autocomplete({
                      delay: 0,
                      minLength: 0,
                      source: $.proxy(this, "_source"),
                      response: function (event, ui) {
                          // ui.content is the array that's about to be sent to the response callback.
                          if (ui.content.length === 0) {
                              //console.log("No hay resultados " + $(this).attr("id"));
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("No matches found <a id='add-driver' style='cursor:pointer'>Add Driver</a>");
                              } else {
                                  $("#company-error").html("No matches found <a id='add-company' style='cursor:pointer'>Add Company</a>");
                              }
                          } else {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("");
                              } else {
                                  $("#company-error").html("");
                              }
                              //$("#empty-message").empty();
                          }
                      }
                  })
                  .tooltip({
                      classes: {
                          "ui-tooltip": "ui-state-highlight"
                      }
                  });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function () {
                var input = this.input,
                  wasOpen = false;

                $("<a>")
                  .attr("tabIndex", -1)
                  .attr("title", "Show All Items")
                  .tooltip()
                  .appendTo(this.wrapper)
                  .button({
                      icons: {
                          primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                  })
                  .removeClass("ui-corner-all")
                  .addClass("custom-combobox-toggle ui-corner-right")
                  .on("mousedown", function () {
                      wasOpen = input.autocomplete("widget").is(":visible");
                  })
                  .on("click", function () {
                      input.trigger("focus");
                      // Close if already visible
                      if (wasOpen) {
                          return;
                      }
                      // Pass empty string as value to search for, displaying all results
                      input.autocomplete("search", "");
                  });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text))) {
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    }
                }));
            },

            _removeIfInvalid: function (event, ui) {
                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                  valueLowerCase = value.toLowerCase(),
                  valid = false;
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                //this.input
                //  .val("")
                //  .attr("title", value + " didn't match any item")
                //  .tooltip("open");
                this.element.val("");

                //this._delay(function () {
                //    this.input.tooltip("close").attr("title", "");
                //}, 2500);
                this.input.autocomplete("instance").term = "";

            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        $("#DriverId").combobox();
        $("#CompanyId").combobox();
    });


    $("#driver-error").on("click", "a", function () {
        //Desplegar form para agregar driver
        $("#divAddDriver").show("fade");
    });

    $("#save-driver").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '/Ingates/saveDriver',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ driverName: $("#DriverName").val(), companyId: $("#CompanyId").val() }),
            dataType: "json",
            success: function (data) {
                console.log(data.Id);
                $('#DriverId')
                     .append($('<option>', { value: data.Id })
                     .text(data.Name));
                $("#divAddDriver").hide("fade");
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#company-error").on("click", "a", function () {
        //Desplegar form para agregar company
        $("#divAddCompany").show("fade");
    });

    $("#save-company").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '/Ingates/saveCompany',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ companyName: $("#CompanyName").val() }),
            dataType: "json",
            success: function (data) {
                $('#CompanyId')
                     .append($('<option>', { value: data.Id })
                     .text(data.Name));
                $("#divAddCompany").hide("fade");
            },
            error: function (jqXHR) {
                console.log("Error saving company");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#selectable").selectable({
        selecting: function (event, ui) {
            if ($(".ui-selected, .ui-selecting").length > 1) {
                $('.ui-selecting').removeClass("ui-selecting");
            }
        },
        stop: function () {
            var result = $("#select-result").empty();
            $(".ui-selected", this).each(function () {
                var index = $("#selectable li").index(this);
                $("#YardSpotId").val(parseInt($(this).text()));
            });
        }
    });

    $("#closeDriver").click(function () {
        $("#divAddDriver").hide("fade");
    });

    $("#closeCompany").click(function () {
        $("#divAddCompany").hide("fade");
    });

    function validKey(keycode) {
        //var keycode = e.keyCode;
        var valid =
            (keycode > 46 && keycode < 58) || // delete (46) & number keys
            keycode == 32 || keycode == 8 || // spacebar & return key(s) (if you want to allow carriage returns)
            (keycode > 64 && keycode < 91) || // letter keys
            (keycode > 95 && keycode < 112) || // numpad keys
            (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
            (keycode > 218 && keycode < 223);   // [\]' (in order)

        return valid;
    }
    var typingTimer;
    var timeOut;
    $("#TrailerNumber").keyup(function (e) {
        if (validKey(e.keyCode)) {
            var value = $("#TrailerNumber").val();
            if (value.length == 0) {
                $("#trailer-error").html("");
            } else {
                if (value.length < 11) {
                    console.log("Entro 5000");
                    var timeOut = 2000;
                } else {
                    console.log("No timer Entro");
                    var timeOut = 0;
                }

                clearTimeout(typingTimer);
                typingTimer = setTimeout(function () {
                    console.log("Con Clear Timeout de: " + timeOut);
                    $.ajax({
                        type: "POST",
                        url: '/Ingates/getRouteMasterData',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({ trailer: $("#TrailerNumber").val() }),
                        dataType: "json",
                        success: function (data) {
                            var list = data;
                            if (data.length == 0) {
                                //Si no hay resultados avisamos
                                $("#trailer-error").html("No matches found for that trailer, please fill all fields.");
                                $("#row-TrailerNumber").removeClass("has-success");
                                $("#row-TrailerNumber").addClass("has-danger");
                                clearfields();
                                $("#dontshow").show();
                                $("#necessaryinfo").show();
                                $("#RouteMasterId").val("0");
                            } else {
                                if (data.length == 1) {
                                    clearfields();

                                    //Si solo hay un resultado llenamos los campos automaticamente
                                    $.each(list, function (index, item) {
                                        $("#TrailerNumber").val(item.TrailerNumber.trim());
                                        $("#row-TrailerNumber").removeClass("has-danger");
                                        $("#row-TrailerNumber").addClass("has-success");
                                        $("#necessaryinfo").show();
                                        $("#Route").val(item.Route.trim());
                                        $("#Sequence").val(item.Run);
                                        $("#Priority").val(item.Priority);
                                        $("#Invoice").val(item.Invoice);
                                        $("#RouteMasterId").val(item.Id);
                                        $("#Hazmat").val(item.Hazmat);
                                        $("#Bound").val(1);
                                        $("#TrailerStatusId").val(1);
                                    });

                                    $("#trailer-error").html("");
                                } else {
                                    if (data.length > 1) {
                                        //Si hay mas de un resultado abrimos el modal "ocurrences" para mostrar los resultados
                                        clearfields();
                                        $("#dontshow").hide();
                                        $("#trailer-error").html("");
                                        var value = $("#TrailerNumber").val();
                                        //if (value.length > 2) {
                                        $("#occurrences").find("tbody > tr").remove();
                                        var x = 0;
                                        $.each(list, function (index, item) {
                                            //console.log(index + " ++ " + item.Route);
                                            var d = new Date(item.UnloadDate.match(/\d+/) * 1);
                                            var seq = item.Run;
                                            var route = item.Route.trim();
                                            var trailer = item.TrailerNumber.trim();
                                            var RouteMasterid = item.Id;
                                            //var vrs = trailer + "_" + route + "_" + seq + "_" + RouteMasterid;
                                            if (x == 0) {
                                                $('#occurrences > tbody').append('<tr id="' + RouteMasterid + '"><td>' + trailer + '</td><td>' + route + '</td><td>' + seq + '</td><td>' + "Bound" + '</td><td>' + d.toLocaleDateString() + '</td></tr>');
                                            } else {
                                                $('#occurrences > tbody:last-child').append('<tr id="' + RouteMasterid + '" ><td>' + trailer + '</td><td>' + route + '</td><td>' + seq + '</td><td>' + "Bound" + '</td><td>' + d.toLocaleDateString() + '</td></tr>');
                                            }
                                            x++;
                                            $("#occurences-title").html("Results for: " + value);
                                        });

                                        $('#exampleModal').modal('toggle');
                                        //}
                                    }
                                }
                            }
                        },
                        error: function (jqXHR) {
                            console.log("Error");
                            console.log(jqXHR.responseText);
                        }
                    });
                }, timeOut);
            }
        } else {
            console.log("Invalid Key: " + e.keyCode)
        }
    });

    $("#occurrences tbody").on("click", "tr", function () {
        clearfields();
        $("#dontshow").hide();
        var RouteMasterId = $(this).attr("id");
        $.ajax({
            type: "POST",
            url: '/Ingates/getSingleRouteMasterData',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: RouteMasterId }),
            dataType: "json",
            success: function (data) {
                //Llenamos los campos automaticamente                               
                $("#TrailerNumber").val(data.TrailerNumber.trim());
                $("#row-TrailerNumber").removeClass("has-danger");
                $("#row-TrailerNumber").addClass("has-success");
                $("#necessaryinfo").show();
                $("#Route").val(data.Route.trim());
                $("#Sequence").val(data.Run);
                $("#Invoice").val(data.Invoice);
                $("#Priority").val(data.Priority);
                $("#Hazmat").val(data.Hazmat);
                $("#Bound").val(1);
                $("#TrailerStatusId").val(1);
                //$("#Comments").val(item.Comments.trim());
                $("#RouteMasterId").val(data.Id);
                console.log(data.Id);
                console.log(data.TrailerNumber);
                $('#exampleModal').modal('toggle');
            }
        });
        $("#trailer-error").html("");
    });

    function clearfields() {
        $("#DriverId").val("");
        $("#Route").val("");
        $("#Sequence").val("");
        $("#Invoice").val("");
        $("#Priority").val("");
        $("#Comments").val("");
        $("#Bound").val("");
        $("#TrailerStatusId").val("");
        $("#Hazmat").val("");
    }    

    //$("#DriverId").val("");

    //$("#YardSpotId").val(0);

    //$('#dontshow').hide();
    //$("#necessaryinfo").hide();

    $("#TrailerNumber").focus();

    $("#ingateForm").submit(function () {
        document.getElementById("btnSaveIngate").disabled = true;
        return true;
    });

});