﻿$(document).ready(function () {

    $(function () {
        $.widget("custom.combobox", {
            _create: function () {
                this.wrapper = $("<span>")
                  .addClass("custom-combobox")
                  .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },

            _createAutocomplete: function () {
                var selected = this.element.children(":selected"),
                value = "";
                this.input = $("<input>")
                  .appendTo(this.wrapper)
                  .val(value)
                  .attr({ "title": "", "id": "input" + $(this.element).attr("id") })
                  .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                  .autocomplete({
                      delay: 0,
                      minLength: 0,
                      source: $.proxy(this, "_source"),
                      response: function (event, ui) {
                          // ui.content is the array that's about to be sent to the response callback.
                          if (ui.content.length === 0) {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("No matches found <a id='add-driver' style='cursor:pointer'>Add Driver</a>");
                              } else {
                                  $("#company-error").html("No matches found <a id='add-company' style='cursor:pointer'>Add Company</a>");
                              }
                          } else {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("");
                              } else {
                                  $("#company-error").html("");
                              }
                          }
                      }
                  })
                  .tooltip({
                      classes: {
                          "ui-tooltip": "ui-state-highlight"
                      }
                  });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {                        
                        
                        ui.item.option.selected = true;
                        
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                        $("#DriverId").trigger("change");
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function () {
                var input = this.input,
                  wasOpen = false;

                $("<a>")
                  .attr("tabIndex", -1)
                  .attr("title", "Show All Items")
                  .tooltip()
                  .appendTo(this.wrapper)
                  .button({
                      icons: {
                          primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                  })
                  .removeClass("ui-corner-all")
                  .addClass("custom-combobox-toggle ui-corner-right")
                  .on("mousedown", function () {
                      wasOpen = input.autocomplete("widget").is(":visible");
                  })
                  .on("click", function () {
                      input.trigger("focus");
                      // Close if already visible
                      if (wasOpen) {
                          return;
                      }
                      // Pass empty string as value to search for, displaying all results
                      input.autocomplete("search", "");
                  });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text))) {
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    }
                }));
            },

            _removeIfInvalid: function (event, ui) {
                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                  valueLowerCase = value.toLowerCase(),
                  valid = false;
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                this.element.val("");

                this.input.autocomplete("instance").term = "";

            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        $("#DriverId").combobox();
        $("#CompanyId").combobox();
    });

    $("#driver-error").on("click", "a", function () {
        //Desplegar form para agregar driver
        $("#divAddDriver").addClass("custom-display");
        $("#divAddDriver").removeClass("no-display");
    });

    $("#save-driver").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '../Ingates/saveDriver',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ driverName: $("#DriverName").val(), companyId: $("#CompanyId").val() }),
            dataType: "json",
            success: function (data) {
                $('#DriverId')
                         .append($('<option>', { value: data.Id })
                         .text(data.Name));
                $("#divAddDriver").hide("fade");
            },
            error: function (jqXHR) {
                console.log("Error saving driver");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#company-error").on("click", "a", function () {
        //Desplegar form para agregar company
        $("#divAddCompany").addClass("custom-display");
        $("#divAddCompany").removeClass("no-display");
    });

    $("#save-company").click(function () {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: '../Ingates/saveCompany',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ companyName: $("#CompanyName").val() }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('#CompanyId')
                         .append($('<option>', { value: data.Id })
                         .text(data.Name));
                $("#divAddCompany").hide("fade");
            },
            error: function (jqXHR) {
                console.log("Error saving company");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#closeDriver").click(function ()
    {
        $("#divAddDriver").removeClass("custom-display");
        $("#divAddDriver").addClass("no-display");
    });

    $("#closeCompany").click(function () {
        $("#divAddCompany").removeClass("custom-display");
        $("#divAddCompany").addClass("no-display");
    });


    $("#tabs").tabs();
    var spot = {};
    var routeMasterId = {};
    var selected = {};
    $("#yardspots tbody tr").contextmenu(function () {        
        var clase = $(this).attr("class");
        spot = this.id;
        selected = this;
        switch (clase){
            case "blocked":
                blocked(spot);
                break;
            case "inUse":
                routeMasterId = $(this).attr("data-routeMasterId");
                $.ajax({
                    type: "POST",
                    url: 'getYardComments',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ routeMasterid: routeMasterId }),
                    dataType: "json",
                    success: function (data) {
                        //console.log(data);
                        $("#table-comments").find("tbody > tr").remove();
                        var x = 0;
                        $.each(data, function (index, item) {
                            var d = new Date(item.DateTime.match(/\d+/) * 1);
                            var t = d.toLocaleTimeString();
                            var comment = item.Comment;
                            var user = item.AspNetUsersId;
                            if (x == 0) {
                                $('#table-comments > tbody').append('<tr><td>' + comment + '</td><td>' + user + '</td><td>' + d.toLocaleDateString("en-US") + " "+ t + '</td></tr>');
                            } else {
                                $('#table-comments > tbody:last-child').append('<tr><td>' + comment + '</td><td>' + user + '</td><td>' + d.toLocaleDateString("en-US") + " " + t + '</td></tr>');
                            }
                            x++;
                        });
                    },
                    error: function (jqXHR) {
                        console.log(jqXHR.responseText);
                    }
                });

                $('#addCommentsModal').modal('toggle');
                break;
        }
        return false;
    });

    function blocked(spot) {
        $.ajax({
            type: "POST",
            url: 'getReason',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ spot: spot }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('#reasonText').html(data.reason);
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText);
            }
        });

        $('#reasonModal').modal('toggle');
    }

    $("#save-comment").click(function () {
        $.ajax({
            type: "POST",
            url: 'saveYardComment',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ comment: $("#Comment").val(), routeMasterid: routeMasterId }),
            dataType: "json",
            success: function (data) {
                console.log(data);                
                $("Comment").val("");
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText);
            }
        });
        $('#addCommentsModal').modal('toggle');
    });

    $("#Comment").change(function () {
        if ($(this).val() != "") {
            $("#save-comment").show();
        } else {
            $("#save-comment").hide();
        }
    });

    $("#Comment").keyup(function (e) {        
        var value = $(this).val();
        if (value.length == 0) {
            $("#save-comment").hide();
        } else {
            $("#save-comment").show();
        }
    });

    
    $("#DriverId").change(function () {
        if ($("#DriverId").val() > 0) {
            $("#change-driver").show();
        } else {
            $("#change-driver").hide();
        }
    });

    $('#addCommentsModal').on('hidden.bs.modal', function () {
        $("#tabs").tabs({ active: 0 });
        $("#Comment").val("");
        $("#change-driver").hide();
        $("#save-comment").hide();
    });

    $("#change-driver").click(function () {
        var preOutgateId = $(selected).find('td#driver').attr("data-preoutgate");
        console.log(preOutgateId);
        if (preOutgateId > 0) {
            $.ajax({
                type: "POST",
                url: 'updateDriver',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ preoutgateId: preOutgateId, newDriver: $("#DriverId").val() }),
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $(selected).find('td#driver').html(data);
                },
                error: function (jqXHR) {
                    console.log("Error");
                    console.log(jqXHR.responseText);
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: 'assignDriver',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ routeMasterId: routeMasterId, newDriver: $("#DriverId").val() }),
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $(selected).find('td#driver').html(data);
                },
                error: function (jqXHR) {
                    console.log("Error");
                    console.log(jqXHR.responseText);
                }
            });
        }
        $('#addCommentsModal').modal('toggle');
    });


    //$("#yardspots tbody").on("contextmenu", "tr", function () {        
    //    if ($(this).hasClass("inUse")) {
    //        console.log("En uso " + this.id);
    //        tblDblClick(this);
    //    }
    //    return false;
    //});

    $("#YardSpotId").change(function () {
        if($("#YardSpotId").val() != "Select"){
            $("#change-spot").show();
            console.log("Cambio");
        } else {
            $("#change-spot").hide();
            console.log("No cambio");
        }
    });

    $("#change-spot").click(function () {
        console.log(routeMasterId);
        var newSpot = $("#YardSpotId").val();
        $.ajax({
            type: "POST",
            url: 'changespot',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: routeMasterId, newSpot: newSpot }),
            dataType: "json",
            success: function (data) {                
                if (data.saved == true) {
                    //Cambio
                    $("#confirmText").html('<h4 style="color:#28a745">Spot changed successfully!</h4>');
                    $('#confirmModal').modal('toggle');
                    setTimeout(function () {
                        $('#confirmModal').modal('toggle');
                    }, 2000);
                    location.reload();
                } else {
                    //No cambio
                    $("#confirmText").html('<h4 style="color:#dc3545">Unable to change spot, the selected spot is in use</h4>');
                    $('#confirmModal').modal('toggle');
                    setTimeout(function () {
                        $('#confirmModal').modal('toggle');
                    }, 2000);
                }                
            },
            error: function (jqXHR) {
                console.log("Error saving company");
                console.log(jqXHR.responseText);
            }
        });
    });


    function tblDblClick(elemento) {
        var parts = $(elemento).attr("id").split("&&");
        $('#tblEditDriverModal').modal('toggle');
        $("#tblEditDriver-title").text("Change driver for: " + parts[0]);
        assignationToChange = parts[2];
        rmToSave = parts[3];
        lastDblClicked = elemento;
    }

    $("#btnSubmit").click(function () {
        //window.location = "ExportYardStatus";
        window.location = "ExportViewYardStatus";
    });

    //
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#yardspots tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
