﻿$(document).ready(function () {
    function validKey(keycode) {
        var valid =
            (keycode > 46 && keycode < 58) || // delete (46) & number keys
            keycode == 32 || keycode == 8 || keycode == 13 || // spacebar & enter & return key(s) (if you want to allow carriage returns)
            (keycode > 64 && keycode < 91) || // letter keys
            (keycode > 95 && keycode < 112) || // numpad keys
            (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
            (keycode > 218 && keycode < 223);   // [\]' (in order)
        return valid;
    }

    var typingTimer;
    var timeOut;

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            console.log('yes');
            event.preventDefault();
            return false;
        }
    });

    $("#TrailerNumber").keyup(function (e) {
        if (validKey(e.keyCode)) {
            var value = $("#TrailerNumber").val();
            if (value.length == 0) {
                $("#trailer-error").html("");
            } else {
                if (value.length < 11) {
                    //console.log("Entro 5000");
                    var timeOut = 3000;
                    if (e.keyCode == 13) { //Si es enter que no espere
                        timeOut = 0;
                    }
                } else {
                    //console.log("No timer Entro");
                    var timeOut = 0;
                }
                clearTimeout(typingTimer);
                typingTimer = setTimeout(function () {
                    loadTable();
                }, timeOut);
            }
        } else {
            console.log("Invalid Key: " + e.keyCode)
        }
    });
    
    $("#find").click(function () {
        console.log("Aqui");
        clearTimeout(typingTimer);
        if ($("#TrailerNumber").val() != "") {
            $("#trailer-error").html("");
            loadTable();
        } else {
            $("#trailer-error").html("Please enter a trailer number.");
        }
    });

    function loadTable() {
        $.ajax({
            type: "POST",
            url: 'getRouteMasterData',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ trailer: $("#TrailerNumber").val() }),
            dataType: "json",
            success: function (data) {
                var list = data;
                if (data.length == 0) {
                    //Si no hay resultados avisamos
                    $("#trailer-error").html("No record was found with that trailer number.");
                    $("#row-TrailerNumber").removeClass("has-success");
                    $("#row-TrailerNumber").addClass("has-danger");
                    $("#RouteMasterId").val("0");
                    clearfields();
                } else {
                    if (data.length == 1) {
                        //Si solo hay un resultado llenamos los campos automaticamente
                        $.each(list, function (index, item) {
                            console.log(item.Id);
                            $("#TrailerNumber").val(item.TrailerNumber.trim());
                            $("#RouteMasterId").val(item.Id);
                            $("#row-TrailerNumber").removeClass("has-danger");
                            $("#row-TrailerNumber").addClass("has-success");
                        });
                        $("#trailer-error").html("");
                    } else {
                        if (data.length > 1) {
                            //Si hay mas de un resultado abrimos el modal "ocurrences" para mostrar los resultados                            
                            $("#trailer-error").html("");
                            var value = $("#TrailerNumber").val();
                            $("#occurrences").find("tbody > tr").remove();
                            var x = 0;
                            $.each(list, function (index, item) {
                                //console.log(item);                               
                                var d = new Date(item.UnloadDate.match(/\d+/) * 1);
                                var seq = item.Run;
                                var route = item.Route.trim();
                                var trailer = item.TrailerNumber.trim();
                                var RouteMasterid = item.Id;
                                var bound = (item.DataSourceId == 6 || item.DataSourceId == 9) ? "NB" : "SB";
                                var pr = (item.Priority == null) ? "" : item.Priority;
                                var makeTr = '<tr id="' + RouteMasterid + '"><td>' + trailer + '</td><td>' + route + '</td><td>' + seq + '</td><td>'+bound+'</td><td>'+pr+'</td><td>' + d.toLocaleDateString('en-US') + '</td></tr>';
                                if (x == 0) {
                                    $('#occurrences > tbody').append(makeTr);
                                } else {
                                    $('#occurrences > tbody:last-child').append(makeTr);
                                }
                                x++;
                                clearfields();
                                console.log(value);
                                $("#occurences-title").html("Results for: " + value);
                            });
                            $('#exampleModal').modal('toggle');
                        }
                    }
                }
            },
            error: function (jqXHR) {
                console.log("Error");
                console.log(jqXHR.responseText);
            }
        });
    }

    function clearfields() {
        $("#TrailerNumber").val("");
        $("#Invoice").val("");
    }

    $("#occurrences tbody").on("click", "tr", function () {        
        var RouteMaster = $(this).attr("id");
        $.ajax({
            type: "POST",
            url: '../Ingates/getSingleRouteMasterData',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: RouteMaster }),
            dataType: "json",
            success: function (data) {
                //Llenamos los campos automaticamente       
                console.log(data);
                $("#TrailerNumber").val(data.RouteMaster.TrailerNumber.trim());
                $("#RouteMasterId").val(data.RouteMaster.Id);
                $("#row-TrailerNumber").removeClass("has-danger");
                $("#row-TrailerNumber").addClass("has-success");
                $("#trailer-error").html("");
            }
        });
        $('#exampleModal').modal('toggle');
    });

    $("#Invoice").val("Select");

    $("#updateInvoiceForm").submit(function () {
        console.log($("#Invoice").val());
        if ($("#RouteMasterId").val() == 0) {
            console.log($("#RouteMasterId").val());
            $("#errorText").html("Please enter a valid trailer");
            $('#errorModal').modal('toggle');
            return false;
        } else if ($("#Invoice").val() != "1" && $("#Invoice").val() != "0") {
            console.log($("#Invoice").val());
            $("#errorText").html("Please choose an option");
            $('#errorModal').modal('toggle');
            $('#Invoice').css("border-color",'red');
            //document.getElementById("btnSaveIngate").disabled = true;
            return false;
        } else {
            return true;
        }        
    });
});