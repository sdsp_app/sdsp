﻿$(document).ready(function () {    

    $(function () {
        $.widget("custom.combobox", {

            _create: function () {
                this.wrapper = $("<span>")
                  .addClass("custom-combobox")
                  .insertAfter(this.element);
                
                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },

            _createAutocomplete: function () {                
                var selected = this.element.children(":selected"),
                //value = selected.val() ? selected.text() : "";
                value = "";
                this.input = $("<input>")
                  .appendTo(this.wrapper)
                  .val(value)
                  .attr({"title":"","id":"input"+$(this.element).attr("id")})
                  .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                  .autocomplete({
                      delay: 0,
                      minLength: 0,                      
                      source: $.proxy(this, "_source"),
                      autofocus: true,
                      response: function (event, ui) {
                          // ui.content is the array that's about to be sent to the response callback.
                          if (ui.content.length === 0) {
                              //console.log("No hay resultados " + $(this).attr("id"));
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("No matches found <a id='add-driver' style='cursor:pointer'>Add Driver</a>");
                              } else {
                                  $("#company-error").html("No matches found <a id='add-company' style='cursor:pointer'>Add Company</a>");
                              }                          
                          } else {
                              if ($(this).attr("id") == "inputDriverId") {
                                  $("#driver-error").html("");
                              } else {
                                  $("#company-error").html("");
                              }
                              //$("#empty-message").empty();
                          }
                      }
                  })
                  .tooltip({
                      classes: {
                          "ui-tooltip": "ui-state-highlight"
                      }
                  });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function () {
                var input = this.input,
                  wasOpen = false;

                $("<a>")
                  .attr("tabIndex", -1)
                  .attr("title", "Show All Items")
                  .tooltip()
                  .appendTo(this.wrapper)
                  .button({
                      icons: {
                          primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                  })
                  .removeClass("ui-corner-all")
                  .addClass("custom-combobox-toggle ui-corner-right")
                  .on("mousedown", function () {
                      wasOpen = input.autocomplete("widget").is(":visible");
                  })
                  .on("click", function () {
                      input.trigger("focus");
                      // Close if already visible
                      if (wasOpen) {
                          return;
                      }
                      // Pass empty string as value to search for, displaying all results
                      input.autocomplete("search", "");
                  });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");               
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text))) {                        
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    }
                }));
            },

            _removeIfInvalid: function (event, ui) {
                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                  valueLowerCase = value.toLowerCase(),
                  valid = false;                  
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }                    
                });

                // Found a match, nothing to do
                if (valid) {                    
                    return;
                }

                // Remove invalid value
                //this.input
                //  .val("")
                //  .attr("title", value + " didn't match any item")
                //  .tooltip("open");
                this.element.val("");
                
                //this._delay(function () {
                //    this.input.tooltip("close").attr("title", "");
                //}, 2500);
                this.input.autocomplete("instance").term = "";
                
            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        $("#DriverId").combobox();
        $("#CompanyId").combobox();
    });


    $("#driver-error").on("click", "a", function () {
        //Desplegar form para agregar driver
        $("#divAddDriver").show("fade");
    });

    $("#save-driver").click(function () {
        event.preventDefault();
        if ($("#DriverName").val() != "") {
            $.ajax({
                type: "POST",
                url: 'saveDriver',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ driverName: $("#DriverName").val(), companyId: $("#CompanyId").val() }),
                dataType: "json",
                success: function (data) {
                    console.log(data.Id);
                    if (data.Id == -1) {
                        //Chofer repetido
                        $('#errorModal').modal('toggle');
                    } else {
                        $('#DriverId')
                             .append($('<option>', { value: data.Id })
                             .text(data.Name));
                        $("#DriverName").val("");
                        $("#divAddDriver").hide("fade");
                    }
                },
                error: function (jqXHR) {
                    console.log("Error saving driver");
                    console.log(jqXHR.responseText);
                }
            });
        } else {
            console.log("No Driver Name");
        }
    });

    $("#company-error").on("click", "a", function () {
        //Desplegar form para agregar company
        $("#divAddCompany").show("fade");
    });    

    $("#save-company").click(function () {
        event.preventDefault();
        if ($("#CompanyName").val() != "") {
            $.ajax({
                type: "POST",
                url: 'saveCompany',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ companyName: $("#CompanyName").val() }),
                dataType: "json",
                success: function (data) {
                    console.log("Company saved");
                    if (data.Id == -1) {
                        //Compania repetida
                        $('#errorModal').modal('toggle');
                    } else {
                        $('#CompanyId')
                             .append($('<option>', { value: data.Id })
                             .text(data.Name));
                        $("#divAddCompany").hide("fade");
                        $("#CompanyName").val("");
                    }
                },
                error: function (jqXHR) {
                    console.log("Error saving company");
                    console.log(jqXHR.responseText);
                }
            });
        } else {
            alert("Company name cannot be empty")
        }
    });

    $("#selectable").selectable({
        selecting: function (event, ui) {
            if ($(".ui-selected, .ui-selecting").length > 1) {
                $('.ui-selecting').removeClass("ui-selecting");
            }
        },
        stop: function () {
            var result = $("#select-result").empty();
            $(".ui-selected", this).each(function () {
                var index = $("#selectable li").index(this);
                $("#YardSpotId").val(parseInt($(this).text()));
            });
        }
    });

    $("#closeDriver").click(function () {
        $("#divAddDriver").hide("fade");
    });

    $("#closeCompany").click(function () {
        $("#divAddCompany").hide("fade");
    });

    function validKey(keycode) {
        var valid = 
            (keycode > 46 && keycode < 58)   || // delete (46) & number keys
            keycode == 32 || keycode == 8 || keycode == 13 || // spacebar & enter & return key(s) (if you want to allow carriage returns)
            (keycode > 64 && keycode < 91)   || // letter keys
            (keycode > 95 && keycode < 112)  || // numpad keys
            (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
            (keycode > 218 && keycode < 223);   // [\]' (in order)
        return valid;
    }

    var typingTimer;
    var timeOut;
    $("#TrailerNumber").keyup(function (e) {
        if (checked == 0) {
            if (validKey(e.keyCode)) {
                var value = $("#TrailerNumber").val();
                if (value.length == 0) {
                    $("#trailer-error").html("");
                } else {
                    if (value.length < 11) {
                        timeOut = 3000;
                        if (e.keyCode == 13) { //Si es enter que no espere
                            timeOut = 0;
                        }
                    } else {
                        timeOut = 0;
                    }
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout(function () {
                        loadTable();
                    }, timeOut);
                }
            }
        }
    });



    function loadTable() {
        $.ajax({
            type: "POST",
            url: 'getRouteMasterData',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ trailer: $("#TrailerNumber").val() }),
            dataType: "json",
            success: function (data) {
                var list = data;
                if (data.length == 0) {
                    //Si no hay resultados avisamos
                    $("#trailer-error").html("No matches found for that trailer, please fill all fields.");
                    $("#manual").prop("checked", true);
                    checked = 1;
                    $("#row-TrailerNumber").removeClass("has-success");
                    $("#row-TrailerNumber").addClass("has-danger");
                    clearfields();
                    $("#dontshow").show();
                    $("#necessaryinfo").show();
                    $('#dontshow').css('display', 'flex');
                    $("#necessaryinfo").css('display', 'flex');
                    $("#RouteMasterId").val("0");
                } else {
                    if (data.length == 1) {
                        //Si solo hay un resultado llenamos los campos automaticamente
                        console.log(data[0]);
                        clearfields();
                        $("#manual").prop("checked", false);
                        checked = 0;
                        fillFields(data[0]);                        
                    } else {
                        if (data.length > 1) {
                            console.log(data);
                            //Si hay mas de un resultado abrimos el modal "ocurrences" para mostrar los resultados
                            clearfields();
                            $("#manual").prop("checked", false);
                            checked = 0;
                            //$("#dontshow").hide();
                            $("#trailer-error").html("");
                            var value = $("#TrailerNumber").val();
                            $("#occurrences").find("tbody > tr").remove();
                            var x = 0;
                            $.each(list, function (index, item) {
                                item = item.RouteMaster;
                                var d = new Date(item.UnloadDate.match(/\d+/) * 1);
                                var seq = (item.Run == null)?  "" : item.Run;
                                var route = item.Route.trim();
                                var trailer = item.TrailerNumber.trim();
                                var bound = (item.DataSourceId == 6 || item.DataSourceId == 9) ? "NB" : "SB";
                                var RouteMasterid = item.Id;
                                var priority = item.Priority
                                var makeTr = '<tr id="' + RouteMasterid + '"><td>' + trailer + '</td><td>' + route + '</td><td>' + seq + '</td><td>' + bound + '</td><td>' + d.toLocaleDateString("en-US") + '</td><td>'+priority+'</td></tr>';
                                if (x == 0) {
                                    $('#occurrences > tbody').append(makeTr);
                                } else {
                                    $('#occurrences > tbody:last-child').append(makeTr);
                                }
                                x++;
                                $("#occurences-title").html("Results for: " + value);
                            });
                            $('#exampleModal').modal('toggle');
                        }
                    }
                }
            },
            error: function (jqXHR) {
                console.log("Error");
                console.log(jqXHR.responseText);
            }
        });
    }

    //console.log("Nuevo north");

    function fillFields(item) {
        north = item.Northbound;
        item = item.RouteMaster;
        $("#TrailerNumber").val(item.TrailerNumber.trim());
        $("#row-TrailerNumber").removeClass("has-danger");
        $("#row-TrailerNumber").addClass("has-success");
        $("#necessaryinfo").show();
        $("#necessaryinfo").css('display', 'flex');
        //$("#dontshow").hide();
        $("#Route").val(item.Route.trim());
        $("#Sequence").val(item.Run);
        $("#Priority").val(item.Priority);
        $("#Invoice").val(item.Invoice);
        $("#RouteMasterId").val(item.Id);
        $("#Hazmat").val(item.Hazmat);
        if (north != null) {
            $("#Bound").val(2);
            $("#TrailerStatusId").val(north.TrailerStatusId);
            $("#DriverId").val(north.DriverId);
        } else {
            $("#Bound").val(1);
            $("#TrailerStatusId").val(1);
        }

        //Al llenar los campos automaticamente desabilitamos los campos de bound y trailer status
        //document.getElementById("Bound").disabled = true;
        //document.getElementById("TrailerStatusId").disabled = true;
        $('#Bound option:not(:selected)').attr('disabled', true);
        $('#TrailerStatusId option:not(:selected)').attr('disabled', true);

        $("#trailer-error").html("");
    }

    $("#occurrences tbody").on("click", "tr", function () {
        clearfields();
        //$("#dontshow").hide();
        var RouteMasterId = $(this).attr("id");
        $.ajax({
            type: "POST",
            url: 'getSingleRouteMasterData',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: RouteMasterId }),
            dataType: "json",
            success: function (data) {
                //Llenamos los campos automaticamente       
                fillFields(data);         
                $('#exampleModal').modal('toggle');
            }
        });
        $("#trailer-error").html("");
    });

    function clearfields() {
        $("#DriverId").val("");
        $("#Route").val("");
        $("#Sequence").val("");
        $("#Invoice").val("");
        $("#Priority").val("");
        $("#Comments").val("");
        $("#Bound").val("");
        $("#TrailerStatusId").val("");
        $("#Hazmat").val("");
        //Al limpiar campos, volvemos a habilitar bound y trailer status
        //document.getElementById("Bound").disabled = false;
        //document.getElementById("TrailerStatusId").disabled = false;
        $('#Bound option:not(:selected)').attr('disabled', false);
        $('#TrailerStatusId option:not(:selected)').attr('disabled', false);
    }

    $("#DriverId").val("");

    $("#YardSpotId").val(0);

    //if ($("#Error").val() == 1) {
    //    console.log("Error caja");
    //    $("#divError").show("fadeup");
    //}
    //if ($("#Error").val() == 2) {
    //    console.log("Error spot");
    //    $("#errorText").text("That spot is in use, please choose another one");
    //    $("#divError").show("fadeup");
    //}
    //if ($("#Error").val() == 0) {
        
    //}
    //if ($("#Error").val() == 3) {
    //    $("#errorText").text("Validation Errors! No record was saved");
    //    $("#divError").show("fadeup");
    //}
    var error = parseInt($("#Error").val());   

    switch (error) {
        case 0:
            console.log("Switch 0");
            $("#divMessage").show("fadeup");
            break;
        case 1:
            console.log("Switch 1");
            $("#divError").show("fadeup");
            break;
        case 2:
            console.log("Switch 2");
            $("#errorText").text("That spot is in use, please choose another one");
            $("#divError").show("fadeup");
            break;
        case 3:
            console.log("Switch 3");
            $("#errorText").text("Validation Errors! No record was saved");
            $("#divError").show("fadeup");
            //document.getElementById("Bound").disabled = false;
            //document.getElementById("TrailerStatusId").disabled = false;
            $('#Bound option:not(:selected)').attr('disabled', false);
            $('#TrailerStatusId option:not(:selected)').attr('disabled', false);
            break;
        default:
            console.log("unknown: "+error);
            break;
    }

    //$('#dontshow').hide();
    //$("#necessaryinfo").hide();


    $("#TrailerNumber").focus();

    $("#ingateForm").submit(function () {
        console.log($("#YardSpotId").val());
        if ($("#YardSpotId").val() == 0) {
            $('#confirmModal').modal('toggle');
            return false;
        }else{
           document.getElementById("btnSaveIngate").disabled = true;
            return true;
        }
    });

    $("#register-manual").click(function () {
        //Si no hay resultados avisamos
        $("#manual").prop("checked", true);
        checked = 1;
        $("#trailer-error").html("No matches found for that trailer, please fill all fields.");
        $("#row-TrailerNumber").removeClass("has-success");
        $("#row-TrailerNumber").addClass("has-danger");
        clearfields();
        $("#dontshow").show();
        $("#necessaryinfo").show();
        $('#dontshow').css('display', 'flex');
        $("#necessaryinfo").css('display', 'flex');
        $("#RouteMasterId").val("0");
    });

    var checked = 0;

    $('#manual').change(function () {
        if (this.checked) {
            checked = 1;            
            //Si no hay resultados avisamos
            //$("#trailer-error").html("No matches found for that trailer, please fill all fields.");
            $("#row-TrailerNumber").removeClass("has-success");
            $("#row-TrailerNumber").addClass("has-danger");
            clearfields();
            $("#dontshow").show();
            $("#necessaryinfo").show();
            $('#dontshow').css('display', 'flex');
            $("#necessaryinfo").css('display', 'flex');
            $("#RouteMasterId").val("0");
            console.log("manual entry");
        } else {
            checked = 0
            clearfields();
            //$("#dontshow").hide();
            //$("#necessaryinfo").hide();
            $("#trailer-error").html("");
            console.log("not manual entry");
        }
    });


    $("#closeb, #closex").click(function () {
        console.log("Se cerro el modal 3");
        $("#TrailerNumber").focus();
        console.log("Despues");
    });
});