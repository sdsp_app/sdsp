﻿$(document).ready(function () {
    $("#companies tbody").on("click", "a", function () {
        //console.log(this.id);
        var parts = this.id.split("-");
        selected = parseInt(parts[1]);
        console.log(selected);
        if ((this.id).includes("Delete")) {
            $('#confirmModal').modal('toggle');
        }
        else if ((this.id).includes("Edit")) {
            getCompanyInfo(selected);
            $('#editModal').modal('toggle');
        }
    });

    function getCompanyInfo(driverId) {
        $.ajax({
            type: "POST",
            url: 'Companies/getInfo',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                $("#Name").val(data.Name);
                console.log(data);
            },
            error: function (jqXHR) {
                console.log("Error retrieving");
                console.log(jqXHR.responseText);
            }
        });
    }

    $("#saveRecord").click(function () {
        var name = $("#Name").val();
        $.ajax({
            type: "POST",
            url: 'Companies/editCompany',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ name: name, id: selected }),
            dataType: "json",
            success: function (data) {
                if (data.saved == true) {
                    $("#messageText").html("Successfully saved!!");
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                }
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#deleteRecord").click(function () {
        $.ajax({
            type: "POST",
            url: 'Companies/delete',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                if (data.deleted == true) {
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                }
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    function loadTable() {
        $("#companies").find("tbody > tr").remove();
        $("#searchBox").val("");
        $.ajax({
            type: "POST",
            url: 'Companies/reloadIndex',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var x = 0;
                $.each(data, function (index, item) {
                    console.log(item);
                    var company = item.Name;
                    var makeTr = '<tr><td>' + company + '</td><td><a id="Edit-' + item.Id + '" href="#">Edit</a> | <a id="Delete-' + item.Id + '" href="#">Delete</a></td></tr>';
                    if (x == 0) {
                        $('#companies > tbody').append(makeTr);
                    } else {
                        $('#companies > tbody:last-child').append(makeTr);
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    }

})