﻿$(document).ready(function () {

    //jQuery.browser = {};
    //(function () {
    //    jQuery.browser.msie = false;
    //    jQuery.browser.version = 0;
    //    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
    //        jQuery.browser.msie = true;
    //        jQuery.browser.version = RegExp.$1;
    //    }
    //})();
    
    //$("table.tablesorter").tablesorter({ widthFixed: true, sortList: [[0, 0]] })
    //.tablesorterPager({ container: $("#pager"), size: 15 });
    
    $("#searchBox").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".tablesorter tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

});