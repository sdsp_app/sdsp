﻿$(document).ready(function () {
    $("#drivers tbody").on("click", "a", function () {
        //console.log(this.id);
        var parts = this.id.split("-");
        selected = parseInt(parts[1]);
        console.log(selected);
        if ((this.id).includes("Delete")) {
            $('#confirmModal').modal('toggle');
        }
        else if ((this.id).includes("Edit")) {
            getDriverInfo(selected);
            $('#editModal').modal('toggle');
        }
    });

    function getDriverInfo(driverId) {
        $.ajax({
            type: "POST",
            url: 'Drivers/getInfo',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                $("#Name").val(data.Name);
                $("#Code").val(data.DriverCode);
                $("#IdCompany").val(data.IdCompany);
                console.log(data);
            },
            error: function (jqXHR) {
                console.log("Error retrieving");
                console.log(jqXHR.responseText);
            }
        });
    }

    $("#saveRecord").click(function () {
        var name = $("#Name").val();
        var code = $("#Code").val();
        var company = $("#IdCompany").val();
        $.ajax({
            type: "POST",
            url: 'Drivers/editDriver',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ name: name, code: code, company: company, id: selected }),
            dataType: "json",
            success: function (data) {
                if (data.saved == true) {
                    $("#messageText").html("Successfully saved!!");
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                }
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#deleteRecord").click(function () {
        $.ajax({
            type: "POST",
            url: 'Drivers/delete',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                if (data.deleted == true) {
                    $("#messageText").html("Successfully deleted!!");
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                }
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    function loadTable() {
        $("#drivers").find("tbody > tr").remove();
        $("#searchBox").val("");
        $.ajax({
            type: "POST",
            url: 'Drivers/reloadIndex',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var x = 0;
                $.each(data, function (index, item) {
                    console.log(item);
                    var driver = item.Driver.Name;
                    var code = item.Driver.DriverCode;
                    var company = item.Company.Name
                    var makeTr = '<tr><td>' + driver + '</td><td>' + code + '</td><td>' + company + '</td><td><a id="Edit-' + item.Driver.Id + '" href="#">Edit</a> | <a id="Delete-' + item.Driver.Id + '" href="#">Delete</a></td></tr>';
                    if (x == 0) {
                        $('#drivers > tbody').append(makeTr);
                    } else {
                        $('#drivers > tbody:last-child').append(makeTr);
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    }
})