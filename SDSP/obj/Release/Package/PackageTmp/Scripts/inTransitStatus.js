﻿$(document).ready(function () {
    var time = 0;
    var action = function () {
        $('#inTransit > tbody tr').each(function () {
            var element = this;            
            setTimeout(function () {
                updateTable();
                $(element).siblings().first().before($(element));
            }, time);
            time += 3000;    
        })
    };

    $("#btnSubmit").click(function () {
        window.location = "ExportRealTime";
    });
    //setInterval(action, time);
    //action();
    
    function updateTable() {
        $.ajax({
            type: "POST",
            url: 'reloadRealTime',
            contentType: "application/json; charset=utf-8",
            success: function (data) {                
                //$("#inTransit").find("tbody > tr").remove();
                var x = 0;
                $.each(data, function (index, item) {                                        
                    var clase = "";
                    var driver = "";
                    if (item.outDriver != null) {
                        driver = (item.DriverCode == "N/A" || item.DriverCode == "" || item.DriverCode == null) ? item.DriverName : item.DriverCode;
                    }
                    if (item.outTime != null) {
                        clase = "ontheway";
                        clase = (item.PreIngateBCDate != null) ? "preassigned" : clase;
                        clase = (item.IngateBcDateTime != null) ? "inbc" : clase;
                    }
                    else {
                        clase = (item.ingateTime != null) ? "inUse" : clase;
                    }
                    var spot = (item.ingatespot != null) ? item.ingatespot : "";
                    var bcspot = (item.IngateBCSpot != null) ? item.IngateBCSpot : "";
                    var unloadDate = (item.UnloadDate != null) ? new Date(item.UnloadDate.match(/\d+/) * 1).toLocaleString("en-US") : "";
                    var outgateTime = (item.outTime != null) ? new Date(item.outTime.match(/\d+/) * 1).toLocaleTimeString("en-US") : "";                    
                    var id = item.Id;
                    var makeTr = "";
                    //makeTr += "<tr class=" + clase + ">";
                    makeTr += "<td>" + item.Priority + "</td>";
                    makeTr += "<td>" + item.Invoice + "</td>";
                    makeTr += "<td>" + item.Route + "-" + item.Run + "</td>";
                    makeTr += "<td>" + item.TrailerNumber + "</td>";
                    makeTr += "<td>" + unloadDate + "</td>";
                    makeTr += "<td>" + driver + "</td>";
                    makeTr += "<td>" + spot + "</td>";
                    makeTr += "<td>" + outgateTime + "</td>";
                    makeTr += "<td>" + bcspot + "</td>";
                    $("#" + id).removeClass();
                    $("#" + id).addClass(clase);
                    $("#"+id).html(makeTr);
                    //makeTr = "</tr>";
                    //if (x == 0) {
                    //    $('#inTransit > tbody').append(makeTr);
                    //} else {
                    //    $('#inTransit > tbody:last-child').append(makeTr);
                    //}
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log("Error reloading");
                console.log(jqXHR.responseText);
            }
        });
    }
    
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#inTransit tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#preassgined-square").html($('.preassigned').length);
    $("#inbc-square").html($('.inbc').length);
    $("#available-square").html($('.pending').length);
    $("#inuse-square").html($('.inUse').length);
    $("#ontheway-square").html($('.ontheway').length);
    $("#pullahead-square").html($('.pullahead').length);
});