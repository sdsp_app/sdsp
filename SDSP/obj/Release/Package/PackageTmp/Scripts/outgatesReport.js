﻿$(document).ready(function () {

    $("#btnSubmit").click(function () {
        var start = $("#startDate").val();
        var end = $("#endDate").val();
        window.location = "Export?start=" + start + "&end=" + end;
    });

    var selected;
    var routeMasterId;
    //$("a[id^='Delete-']").click(function () {
    //    var parts = this.id.split("-");
    //    selected = parseInt(parts[1]);
    //    console.log(selected);
    //    $('#confirmModal').modal('toggle');
    //});

    $("#Trailers tbody").on("click", "a", function () {
        //console.log(this.id);
        if ((this.id).includes("Delete")) {
            var parts = this.id.split("-");
            selected = parseInt(parts[1]);
            routeMasterId = parseInt(parts[2]);
            console.log(selected);
            $('#confirmModal').modal('toggle');
        }
    });

    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#Trailers tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#YardSpotId").change(function () {
        if ($("#YardSpotId").val() != "Select") {
            $("#change-spot").show();
            console.log("Cambio");
        } else {
            $("#change-spot").hide();
            console.log("No cambio");
        }
    });

    $("#change-spot").click(function () {
        console.log(routeMasterId);
        var newSpot = $("#YardSpotId").val();
        $.ajax({
            type: "POST",
            url: 'changespot',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ routeMasterId: routeMasterId, newSpot: newSpot }),
            dataType: "json",
            success: function (data) {
                if (data.saved == true) {
                    //Cambio
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                } else {
                    //No cambio
                    $("#resultText").html('<h4 style="color:#dc3545">Unable to delete outgate and change spot, the selected spot is in use, please try again.</h4>');
                    $('#resultModal').modal('toggle');
                    setTimeout(function () {
                        $('#resultModal').modal('toggle');
                        location.reload();
                    }, 4000);                    
                }
            },
            error: function (jqXHR) {
                console.log("Error in changespot request");
                console.log(jqXHR.responseText);
            }
        });
    });

    $("#yes").click(function () {
        $.ajax({
            type: "POST",
            url: 'delete',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ id: selected }),
            dataType: "json",
            success: function (data) {
                if (data.deleted == true) {
                    $("#divMessage").fadeTo(2000, 500).slideUp(500, function () {
                        $("#divMessage").slideUp(500);
                    });
                    loadTable();
                } else {
                    //No se pudo borrar, porque el antiguo spot esta ocupado, debemos mandar el modal para escoger spot
                    $('#changeSpotModal').modal('toggle');
                }
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    });

    //Testing
    //$('#changeSpotModal').modal('toggle');

    function loadTable() {
        $("#Trailers").find("tbody > tr").remove();
        $.ajax({
            type: "POST",
            url: 'reloadoutgatesReport',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ startDate: $("#startDate").val(), endDate: $("#endDate").val() }),
            dataType: "json",
            success: function (data) {
                console.log(data);
                var x = 0;
                $.each(data.outgatesReport, function (index, item) {
                    console.log(item);
                    var trailer = item.TrailerNumber;
                    var route = item.Route;
                    var run = item.Run;
                    if (item.UnloadDate != null) {
                        var uD = new Date(item.UnloadDate.match(/\d+/) * 1);
                        uD = uD.toLocaleString("en-US");
                    } else {
                        var uD = "";
                    }
                    var iD = new Date(item.OutgateDateTime.match(/\d+/) * 1);

                    var bound = (item.Bound == 1) ? "Southbound" : "Northbound";

                    iD = iD.toLocaleString("en-US");
                    var driver = item.DriverName;
                    var makeTr = '<tr><td>' + trailer + '</td><td>' + route + '</td><td>' + run + '</td><td>' + iD + '</td><td>' + driver + '</td><td>' + bound + '</td><td><a id="Delete-' + item.Id + '" href="#">Delete</a></td></tr>';
                    if (x == 0) {
                        $('#Trailers > tbody').append(makeTr);
                    } else {
                        $('#Trailers > tbody:last-child').append(makeTr);
                    }
                    x++;
                });
            },
            error: function (jqXHR) {
                console.log("Error deleting");
                console.log(jqXHR.responseText);
            }
        });
    }


});