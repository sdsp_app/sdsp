﻿using SDSP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SDSP.Class
{
    public class DataSourceHelper
    {
        ServiceReference1.SDSPServiceClient wcf = new ServiceReference1.SDSPServiceClient();
        public SelectList getSpots()
        {
            List<SelectListItem> SpotsList = new List<SelectListItem>();
            var spotlst = wcf.getYardSpots(2);
            foreach (var item in spotlst)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.SpotNumber.ToString();
                SpotsList.Add(lst);
            }
            return new SelectList(SpotsList, "Value", "Text");
        }

        public SelectList getSpotstoChange()
        {
            List<SelectListItem> SpotsList = new List<SelectListItem>();            
            SpotsList.Add(new SelectListItem {
                Value = "Select",
                Text = "Select"
            });
            var spotlst = wcf.getYardSpots(2);
            foreach (var item in spotlst)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.SpotNumber.ToString();
                SpotsList.Add(lst);
            }
            return new SelectList(SpotsList, "Value", "Text");
        }

        public SelectList getEditingateSpots(long? id)
        {
            List<SelectListItem> SpotsList = new List<SelectListItem>();
            var spotlst = wcf.getYardSpotsbyIngate((int)id);
            foreach (var item in spotlst)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.SpotNumber.ToString();
                SpotsList.Add(lst);
            }
            return new SelectList(SpotsList, "Value", "Text");
        }

        public SelectList getDrivers()
        {
            List<SelectListItem> DriversList = new List<SelectListItem>();
            var driverslst = wcf.GetDriverCompanyList(2);
            foreach (var item in driverslst)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Driver.Id.ToString();
                lst.Text = item.Driver.Name.ToString() + " - " + item.Company.Name.ToString();
                DriversList.Add(lst);
            }
            return new SelectList(DriversList, "Value", "Text");
        }

        public SelectList getCompanies()
        {
            List<SelectListItem> CompaniesList = new List<SelectListItem>();
            var companylst = wcf.GetCompaniesList();
            foreach (var item in companylst)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.Name.ToString();
                CompaniesList.Add(lst);
            }
            return new SelectList(CompaniesList, "Value", "Text");
        }

        public List<YardSpot> getAllSpots() {
            List<YardSpot> SpotsList = new List<YardSpot>();
            var spotlst = wcf.getYardSpots(0);
            foreach (var item in spotlst)
            {
                YardSpot lst = new YardSpot();
                lst.Id = item.Id;
                lst.SpotNumber = item.SpotNumber;
                lst.SpotStatusId = item.SpotStatusId;
                SpotsList.Add(lst);
            }

            return SpotsList;
        }

        public SelectList getIssueStatus()
        {
            List<SelectListItem> issueStatusList = new List<SelectListItem>();
            var issueStatusResult = wcf.getIssueStatuses();
            foreach (var item in issueStatusResult)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.Name.ToString();
                issueStatusList.Add(lst);
            }
            return new SelectList(issueStatusList, "Value", "Text");
        }

        public SelectList getSelectedIssueStatus(int id)
        {
            List<SelectListItem> issueStatusList = new List<SelectListItem>();
            var issueStatusResult = wcf.getIssueStatuses();
            foreach (var item in issueStatusResult)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.Name.ToString();
                lst.Selected = (item.Id == id);
                issueStatusList.Add(lst);
            }
            return new SelectList(issueStatusList, "Value", "Text",id);
        }

        public SelectList getBound() {
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem() { Text = "Southbound", Value = "1" });
            lst.Add(new SelectListItem() { Text = "Northbound", Value = "2" });

            return new SelectList(lst, "Value", "Text");
        }

        public SelectList getTrailerStatus()
        {
            List<SelectListItem> trailerStatusList = new List<SelectListItem>();
            var trailerStatus = wcf.getTrailerStatus();
            foreach (var item in trailerStatus)
            {
                SelectListItem lst = new SelectListItem();
                lst.Value = item.Id.ToString();
                lst.Text = item.Status.ToString();
                trailerStatusList.Add(lst);
            }
            return new SelectList(trailerStatusList, "Value", "Text");
        }

        public SelectList getYesno()
        {
            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "No", Value = "0"},
                new SelectListItem{Text = "Yes", Value = "1"},
            };

            return new SelectList(items, "Value", "Text");
        }

        public SelectList getYesNoSelect() {
            List<SelectListItem> options = new List<SelectListItem>();
            options.Add(new SelectListItem{Value = "Select",Text = "Select"});
            options.Add(new SelectListItem { Text = "No", Value = "0" });
            options.Add(new SelectListItem { Text = "Yes", Value = "1" });

            return new SelectList(options, "Value", "Text");
        }

        public SelectList getMissingExceding()
        {
            List<SelectListItem> lst = new List<SelectListItem> {
                new SelectListItem() { Text = "Missing Trailers", Value = "1" },
                new SelectListItem() { Text = "Exceeded Trailers", Value = "2" }
            };

            return new SelectList(lst, "Value", "Text");
        }

        public SelectList getHazmat()
        {
            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "No", Value = "N"},
                new SelectListItem{Text = "Yes", Value = "Y"},
            };

            return new SelectList(items, "Value", "Text");
        }
    }
}