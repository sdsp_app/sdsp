//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfSDSP
{
    using System;
    using System.Collections.Generic;
    
    public partial class OutgatesReport
    {
        public long Id { get; set; }
        public Nullable<System.DateTime> OutgateDateTime { get; set; }
        public Nullable<long> OutgateDriverId { get; set; }
        public Nullable<long> OutgateRouteMasterId { get; set; }
        public long IdDriver { get; set; }
        public string DriverName { get; set; }
        public Nullable<long> IdCompany { get; set; }
        public string AspNetUsersId { get; set; }
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyUserId { get; set; }
        public long idRouteMaster { get; set; }
        public string Hazmat { get; set; }
        public Nullable<System.DateTime> UnloadDate { get; set; }
        public string RouteRun { get; set; }
        public string Route { get; set; }
        public string Run { get; set; }
        public string PuRoute2 { get; set; }
        public string RouteType { get; set; }
        public string RouteMiles { get; set; }
        public string LegMiles { get; set; }
        public string Seq { get; set; }
        public string SName { get; set; }
        public string LDK { get; set; }
        public string LogisticsPointId { get; set; }
        public Nullable<System.DateTime> ArrvDate { get; set; }
        public Nullable<System.TimeSpan> ArrvTime { get; set; }
        public Nullable<System.DateTime> DepDate { get; set; }
        public Nullable<System.TimeSpan> DepTime { get; set; }
        public string Timezone { get; set; }
        public string RoutesMilestone { get; set; }
        public string Day { get; set; }
        public Nullable<int> PeriodId { get; set; }
        public Nullable<System.DateTime> RegisterDate { get; set; }
        public Nullable<int> Priority { get; set; }
        public string TrailerNumber { get; set; }
        public string Comments { get; set; }
        public Nullable<int> DataSourceId { get; set; }
        public Nullable<int> ExpressPriority { get; set; }
        public string Invoice { get; set; }
        public Nullable<System.DateTime> PullAhead { get; set; }
        public Nullable<long> RouteMasterId { get; set; }
        public long Expr1 { get; set; }
        public Nullable<int> Bound { get; set; }
    }
}
