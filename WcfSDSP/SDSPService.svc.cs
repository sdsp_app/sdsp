﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfSDSP
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SDSPService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SDSPService.svc or SDSPService.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class SDSPService : ISDSPService
    {
        public List<IngatesReport> GetIngatesList(DateTime? date)
        {
            List<IngatesReport> ingateList = new List<IngatesReport>();
            SDSPEntities db = new SDSPEntities();

            if (date == null)
            {
                var result = from ir in db.IngatesReports
                             //where ir.IngateDateTime >= start && ir.IngateDateTime <= end
                             orderby ir.IngateDateTime
                             select ir;

                foreach (var item in result)
                {
                    IngatesReport iObj = new IngatesReport
                    {
                        YardSpotId = item.YardSpotId,
                        Id = item.Id,
                        IngateRouteMasterId = item.IngateRouteMasterId,
                        IngateDriverId = item.IngateDriverId,
                        IngateDateTime = item.IngateDateTime,
                        CompanyName = item.CompanyName,
                        DriverName = item.DriverName,
                        TrailerNumber = item.TrailerNumber,
                        Route = item.Route,
                        Run = item.Run,
                        UnloadDate = item.UnloadDate
                    };
                    ingateList.Add(iObj);
                }            
            }
            return ingateList;
        }

        public List<PreOutgate> GetPreoutgatesList()
        {
            List<PreOutgate> preOutgateList = new List<PreOutgate>();
            SDSPEntities db = new SDSPEntities();
            var queryPreOutGate = from k in db.PreOutgates select k;
            foreach (var item in queryPreOutGate)
            {                
                preOutgateList.Add(new PreOutgate
                {
                    Id = item.Id,
                    DriverId = item.DriverId,
                    RouteMasterId = item.RouteMasterId,
                    DateTime = item.DateTime
                });
            }
            return preOutgateList;
        }

        public bool checkspot(int? YardSpotID)
        {
            List<YardSpot> spotsLibres = getYardSpots(2);
            return spotsLibres.Any(x => x.Id == YardSpotID);
        }

        public long? SaveIngate(long? RouteMasterId, string AspNetUsersId, int? YardSpotId, long? DriverId, int? bound, int? trailerStatus)
        {
            SDSPEntities db = new SDSPEntities();
            var result = (from i in db.Ingates
                          where i.RouteMasterId == RouteMasterId
                          select i).FirstOrDefault();

            if (result == null)
            {
                //Si no hay ingate aun para esa caja
                bool check = checkspot(YardSpotId);
                if (check == false)
                {
                    //El spot ya fue asignado
                    return -2;
                }
                else
                {
                    Ingate i = new Ingate
                    {
                        RouteMasterId = RouteMasterId,
                        AspNetUsersId = AspNetUsersId,
                        YardSpotId = YardSpotId,
                        DriverId = DriverId,
                        DateTime = DateTime.Now,
                        Bound = bound,
                        TrailerStatusId = trailerStatus
                    };

                    db.Ingates.Add(i);

                    if (db.SaveChanges() > 0)
                    {
                        useSpot(YardSpotId);
                        return i.Id;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else {
                //Si ya hay un ingate para esa caja
                return -1;
            }
        }        

        public void useSpot(int? spot) {            
            SDSPEntities db = new SDSPEntities();
            var result = db.YardSpots.SingleOrDefault(b => b.Id == spot);
            if (result != null)
            {
                result.SpotStatusId = 3;
                db.SaveChanges();
            }
        }

        public long? saveManualIngate(string AspNetUsersId, int? YardSpotId, string hazmat, long? DriverId, string trailerNumber, string route, string sequence, string invoice, string comments, int? bound, int? trailerStatus)
        {
            SDSPEntities db = new SDSPEntities();
            int period = getCurrentPeriod();
            int? datasource;
            trailerNumber = trailerNumber.Trim();
            trailerNumber = trailerNumber.ToUpper();

            if (!checkspot(YardSpotId))
            {
                //El spot ya fue asignado
                return -2;
            }
            else
            {
                //Revisar si la caja ya esta en yarda de SD aunque no este en el intransit
                var inYard = (from y in db.YardStatus
                          where
                          y.TrailerNumber == trailerNumber
                          select y).FirstOrDefault();

                if (inYard != null)
                {
                    //Se regresa -1 para controlar el error en la app
                    return -1;
                }
                else
                {
                    //Bound 2 es al norte       
                    if (bound == 2)
                    {
                        //DataSource = 6 - Ingate Manual al norte
                        datasource = 6;                    
                    }
                    else
                    {
                        //DataSource = 4 - Ingate Manual al sur
                        datasource = 4;
                        DateTime today = DateTime.Now.Date;
                        //Revisar si la caja ya esta en el intransit Edit 11/20: Agregar condicion para que no tome en cuenta cajas que ya fueron procesadas
                        var exists = (from r in db.RouteMasters
                                      where r.TrailerNumber == trailerNumber &&
                                          r.RegisterDate == today &&
                                          r.Priority > 0 &&
                                          !db.Outgates.Select(e => e.RouteMasterId).Contains(r.Id) &&
                                          !db.PreOutgates.Select(e => e.RouteMasterId).Contains(r.Id) &&
                                          !db.Ingates.Select(e => e.RouteMasterId).Contains(r.Id)
                                      select r).FirstOrDefault();
                        if (exists != null)
                        {
                            //La caja ya estaba en el intransit, hacemos un ingate normal con el routemasterId de la caja existente
                            return SaveIngate(exists.Id, AspNetUsersId, YardSpotId, DriverId, bound, trailerStatus);
                        }
                    }
                
                    //Checar si ruta y secuencia vienen en el mismo campo
                    if (route.ToLower().Contains('-') || route.Any(x => Char.IsWhiteSpace(x)))
                    {
                        route = new String((route.Where(Char.IsLetter).ToArray()));
                        sequence = new String((route.Where(Char.IsDigit).ToArray()));
                    }

                    string routeRun = (sequence == "") ? route : route + "-" + sequence;

                    if (hazmat != "" && hazmat != null) {
                        hazmat = (hazmat == "N" || hazmat == "No") ? "" : "HAZMAT";                        
                    }

                    RouteMaster rmObj = new RouteMaster
                    {
                        TrailerNumber = trailerNumber,
                        Route = route,
                        Run = sequence,
                        RouteRun = routeRun,
                        Invoice = invoice,
                        DataSourceId = datasource,
                        PeriodId = period,
                        UnloadDate = DateTime.Now.Date,
                        RegisterDate = DateTime.Now.Date,
                        RoutesMilestone = "YARD ARRIVAL",
                        PuRoute2 = "",
                        SName = "TMMBC YARD",
                        Hazmat = hazmat
                    };

                    db.RouteMasters.Add(rmObj);
                    db.SaveChanges();

                    long rmId = rmObj.Id;

                    Ingate i = new Ingate
                    {
                        RouteMasterId = rmId,
                        AspNetUsersId = AspNetUsersId,
                        YardSpotId = YardSpotId,
                        DriverId = DriverId,
                        DateTime = DateTime.Now,
                        Bound = bound,
                        TrailerStatusId = trailerStatus
                    };
                
                    db.Ingates.Add(i);
                    db.SaveChanges();
                    useSpot(YardSpotId);
                    return i.Id;
                }
            }
        }

        public List<YardSpot> getYardSpots(int status)
        {
            //Primero empatamos la yarda
            UpdateYard();

            List<YardSpot> freeSpotList = new List<YardSpot>();
            SDSPEntities db = new SDSPEntities();

            var spots = (status == 0) ? from k in db.YardSpots orderby k.Id select k : from k in db.YardSpots where k.SpotStatusId == status orderby k.Id select k;

            foreach (var item in spots)
            {
                freeSpotList.Add(new YardSpot
                {
                    Id = item.Id,
                    SpotNumber = item.SpotNumber,
                    SpotStatusId = item.SpotStatusId
                });
            }

            return freeSpotList;
        }

        public void UpdateYard() {
            //Metodo para empatar la yarda (status de los spots en sistema contra el status del spot en la tabla yardspot)

            SDSPEntities db = new SDSPEntities();
            //Traemos los ids de los spots que tengan status 2 (libre) pero que tengan un ingate activo en yarda.
            var wrongIngates = (from y in db.YardStatus
                           where y.IngateId != null && y.SpotStatusId == 2
                           select y.Id).ToList();

            if (wrongIngates.Count > 0)
            {
                foreach (var item in wrongIngates)
                {
                    YardSpot spot = db.YardSpots.Find(item);
                    //A los spots que estaban incorrectos le colocamos el status 3 (En uso)
                    spot.SpotStatusId = 3;
                    db.SaveChanges();
                }
            }

            //Estos son los ids de los spots que no tienen ingate pero estan con status 3 (En uso, hay que liberarlos)
            var wrongSpots = (from y in db.YardStatus
                            where y.IngateId == null && y.SpotStatusId == 3
                            select y.Id).ToList();

            if (wrongSpots.Count > 0)
            {
                foreach (var item in wrongSpots)
                {
                    YardSpot spot = db.YardSpots.Find(item);
                    //A los spots que estaban incorrectos le colocamos el status 2 (Libre)
                    spot.SpotStatusId = 2;
                    db.SaveChanges();
                }
            }
        }

        public List<YardSpot> getYardSpotsbyIngate(int ingateYardSpot)
        {            
            List<YardSpot> freeSpots = new List<YardSpot>();
            SDSPEntities db = new SDSPEntities();
            Ingate ingate = db.Ingates.Find(ingateYardSpot);
            int? spot = ingate.YardSpotId;
            var spotList = from k in db.YardSpots where k.SpotStatusId == 2 || k.Id == spot select k;

            foreach (var item in spotList)
            {
                freeSpots.Add(new YardSpot
                {
                    Id = item.Id,
                    SpotNumber = item.SpotNumber,
                    SpotStatusId = item.SpotStatusId
                });
            }

            return freeSpots;
        }

        public List<DriverCompanyVM> GetDriverCompanyList(int type)
        {
            List<DriverCompanyVM> drivers = new List<DriverCompanyVM>();
            SDSPEntities db = new SDSPEntities();

            //Choferes Activos
            var hookTrailers = (
                from p in db.PreOutgates
                where
                !db.Outgates.Select(e => e.RouteMasterId).Contains(p.RouteMasterId)                
                select p.DriverId
            ).ToList();
            if (type == 3)
            {
                //BC y SDSP
                var driverslist = (from d in db.Drivers
                                   join c in db.Companies on d.IdCompany equals c.Id
                                   where d.Active == true &&
                                   !hookTrailers.Contains(d.Id)
                                   orderby d.Name
                                   select new { driver = d, company = c });
                foreach (var item in driverslist)
                {
                    DriverCompanyVM dcl = new DriverCompanyVM();
                    dcl.Driver = new Driver { Id = item.driver.Id, Name = item.driver.Name, DriverCode = item.driver.DriverCode };
                    dcl.Company = new Company { Name = item.company.Name };
                    drivers.Add(dcl);
                }
            }
            else
            {
                //Distinguimos el tipo
                var driverslist = (from d in db.Drivers
                                   join c in db.Companies on d.IdCompany equals c.Id
                                   where d.Active == true &&
                                   c.CompanySourceId == type &&
                                   !hookTrailers.Contains(d.Id)
                                   orderby d.Name
                                   select new { driver = d, company = c });
                foreach (var item in driverslist)
                {
                    DriverCompanyVM dcl = new DriverCompanyVM();
                    dcl.Driver = new Driver { Id = item.driver.Id, Name = item.driver.Name, DriverCode = item.driver.DriverCode };
                    dcl.Company = new Company { Name = item.company.Name };
                    drivers.Add(dcl);
                }
            }            

            return drivers;
        }

        public List<Company> GetCompaniesList()
        {
            List<Company> companies = new List<Company>();
            SDSPEntities db = new SDSPEntities();
            var lstUsr = from k in db.Companies
                         where                          
                         //k.Name != "Placeholder"
                         k.Active == true
                         select k;

            foreach (var item in lstUsr)
            {
                Company company = new Company();
                company.Id = item.Id;
                company.Name = item.Name;
                companies.Add(company);
            }

            return companies;
        }

        public Driver SaveDriver(string driverName, string companyId, string driverCode, string user)
        {
            SDSPEntities db = new SDSPEntities();

            int company = int.Parse(companyId);

            //Revisar si ya existe un chofer con ese nombre
            var duplicates = (from d in db.Drivers
                             where d.Name == driverName &&
                             d.IdCompany == company
                             select d).FirstOrDefault();

            if (duplicates == null) {
                //No hay duplicados, agregamos el chofer a la tabla
                Driver driver = new Driver {
                    Name = driverName,
                    IdCompany = company,
                    DriverCode = (driverCode == "SDSP") ? null : driverCode,
                    AspNetUsersId = user,
                    Active = true,
                };
                db.Drivers.Add(driver);
                db.SaveChanges();

                return driver;
            }
            //Si llega aqui quiere decir que el chofer esta repetido, regresamos -1 para manejar el error en la app
            return new Driver { Id = -1 };
        }

        public Company SaveCompany(string companyName, string user)
        {
            SDSPEntities db = new SDSPEntities();
            //Revisar si ya existe una compania con ese nombre
            var duplicates = (from d in db.Companies
                              where d.Name == companyName
                              select d).FirstOrDefault();

            if (duplicates == null) { 
                Company company = new Company {
                    Name = companyName,
                    Active = true,
                    AspNetUsersId = user,
                    CompanySourceId = 2
                };

                db.Companies.Add(company);
                db.SaveChanges();

                return company;
            }

            //Si llega aqui quiere decir que esta repetido, regresamos -1 para manejar el error en la app
            return new Company { Id = -1 };
        }

        public bool SaveIngateComment(string comment, long? ingateId, string user)
        {
            SDSPEntities db = new SDSPEntities();

            db.Comments.Add(new Comment
            {
                IngateId = ingateId,
                Comment1 = comment,
                AspNetUsersId = user,
            });            

            return (db.SaveChanges() > 0);
        }

        public List<RouteMaster> searchTrailer(string trailer)
        {
            SDSPEntities db = new SDSPEntities();
            int period = getCurrentPeriod();
            DateTime today = DateTime.Now.Date;
            var resultado =
                from p in db.RouteMasters
                where
                p.TrailerNumber.Equals(trailer) &&
                p.PeriodId == period &&
                p.DataSourceId != 9 &&
                p.DataSourceId != 6 &&
                !db.Outgates.Select(e => e.RouteMasterId).Contains(p.Id) &&
                //Agregamos que tenga prioridad del dia, Joel confirmo que no se ponen prioridades adelantadas 
                //y las que se van quedando se vuelven a agregar el intransit del dia siguiente asi que se puede filtrar por prioridades del dia en curso.
                p.Priority > 0 &&
                p.RegisterDate == today
                //&& !db.PreOutgates.Select(e => e.RouteMasterId).Contains(p.Id)
                //p.UnloadDate >= today
                //(p.Invoice == null || p.Invoice == "")
                orderby p.RegisterDate descending
                select p;

            List<RouteMaster> lstRouteMaster = new List<RouteMaster>();
            int i = 0;
            foreach (var item in resultado)
            {
                //if (i == 0 || !lstRouteMaster.Any(x => x.TrailerNumber == item.TrailerNumber && x.Route == item.Route))
                //{
                    lstRouteMaster.Add(new RouteMaster
                    {
                        Id = item.Id,
                        TrailerNumber = item.TrailerNumber,
                        Run = item.Run,
                        Route = item.Route,
                        Priority = item.Priority,
                        UnloadDate = item.UnloadDate,
                        DataSourceId = item.DataSourceId
                    });
                //}
                i++;
            }

            return lstRouteMaster;
        }

        public List<RouteMaster> getRouteMasterDatabyTrailerNumber(string trailer)
        {
            SDSPEntities db = new SDSPEntities();
            //int period = getCurrentPeriod();
            DateTime today = DateTime.Now.Date;
            var resultado =
                from p in db.RouteMasters
                where 
                p.TrailerNumber.Contains(trailer) && 
                //p.PeriodId == period &&
                p.UnloadDate >= today &&
                p.DataSourceId != 9 &&
                p.DataSourceId != 6 && 
                !db.Outgates.Select(e => e.RouteMasterId).Contains(p.Id) &&
                !db.PreOutgates.Select(e => e.RouteMasterId).Contains(p.Id) &&
                !db.Ingates.Select(e => e.RouteMasterId).Contains(p.Id)
                orderby p.RegisterDate descending
                select p;

            List<RouteMaster> lstRouteMaster = new List<RouteMaster>();
            int i = 0;
            foreach (var item in resultado)
            {                
                if (i == 0 || !lstRouteMaster.Any(x => x.TrailerNumber == item.TrailerNumber && x.Route == item.Route))
                {
                    lstRouteMaster.Add(new RouteMaster
                    {
                        Id = item.Id,
                        TrailerNumber = item.TrailerNumber,
                        Run = item.Run,
                        Route = item.Route,
                        Priority = item.Priority,
                        UnloadDate = item.UnloadDate,
                        Hazmat = item.Hazmat,
                        Invoice = item.Invoice,
                        DataSourceId = item.DataSourceId,
                    });
                }
                i++;
            }

            var resultadoNorte =
                from p in db.RouteMasters
                where
                p.TrailerNumber.Contains(trailer) &&
                //p.PeriodId == period &&
                p.UnloadDate >= today &&
                (p.DataSourceId == 9 || p.DataSourceId == 6) &&
                !db.Outgates.Select(e => e.RouteMasterId).Contains(p.Id) &&
                !db.PreOutgates.Select(e => e.RouteMasterId).Contains(p.Id) &&
                !db.Ingates.Select(e => e.RouteMasterId).Contains(p.Id)
                orderby p.RegisterDate descending
                select p;

            int c = 0;
            foreach (var item in resultadoNorte)
            {
                if (c == 0 || lstRouteMaster.Any(x => x.TrailerNumber == item.TrailerNumber && x.Route == item.Route && x.RegisterDate < item.RegisterDate))
                {
                    lstRouteMaster.Add(new RouteMaster
                    {
                        Id = item.Id,
                        TrailerNumber = item.TrailerNumber,
                        Run = item.Run,
                        Route = item.Route,
                        Priority = item.Priority,
                        UnloadDate = item.UnloadDate,
                        Hazmat = item.Hazmat,
                        Invoice = item.Invoice,
                        DataSourceId = item.DataSourceId,
                    });
                }
                c++;
            }
            return lstRouteMaster;
        }

        public RouteMaster getSingleRouteMasterData(long routeMasterId)
        {
            SDSPEntities db = new SDSPEntities();
            var resultado = (from p in db.RouteMasters
                where p.Id == routeMasterId
                select p).First();
            
            return new RouteMaster
            {
                Id = resultado.Id,
                TrailerNumber = resultado.TrailerNumber,
                Run = resultado.Run,
                Route = resultado.Route,
                Priority = resultado.Priority,
                UnloadDate = resultado.UnloadDate,
                Hazmat = resultado.Hazmat,
                Invoice = resultado.Invoice,
                DataSourceId = resultado.DataSourceId,
            };
        }

        public Northbound getNorthInfo(long routeMasterId) {
            SDSPEntities db = new SDSPEntities();

            var record = (from n in db.Northbounds
                         where n.RouteMasterId == routeMasterId
                         select n).FirstOrDefault();

            Northbound oNorth = new Northbound {
                RouteMasterId = record.RouteMasterId,
                TrailerStatusId = record.TrailerStatusId,
                DriverId = record.DriverId
            };

            return oNorth;
        }

        public long? SavePreOutGate(long? routeMasterId, long? driverId, string user)
        {
            SDSPEntities db = new SDSPEntities();
            var result = (from p in db.PreOutgates
                          where p.RouteMasterId == routeMasterId
                          select p).FirstOrDefault();
            if (result != null) {
                return -1;
            }
            else
            {
                PreOutgate preOutGate = new PreOutgate {
                    RouteMasterId = routeMasterId,
                    DriverId = driverId,
                    DateTime = DateTime.Now,
                };                

                db.PreOutgates.Add(preOutGate);

                return (db.SaveChanges() > 0) ? preOutGate.Id : 0;
            }
        }

        public PrintHookVM printHook(long? preOutGateid)
        {
            PrintHookVM vmObj = new PrintHookVM();
            SDSPEntities db = new SDSPEntities();

            var hookInfo = (from p in db.PreOutgates
                            join d in db.Drivers on p.DriverId equals d.Id
                            join c in db.Companies on d.IdCompany equals c.Id
                            join pl in db.RouteMasters on p.RouteMasterId equals pl.Id
                            join i in db.Ingates on pl.Id equals i.RouteMasterId
                            where p.Id == preOutGateid
                            select new PrintHookVM { Driver = d, Company = c, PreOutgate = p, RouteMaster = pl, Ingate = i }).First();

            vmObj.Driver = new Driver { Id = hookInfo.Driver.Id, Name = hookInfo.Driver.Name, IdCompany = hookInfo.Driver.IdCompany };
            vmObj.Company = new Company { Id = hookInfo.Company.Id, Name = hookInfo.Company.Name };
            vmObj.Ingate = new Ingate { Id = hookInfo.Ingate.Id, YardSpotId = hookInfo.Ingate.YardSpotId };
            vmObj.RouteMaster = new RouteMaster { Id = hookInfo.RouteMaster.Id, TrailerNumber = hookInfo.RouteMaster.TrailerNumber };

            return vmObj;
        }


        public PrintDropVM printDrop(long? ingateId)
        {
            PrintDropVM dropObj = new PrintDropVM();
            SDSPEntities db = new SDSPEntities();
            var dropInfo = (from i in db.Ingates
                            join d in db.Drivers on i.DriverId equals d.Id
                            join c in db.Companies on d.IdCompany equals c.Id
                            join p in db.RouteMasters on i.RouteMasterId equals p.Id
                            where i.Id == ingateId
                            select new PrintDropVM { Driver = d, Company = c, Ingate = i, RouteMaster = p }).First();

            dropObj.Driver = new Driver { Id = dropInfo.Driver.Id, Name = dropInfo.Driver.Name, IdCompany = dropInfo.Driver.IdCompany };
            dropObj.Company = new Company { Id = dropInfo.Company.Id, Name = dropInfo.Company.Name };
            dropObj.Ingate = new Ingate { Id = dropInfo.Ingate.Id, YardSpotId = dropInfo.Ingate.YardSpotId };
            dropObj.RouteMaster = new RouteMaster { Id = dropInfo.RouteMaster.Id, TrailerNumber = dropInfo.RouteMaster.TrailerNumber };
            return dropObj;
        }

        public IngateTrailersVM getIngatebyId(long? ingateId) {
            SDSPEntities db = new SDSPEntities();

            Ingate ingateObj = db.Ingates.Find(ingateId);

            RouteMaster rmObj = db.RouteMasters.Find(ingateObj.RouteMasterId);

            IngateTrailersVM itVM = new IngateTrailersVM {
                Ingate = ingateObj,
                RouteMaster = rmObj
            };

            return itVM;
        }

        public List<IngateTrailersVM> GetIngateTrailers(int tipo)
        {
            List<IngateTrailersVM> lstIngateTrailers = new List<IngateTrailersVM>();
            SDSPEntities db = new SDSPEntities();

            if (tipo == 1)
            {
                DateTime today = DateTime.Now.Date;
                DateTime expiration = today.AddDays(2);

                var ingates = from i in db.Ingates
                            join c in db.Comments on i.Id equals c.IngateId
                            join r in db.RouteMasters on i.RouteMasterId equals r.Id
                            //join ie in db.InvoiceExpirations on r.Id equals ie.RouteMasterId
                            where
                            !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                            !db.PreOutgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                            //Si es southbound tiene que tener prioridad y "Y" o no se debe mostrar en hook
                            r.Priority > 0 && i.Bound == tipo && r.Invoice != null && r.Invoice != "" && r.Invoice != "0"
                            //Agregamos condicion para la validez de la factura
                            //ie.ExpirationDate <= expiration
                            orderby r.Priority
                            select new { RouteMaster = r, Ingate = i, Comment = c };

                foreach (var item in ingates)
                {
                    var yardComments = (from y in db.YardComments
                                        where y.RouteMasterId == item.RouteMaster.Id
                                        orderby y.Id descending
                                        select y).FirstOrDefault();

                    string comment = (yardComments != null) ? yardComments.Comment : item.Comment.Comment1;

                    //Obtenemos el comentarios que se haya hecho cuando se le cambio el orden a la caja, si es que paso.
                    var priorityComment = (from p in db.PriorityChangesLogs
                                           where p.RouteMasterId == item.RouteMaster.Id
                                           orderby p.Id descending
                                           select p).FirstOrDefault();
                    
                    comment = (priorityComment != null) ? priorityComment.Comment + " | " + comment : comment;

                    lstIngateTrailers.Add(new IngateTrailersVM {
                        Comment = new Comment
                        {
                            Comment1 = comment
                        },
                        Ingate = new Ingate
                        {
                            Id = item.Ingate.Id,
                            YardSpotId = item.Ingate.YardSpotId,
                            TrailerStatusId = item.Ingate.TrailerStatusId,
                        },
                        RouteMaster = new RouteMaster
                        {
                            Id = item.RouteMaster.Id,
                            TrailerNumber = item.RouteMaster.TrailerNumber,
                            Priority = item.RouteMaster.Priority,
                            Run = item.RouteMaster.Run,
                            Route = item.RouteMaster.Route,
                            UnloadDate = item.RouteMaster.UnloadDate,
                            Invoice = item.RouteMaster.Invoice,
                            Hazmat = item.RouteMaster.Hazmat,
                            ExpressPriority = item.RouteMaster.ExpressPriority
                        }
                    });
                }

                //Si es Southbound con prioridad ordenarlos por prioridad
                var getExpress = db.PriorityChangesLogs.Select(e => e).Where(e => e.DateTime > today).Count();
                lstIngateTrailers = (getExpress > 0) ? lstIngateTrailers.OrderBy(x => x.RouteMaster.ExpressPriority).ToList() : lstIngateTrailers;
            }
            else {
                //Si el tipo == 3 (Southbound no priority) Le ponemos tipo = 1 para que tome las sb
                tipo = (tipo == 3) ? 1 : 2;
                
                var ingates = from i in db.Ingates
                                join c in db.Comments on i.Id equals c.IngateId
                                join r in db.RouteMasters on i.RouteMasterId equals r.Id
                                where
                                !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                                !db.PreOutgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                                i.Bound == tipo
                                orderby i.YardSpotId
                                select new { RouteMaster = r, Ingate = i, Comment = c };

                foreach (var item in ingates)
                {
                    var yardComments = (from y in db.YardComments
                                        where y.RouteMasterId == item.RouteMaster.Id
                                        orderby y.Id descending
                                        select y).FirstOrDefault();

                    string comment = (yardComments != null) ? yardComments.Comment : item.Comment.Comment1;

                    lstIngateTrailers.Add(new IngateTrailersVM
                    {
                        Comment = new Comment
                        {
                            Comment1 = comment
                        },
                        Ingate = new Ingate
                        {
                            Id = item.Ingate.Id,
                            YardSpotId = item.Ingate.YardSpotId,
                            TrailerStatusId = item.Ingate.TrailerStatusId,
                        },
                        RouteMaster = new RouteMaster
                        {
                            Id = item.RouteMaster.Id,
                            TrailerNumber = item.RouteMaster.TrailerNumber,
                            Priority = item.RouteMaster.Priority,
                            Run = item.RouteMaster.Run,
                            Route = item.RouteMaster.Route,
                            UnloadDate = item.RouteMaster.UnloadDate,
                            Invoice = item.RouteMaster.Invoice,
                            Hazmat = item.RouteMaster.Hazmat,
                            ExpressPriority = item.RouteMaster.ExpressPriority
                        }
                    });
                }
            }

            return lstIngateTrailers;
        }

        public List<PrintHookVM> GetPreoutgateTrailers()
        {
            List<PrintHookVM> trailersList = new List<PrintHookVM>();
            using (SDSPEntities db = new SDSPEntities())
            {
                var hookTrailers =  from p in db.PreOutgates
                                        join r in db.RouteMasters on p.RouteMasterId equals r.Id
                                        join d in db.Drivers on p.DriverId equals d.Id
                                        join c in db.Companies on d.IdCompany equals c.Id
                                        join i in db.Ingates on r.Id equals i.RouteMasterId
                                        where
                                        !db.Outgates.Select(e => e.RouteMasterId).Contains(r.Id)
                                        orderby i.YardSpotId
                                        select new PrintHookVM { Driver = d, Company = c, PreOutgate = p, RouteMaster = r, Ingate = i};

                foreach (var item in hookTrailers)
                {
                    var yardComments = (from y in db.YardComments
                                        where y.RouteMasterId == item.RouteMaster.Id
                                        orderby y.Id descending
                                        select y).FirstOrDefault();

                    var ingatecomment = (from c in db.Comments
                                         where c.IngateId == item.Ingate.Id
                                         select c).FirstOrDefault();

                    string comment = (yardComments != null) ? yardComments.Comment : ingatecomment.Comment1;

                    PrintHookVM tl = new PrintHookVM();
                    tl.Driver = new Driver { Id = item.Driver.Id, Name = item.Driver.Name };
                    tl.Company = new Company { Name = item.Company.Name, Id = item.Company.Id };
                    tl.Ingate = new Ingate { Id = item.Ingate.Id, YardSpotId = item.Ingate.YardSpotId };
                    tl.PreOutgate = new PreOutgate { Id = item.PreOutgate.Id };
                    tl.RouteMaster = new RouteMaster
                    {
                        Id = item.RouteMaster.Id,
                        Route = item.RouteMaster.Route,
                        Run = item.RouteMaster.Run,
                        Priority = item.RouteMaster.Priority,
                        TrailerNumber = item.RouteMaster.TrailerNumber,
                        UnloadDate = item.RouteMaster.UnloadDate,
                        ExpressPriority = item.RouteMaster.ExpressPriority,
                        Comments = comment
                    };
                    trailersList.Add(tl);
                }
            }
            return trailersList;
        }

        public List<RouteMaster> saveDispatches(IDictionary<string, int> trailers, string user)
        {
            List<RouteMaster> skippedList = new List<RouteMaster>();
            DateTime today2 = DateTime.Now.Date;
            
            using (var db = new SDSPEntities())
            {                
                //var getExpress = db.PriorityChangesLogs.Select(e => e).Where(e => e.DateTime > today2.Date).Count();
                
                foreach (var item in trailers)
                {
                    
                    int rmId = int.Parse(item.Key);
                    var result = (from i in db.Outgates
                                  where i.RouteMasterId == rmId
                                  select i).FirstOrDefault();
                    if (result == null)
                    {
                        //Lo que esta comentado era para determinar cuantas prioridades se estaban saltando
                        //RouteMaster skipped = new RouteMaster();
                        ////Saca la prioridad del RM
                        //var rmPriority = (from r in db.RouteMasters where r.Id == rmId select r).First();
                        //int period = getCurrentPeriod();
                        //if (getExpress > 0)
                        //{

                        //    var listPriorities = from r in db.RouteMasters
                        //                         where
                        //                         r.UnloadDate >= today2.Date &&
                        //                         r.PeriodId == period &&
                        //                         !db.Outgates.Select(e => e.RouteMasterId).Contains(r.Id)
                        //                         && r.ExpressPriority != null
                        //                         orderby r.ExpressPriority ascending
                        //                         select r;
                        //    foreach (var current in listPriorities)
                        //    {
                        //        if (rmPriority.ExpressPriority > current.ExpressPriority)
                        //        {
                        //            skipped = new RouteMaster();
                        //            skipped.Id = current.Id;
                        //            skipped.TrailerNumber = current.TrailerNumber;
                        //            skipped.Route = current.Route;
                        //            skipped.Run = current.Run;
                        //            skipped.ExpressPriority = current.ExpressPriority;
                        //            if (!skippedList.Any(x => x.Id == skipped.Id))
                        //            {
                        //                skippedList.Add(skipped);
                        //            }                                
                        //        }

                        //    }
                        //}
                        //else
                        //{
                        //    var listPriorities = from r in db.RouteMasters
                        //                         where
                        //                         r.UnloadDate >= today2.Date &&
                        //                         r.PeriodId == period &&
                        //                         !db.Outgates.Select(e => e.RouteMasterId).Contains(r.Id)
                        //                         && r.Priority != null
                        //                         orderby r.Priority ascending
                        //                         select r;
                        //    foreach (var current in listPriorities)
                        //    {
                        //        if (rmPriority.Priority > current.Priority)
                        //        {
                        //            skipped = new RouteMaster();
                        //            skipped.Id = current.Id;
                        //            skipped.TrailerNumber = current.TrailerNumber;
                        //            skipped.Route = current.Route;
                        //            skipped.Run = current.Run;
                        //            skipped.Priority = current.Priority;
                        //            if (!skippedList.Any(x => x.Id == skipped.Id))
                        //            {
                        //                skippedList.Add(skipped);
                        //            }
                        //        }
                        //    }
                        //}

                        //Traemos el ingate para poder dejar libre el spot de nuevo.
                        var ingateRecord = db.Ingates.SingleOrDefault(b => b.RouteMasterId == rmId);

                        if (ingateRecord != null)
                        {
                            var result2 = db.YardSpots.SingleOrDefault(b => b.Id == ingateRecord.YardSpotId);
                            if (result2 != null)
                            {
                                result2.SpotStatusId = 2;
                                db.SaveChanges();
                            }
                        }

                        //Salvamos el outgate
                        Outgate outgate = new Outgate
                        {
                            DriverId = item.Value,
                            RouteMasterId = rmId,
                            DateTime = DateTime.Now,
                            AspNetUsersId = user
                        };
                        db.Outgates.Add(outgate);
                        db.SaveChanges();
                    }
                    else {                        
                        //RouteMaster skipped = new RouteMaster();
                        //RouteMaster skipped = db.RouteMasters.Find(rmId);
                        skippedList.Add(db.RouteMasters.Find(rmId));                        
                    }
                }
            }
            return skippedList;
        }

        public long updateAssignedDriver(int preoutgateId, int newDriver, string user)
        {
            int updatesCount = 0;
            SDSPEntities db = new SDSPEntities();

            //var result = db.PreOutgates.SingleOrDefault(b => b.Id == preoutgateId);
            PreOutgate record = db.PreOutgates.Find(preoutgateId);

            if (record != null)
            {
                //Antes de editar salvamos log
                PreOutgateLog logObj = new PreOutgateLog
                {
                    PreOutgateId = record.Id,
                    OriginalAspNetUsersId = record.AspNetUsersId,
                    OriginalDateTime = record.DateTime,
                    ChangeTypeId = 1,
                    DriverId = record.DriverId,
                    RouteMasterId = record.RouteMasterId,
                    DateTime = DateTime.Now,
                    AspNetUsersId = user
                };

                db.PreOutgateLogs.Add(logObj);
                db.SaveChanges();

                record.DriverId = newDriver;
                updatesCount = db.SaveChanges();
            }

            return preoutgateId;
        }

        public int saveInTransitBulk(DataTable inTransitData, string fileName, string user)
        {
            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["SQLBulkConnection"].ConnectionString))
            {
                sqlBulk.DestinationTableName = "InTransit";
                sqlBulk.WriteToServer(inTransitData);
                File oFile = new File
                {
                    FileName = fileName,
                    UploadDate = DateTime.Now,
                    FileTypeId = 3,
                    AspNetUsersId = user
                };

                SDSPEntities db = new SDSPEntities();
                db.Files.Add(oFile);
                db.SaveChanges();
            }
            int period = getCurrentPeriod();
            int updatedcounts = 0;
            List<string> procesados = new List<string>();
            foreach (DataRow row in inTransitData.Rows)
            {
                int ispotSDSP = 0;
                int.TryParse(row["SpotSDSP"].ToString(), out ispotSDSP);
                int ispotTMMB = 0;
                int priority = 0;
                int.TryParse(row["Priority"].ToString(), out priority);
                int.TryParse(row["Spot"].ToString(), out ispotTMMB);
                DateTime? dtSDSPDispatch = (!string.IsNullOrEmpty(row["SDSPDispatch"].ToString())) ? DateTime.Parse(row["SDSPDispatch"].ToString()) : (DateTime?)null;
                DateTime? dtTMMBCArrives = (!string.IsNullOrEmpty(row["TMMBCArrives"].ToString())) ? DateTime.Parse(row["TMMBCArrives"].ToString()) : (DateTime?)null;
                DateTime? dtUnloadDate = (!string.IsNullOrEmpty(row["UnloadDate"].ToString())) ? DateTime.Parse(row["UnloadDate"].ToString()) : (DateTime?)null;

                InTransit oInTransit = new InTransit
                {
                    Priority = priority,
                    Route = row["Route"].ToString(),
                    Origin = row["Origin"].ToString(),
                    OrdersLines = row["OrdersLines"].ToString(),
                    MainShuttle = row["MainShuttle"].ToString(),
                    Lines = row["Lines"].ToString(),
                    L1 = row["L1"].ToString(),
                    L2 = row["L2"].ToString(),
                    L3 = row["L3"].ToString(),
                    L4 = row["L4"].ToString(),
                    L5 = row["L5"].ToString(),
                    L6 = row["L6"].ToString(),
                    L7 = row["L7"].ToString(),
                    L8 = row["L8"].ToString(),
                    L9 = row["L9"].ToString(),
                    L10 = row["L10"].ToString(),
                    L11 = row["L11"].ToString(),
                    L12 = row["L12"].ToString(),
                    TrailerNumber = row["TrailerNumber"].ToString(),
                    UnloadDate = dtUnloadDate,
                    Hazmat = row["Hazmat"].ToString(),
                    F1 = row["F1"].ToString(),
                    OW = row["OW"].ToString(),
                    PacerStatus = row["PacerStatus"].ToString(),
                    PacerTime = row["PacerTime"].ToString(),
                    SpotSDSP = ispotSDSP,
                    StatusComments = row["StatusComments"].ToString(),
                    SDSPDispatch = dtSDSPDispatch,
                    TMMBCArrives = dtTMMBCArrives,
                    Spot = ispotTMMB,
                    D = row["D"].ToString(),
                    PullAhead = row["PullAhead"].ToString(),
                };

                oInTransit.TrailerNumber = oInTransit.TrailerNumber.Trim();
                oInTransit.TrailerNumber = oInTransit.TrailerNumber.ToUpper();

                if (oInTransit.Route.ToLower().Contains('-') || oInTransit.Route.Any(x => Char.IsWhiteSpace(x)))
                {
                    oInTransit.Route = new String((oInTransit.Route.Where(Char.IsLetter).ToArray()));
                    oInTransit.Origin = new String((oInTransit.Route.Where(Char.IsDigit).ToArray()));
                }

                string routeRun = (oInTransit.Origin == "") ? oInTransit.Route : oInTransit.Route + "-" + oInTransit.Origin;
                DateTime? PullAhead = (oInTransit.PullAhead == "y") ? DateTime.Now.Date : (DateTime?)null;

                //Si en el intransit la fecha de descarga viene vacia, le asignamos la fecha de descarga del dia para ver 
                //que no se repitan registros si es que el intransit ya se habia subido anteriormente durante el dia.
                oInTransit.UnloadDate = (oInTransit.UnloadDate == null) ? DateTime.Now.Date : oInTransit.UnloadDate;

                if (!procesados.Contains(oInTransit.TrailerNumber))
                {
                    var db = new SDSPEntities();
                    
                    //Buscar si esta ya en la yarda de san diego
                    var ingate = (from i in db.YardStatus
                                          where i.TrailerNumber == oInTransit.TrailerNumber &&
                                          i.Bound == 1
                                          select i).FirstOrDefault();

                    if (ingate != null){                        
                        //Si esta en la yarda, hay que actualizarle los datos
                        RouteMaster found = db.RouteMasters.Find(ingate.RouteMasterId);
                        found.PullAhead = PullAhead;
                        found.TrailerNumber = oInTransit.TrailerNumber;
                        found.Hazmat = oInTransit.Hazmat;
                        found.Route = oInTransit.Route;
                        found.Run = oInTransit.Origin;
                        found.RouteRun = routeRun;
                        found.PeriodId = period;
                        //Si el intransit tiene fecha de descarga (mayor a la del dia) se le pone, si no se le deja la que tenia
                        found.UnloadDate = (oInTransit.UnloadDate > DateTime.Now.Date) ? oInTransit.UnloadDate : found.UnloadDate;
                        //found.Invoice = oInTransit.F1;
                        found.DataSourceId = 11;
                        found.RegisterDate = DateTime.Now.Date;
                        if (!string.IsNullOrEmpty(oInTransit.StatusComments))
                        {
                            found.Comments = found.Comments + " | " + oInTransit.StatusComments;
                        }
                        found.Priority = oInTransit.Priority;
                        //Si se encuentra se reinicia la prioridad express
                        found.ExpressPriority = null;
                        updatedcounts += db.SaveChanges();
                    }else{
                        //No la encontro en la yarda, la busca en routemaster
                        var result = db.RouteMasters.FirstOrDefault(
                            b =>
                            b.Route == oInTransit.Route &&
                            b.Run == oInTransit.Origin &&
                            b.UnloadDate == oInTransit.UnloadDate &&
                            b.TrailerNumber == oInTransit.TrailerNumber &&
                            b.SName == "TMMBC YARD" &&
                            (b.RoutesMilestone.Equals("YARD ARRIVAL") || b.RoutesMilestone.Equals("drop")) &&
                            (b.PuRoute2 != "OTR Mexico" && b.PuRoute2 != "air")
                        );
                        if (result != null)
                        {
                            //Encontro el registro, solo falta actualizarle los datos        
                            result.PullAhead = PullAhead;
                            result.TrailerNumber = oInTransit.TrailerNumber;
                            result.PeriodId = period;
                            result.Hazmat = oInTransit.Hazmat;
                            //result.Invoice = oInTransit.F1;
                            result.DataSourceId = 3;
                            result.RegisterDate = DateTime.Now.Date;
                            if (!string.IsNullOrEmpty(oInTransit.StatusComments))
                            {
                                result.Comments = result.Comments + " | " + oInTransit.StatusComments;
                            }
                            result.Priority = oInTransit.Priority;
                            //Si se encuentra se reinicia la prioridad express
                            result.ExpressPriority = null;
                            updatedcounts += db.SaveChanges();
                        }
                        else
                        {
                            //No encontro el registro, se busca como complementaria/Blowout
                            var complementaria = (from r in db.RouteMasters
                                                  where
                                                  r.TrailerNumber == oInTransit.TrailerNumber &&
                                                  r.UnloadDate == oInTransit.UnloadDate &&
                                                  r.DataSourceId == 7 &&
                                                  r.Comments.Contains("COMPLEMENTARIA")
                                                  select r).FirstOrDefault();
                            if (complementaria != null)
                            {
                                //Ya hay registro de complementaria, actualizamos los datos faltantes
                                complementaria.PullAhead = PullAhead;
                                complementaria.TrailerNumber = oInTransit.TrailerNumber;
                                complementaria.Hazmat = oInTransit.Hazmat;
                                //complementaria.Invoice = oInTransit.F1;
                                complementaria.DataSourceId = 14;
                                complementaria.RegisterDate = DateTime.Now.Date;
                                complementaria.RoutesMilestone = "YARD ARRIVAL";
                                complementaria.PuRoute2 = "";
                                complementaria.PeriodId = period;
                                complementaria.SName = "TMMBC YARD";
                                if (!string.IsNullOrEmpty(oInTransit.StatusComments))
                                {
                                    complementaria.Comments = complementaria.Comments + " | " + oInTransit.StatusComments;
                                }
                                complementaria.Priority = oInTransit.Priority;
                                updatedcounts += db.SaveChanges();
                            }
                            else
                            {
                                //Si no la encuentra en complementarias tampoco, debe ser expedite o Camex, agregamos el nuevo registro                                
                                RouteMaster rmObj = new RouteMaster
                                {
                                    TrailerNumber = oInTransit.TrailerNumber,
                                    Route = oInTransit.Route,
                                    Run = oInTransit.Origin,
                                    RouteRun = routeRun,
                                    //Invoice = oInTransit.F1,
                                    DataSourceId = 12,
                                    PeriodId = period,
                                    UnloadDate = oInTransit.UnloadDate,
                                    Comments = oInTransit.StatusComments,
                                    Hazmat = oInTransit.Hazmat,
                                    PullAhead = PullAhead,
                                    Priority = oInTransit.Priority,
                                    ExpressPriority = null,
                                    RoutesMilestone = "YARD ARRIVAL",
                                    PuRoute2 = "",
                                    SName = "TMMBC YARD",
                                    RegisterDate = DateTime.Now.Date
                                };

                                db.RouteMasters.Add(rmObj);
                                db.SaveChanges();
                            }
                        }
                    }
                    //Anadir el contenedor a lista de procesados por si se encuentra el mismo
                    //contenedor en varios tabs en el excel, deberia mantener la info de 
                    //la primera hoja (Es la principal porque tiene la prioridad)
                    procesados.Add(oInTransit.TrailerNumber);
                }
            }
            return updatedcounts;
        }

        public List<SpotsVM> saveBrokerFile(DataTable BrokerData, string fileName, string user)
        {
            SDSPEntities db = new SDSPEntities();
            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["SQLBulkConnection"].ConnectionString))
            {
                sqlBulk.DestinationTableName = "Broker";
                sqlBulk.WriteToServer(BrokerData);

                File oFile = new File
                {
                    FileName = fileName,
                    UploadDate = DateTime.Now,
                    FileTypeId = 4,
                    AspNetUsersId = user
                };
                
                db.Files.Add(oFile);
                db.SaveChanges();
            }

            List<SpotsVM> lstSpotVM = new List<SpotsVM>();
            int updatedcounts = 0;
            foreach (DataRow row in BrokerData.Rows)
            {
                DateTime? dtUnloadDate = (!string.IsNullOrEmpty(row["UnloadDate"].ToString())) ? DateTime.Parse(row["UnloadDate"].ToString()) : (DateTime?)null;

                Broker oBroker = new Broker
                {
                    Route = row["Route"].ToString(),
                    Sequence = row["Sequence"].ToString(),
                    TrailerNumber = row["TrailerNumber"].ToString(),
                    UnloadDate = dtUnloadDate,
                    Invoice = row["Invoice"].ToString()
                };
                //Revisar si la caja esta en yarda aunque no cuadren los datos
                var ingates = (from y in db.YardStatus
                                where y.TrailerNumber == oBroker.TrailerNumber && y.Bound == 1
                                select y).FirstOrDefault();

                if (!string.IsNullOrEmpty(oBroker.Invoice))
                {
                    if (ingates != null)
                    {
                        SpotsVM oSpotVM = new SpotsVM {
                            trailerNumber = oBroker.TrailerNumber,
                            yardSpotId = ingates.YardSpotId
                        };
                        
                        RouteMaster toUpdate = db.RouteMasters.Find(ingates.RouteMasterId);
                        
                        toUpdate.Invoice = "1";
                        updatedcounts += db.SaveChanges();

                        lstSpotVM.Add(oSpotVM);
                    }
                    else
                    {
                        //Revisamos si la caja esta en RouteMaster
                        var result = (from r in db.RouteMasters
                                      where
                                      r.Route == oBroker.Route &&
                                      r.Run == oBroker.Sequence &&
                                      r.UnloadDate == oBroker.UnloadDate &&
                                      r.TrailerNumber == oBroker.TrailerNumber
                                      orderby r.RegisterDate descending
                                      select r).FirstOrDefault();

                        if (result != null)
                        {
                            //Crear Lista de Ingates para ver el spot
                            var ingate = (from i in db.Ingates
                                          where i.RouteMasterId == result.Id &&
                                          //Revisar que no se haya despachado
                                          !db.Outgates.Select(e => e.RouteMasterId).Contains(result.Id)
                                          select i).FirstOrDefault();

                            //Agregamos la caja a la lista para mostrar en pantalla la caja con el spot que tiene en yarda
                            lstSpotVM.Add(new SpotsVM {
                                trailerNumber = oBroker.TrailerNumber,
                                yardSpotId = (ingate == null) ? 0 : ingate.YardSpotId
                            });
                            
                            //Hacemos el update de invoice al registro del RouteMaster de la caja
                            result.Invoice = "1";
                            db.SaveChanges();

                            //Agregamos un registro a en la tabla InvoiceExpiration para manejar la validez de la factura
                            db.InvoiceExpirations.Add(new InvoiceExpiration
                            {
                                RouteMasterId = result.Id,
                                InputDate = DateTime.Now.Date,
                                ExpirationDate = DateTime.Now.Date.AddDays(2),
                                AspNetUsersId = user
                            });
                            db.SaveChanges();
                        }
                    }
                }
            }
            return lstSpotVM;
        }

        public bool saveRouteMaster(DataTable routeMasterData, string fileName, string user)
        {
            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["SQLBulkConnection"].ConnectionString))
            {
                sqlBulk.DestinationTableName = "RouteMaster";
                sqlBulk.WriteToServer(routeMasterData);
                File oFile = new File
                {
                    FileName = fileName,
                    UploadDate = DateTime.Now,
                    FileTypeId = 1,
                    AspNetUsersId = user
                };
                SDSPEntities db = new SDSPEntities();
                db.Files.Add(oFile);
                db.SaveChanges();
            }
            return true;
        }

        public int saveDTR(DataTable DTRData, string fileName, string user)
        {
            int updatesCount = 0;
            int period = getCurrentPeriod();
            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["SQLBulkConnection"].ConnectionString))
            {
                sqlBulk.DestinationTableName = "DTR";
                sqlBulk.WriteToServer(DTRData);

                File oFile = new File
                {
                    FileName = fileName,
                    UploadDate = DateTime.Now,
                    FileTypeId = 2,
                    AspNetUsersId = user
                };

                SDSPEntities db = new SDSPEntities();
                db.Files.Add(oFile);
                db.SaveChanges();
            }

            foreach (DataRow row in DTRData.Rows)
            {
                DTR oDTR = new DTR
                {
                    TrailerNumber = row["TrailerNumber"].ToString(),
                    Route_Seq = row["Route_Seq"].ToString(),
                    Renban = row["Renban"].ToString(),
                    UnloadDate = DateTime.Parse(row["UnloadDate"].ToString()),
                    SecondUnloadDate = DateTime.Parse(row["SecondUnloadDate"].ToString()),
                    ShipDate = DateTime.Parse(row["ShipDate"].ToString()),
                    ETAbyXPO = DateTime.Parse(row["ETAbyXPO"].ToString()),
                    Hazmat = row["Hazmat"].ToString(),
                    XBorder = DateTime.Parse(row["XBorder"].ToString()),
                    CLoadLinesMROS = row["CLoadLinesMROS"].ToString(),
                };
                oDTR.TrailerNumber = oDTR.TrailerNumber.Trim();
                oDTR.TrailerNumber = oDTR.TrailerNumber.ToUpper();
                var db = new SDSPEntities();
                //Consideramos que si hay numeros en el texto, es que lleva ruta y secuencia y no es BLOWOUT ni OVERWEIGHT
                if (oDTR.Route_Seq.Any(char.IsDigit))
                {
                    string searchRoute = new String(oDTR.Route_Seq.Where(Char.IsLetter).ToArray());
                    string searchSeq = new String(oDTR.Route_Seq.Where(Char.IsDigit).ToArray());
                    var result = db.RouteMasters.FirstOrDefault(
                        b =>
                        b.Route == searchRoute &&
                        b.Run == searchSeq &&
                        b.UnloadDate == oDTR.UnloadDate &&
                        b.SName == "TMMBC YARD" &&
                        (b.RoutesMilestone.Equals("YARD ARRIVAL") || b.RoutesMilestone.Equals("drop")) &&
                        (b.PuRoute2 != "OTR Mexico" && b.PuRoute2 != "air")
                    );
                    if (result != null)
                    {
                        
                        result.TrailerNumber = oDTR.TrailerNumber;
                        result.Hazmat = oDTR.Hazmat;
                        result.Comments = oDTR.CLoadLinesMROS;
                        if (result.DataSourceId == 1)
                        {
                            //Solo deberia actualizar el datasource si es registro de Routemaster,
                            //Si ya paso por in transit u algun otro proceso, no se actualiza ya que puede 
                            //afectar reportes como el Real time in transit
                            result.DataSourceId = 2;
                        }
                        //DTR no tiene que actualizar fecha de registro, solo en nuevos registros
                        //result.RegisterDate = DateTime.Now.Date;
                        updatesCount += db.SaveChanges();
                    }
                }
                else
                {
                    //Si no tiene ruta y secuencia, entonces es blowout/overweight y tenemos que crear un record en RouteMaster
                    //Primero revisamos si no se cargo anteriormente para no repetir record
                    //ya que el dtr se carga diario y trae informacion de varios dias
                    var checkdupes = (from r in db.RouteMasters
                                      where
                                      r.TrailerNumber == oDTR.TrailerNumber &&
                                      r.UnloadDate == oDTR.UnloadDate &&
                                      r.DataSourceId == 7 &&
                                      r.Comments.Contains("COMPLEMENTARIA")
                                      select r).FirstOrDefault();

                    if (checkdupes == null)
                    {
                        //Si no es record repetido sacamos el record de la complementaria
                        var rDTR = (from d in db.DTRs
                                    where
                                    oDTR.CLoadLinesMROS.Contains(d.TrailerNumber) &&
                                    d.Route_Seq != "BLOWOUT" &&
                                    d.UnloadDate == oDTR.UnloadDate
                                    select d).FirstOrDefault();
                        if (rDTR != null)
                        {
                            string route_run = rDTR.Route_Seq;
                            string route = new String(route_run.Where(Char.IsLetter).ToArray());
                            string run = new String(route_run.Where(Char.IsDigit).ToArray());

                            //contiene la ruta-secuencia
                            RouteMaster nrRouteMaster = new RouteMaster
                            {
                                TrailerNumber = oDTR.TrailerNumber,
                                UnloadDate = oDTR.UnloadDate,
                                Route = route,
                                Run = run,
                                PeriodId = period,
                                RouteRun = route + '-' + run,
                                Hazmat = oDTR.Hazmat,
                                Comments = oDTR.CLoadLinesMROS,
                                DataSourceId = 7,
                                RegisterDate = DateTime.Now.Date,
                                RoutesMilestone = "YARD ARRIVAL",
                                PuRoute2 = "",
                                SName = "TMMBC YARD",
                            };
                            db.RouteMasters.Add(nrRouteMaster);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        //Si es duplicado, le agregamos el comentario.
                        checkdupes.Comments = checkdupes.Comments + " | " + oDTR.CLoadLinesMROS;
                        db.SaveChanges();
                    }
                }
            }
            return updatesCount;
        }

        public bool blockspots(int spot, int status, string comment, string user)
        {
            SDSPEntities db = new SDSPEntities();

            var spotId = from y in db.YardSpots where y.SpotNumber == spot select y.Id;

            if (spotId.Count() > 1)
            {
                return false;
            }
            else
            {
                int ysId = int.Parse(spotId.First().ToString());
                db.UpdateSpot(ysId, status, comment, user);
                return true;
            }
        }


        public SpotsVM updateSingleInvoice(string invoice, long routeMasterId, string user)
        {
            var db = new SDSPEntities();
            
            RouteMaster result = db.RouteMasters.Find(routeMasterId);

            var ingates = (from i in db.Ingates
                           where i.RouteMasterId == result.Id
                           select i).FirstOrDefault();

            SpotsVM oSpotVM = new SpotsVM {
                trailerNumber = result.TrailerNumber,
                yardSpotId = (ingates == null) ? 0 : ingates.YardSpotId
            };
            
            result.Invoice = invoice;
            db.SaveChanges();

            if (invoice == "1")
            {
                //Agregamos un registro a en la tabla InvoiceExpiration para manejar la validez de la factura;
                db.InvoiceExpirations.Add(new InvoiceExpiration
                {
                    RouteMasterId = result.Id,
                    InputDate = DateTime.Now.Date,
                    ExpirationDate = DateTime.Now.Date.AddDays(2),
                    AspNetUsersId = user
                });
                db.SaveChanges();
            }
            else {
                //Borramos el registro de la tabla Expiration Date
                var expiration = (from i in db.InvoiceExpirations
                                  where i.RouteMasterId == routeMasterId
                                  select i).FirstOrDefault();
                if (expiration != null) {
                    InvoiceExpiration obj = db.InvoiceExpirations.Find(expiration.Id);
                    db.InvoiceExpirations.Remove(obj);
                    db.SaveChanges();
                }
            }

            return oSpotVM;
        }

        public string getReason(int spot)
        {
            SDSPEntities db = new SDSPEntities();

            int spotId = (from y in db.YardSpots where y.SpotNumber == spot select y.Id).FirstOrDefault();


            long maxId = (from yy in db.YardSpotChangesLogs where yy.YardSpotId == spotId && yy.SpotStatusId == 1 select yy.Id).Max();


            var result = (from y in db.YardSpotChangesLogs
                          where y.Id == maxId
                          select y.Comment).SingleOrDefault();

            return result;
        }

        public List<PrintHookVM> getactiveIngates()
        {
            List<PrintHookVM> lstContenedoresEnYarda = new List<PrintHookVM>();

            SDSPEntities db = new SDSPEntities();

            var ingates = from i in db.Ingates
                          where !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId)
                          select new
                          {
                              ingateObj = i
                          };
            var preOutgates = from p in db.PreOutgates
                              join d in db.Drivers on p.DriverId equals d.Id
                              join c in db.Companies on d.IdCompany equals c.Id
                              where !db.Outgates.Select(e => e.RouteMasterId).Contains(p.RouteMasterId)
                              select new
                              {
                                  preoutgateObj = p,
                                  driverObj = d,
                                  companyObj = c
                              };

            var routeMasters = from r in db.RouteMasters
                               where db.Ingates.Select(e => e.RouteMasterId).Contains(r.Id)
                               select r;

            foreach (var i in ingates)
            {
                PrintHookVM phObj = new PrintHookVM();
                phObj.Ingate = new Ingate
                {
                    Id = i.ingateObj.Id,
                    YardSpotId = i.ingateObj.YardSpotId,
                    RouteMasterId = i.ingateObj.RouteMasterId,
                    DateTime = i.ingateObj.DateTime,
                    TrailerStatusId = i.ingateObj.TrailerStatusId,
                    Bound = i.ingateObj.Bound
                };

                //foreach (var p in preOutgates)
                //{
                //    if (p.preoutgateObj.RouteMasterId == i.ingateObj.RouteMasterId)
                //    {
                //        phObj.PreOutgate = new PreOutgate
                //        {
                //            Id = p.preoutgateObj.Id,
                //            RouteMasterId = p.preoutgateObj.RouteMasterId
                //        };
                //        phObj.Driver = new Driver
                //        {
                //            Id = p.driverObj.Id,
                //            Name = p.driverObj.Name,
                //            IdCompany = p.driverObj.IdCompany
                //        };
                //        phObj.Company = new Company
                //        {
                //            Id = p.companyObj.Id,
                //            Name = p.companyObj.Name
                //        };
                //    }
                //}

                foreach (var r in routeMasters)
                {
                    if (r.Id == i.ingateObj.RouteMasterId)
                    {
                        var yardComments = (from y in db.YardComments
                                            where y.RouteMasterId == r.Id
                                            orderby y.Id descending
                                            select y).FirstOrDefault();

                        var ingatecomment = (from c in db.Comments
                                             where c.IngateId == i.ingateObj.Id
                                             select c).FirstOrDefault();

                        string comment = (yardComments != null) ? yardComments.Comment : ingatecomment.Comment1;

                        phObj.RouteMaster = new RouteMaster
                        {
                            Id = r.Id,
                            Route = r.Route,
                            Run = r.Run,
                            UnloadDate = r.UnloadDate,
                            Hazmat = r.Hazmat,
                            Invoice = r.Invoice,
                            Priority = r.Priority,
                            TrailerNumber = r.TrailerNumber,
                            Comments = comment
                        };
                    }
                }

                lstContenedoresEnYarda.Add(phObj);
            }

            return lstContenedoresEnYarda;
        }

        public bool saveYardComment(string comment, long routeMasterId, string user)
        {
            SDSPEntities db = new SDSPEntities();

            YardComment yardComment = new YardComment
            {
                Comment = comment,
                RouteMasterId = routeMasterId,
                AspNetUsersId = user,
                DateTime = DateTime.Now
            };

            db.YardComments.Add(yardComment);

            return (db.SaveChanges() > 0);
        }

        public List<YardComment> getYardComments(long routeMasterId)
        {
            List<YardComment> commentList = new List<YardComment>();

            SDSPEntities db = new SDSPEntities();
            var results = from c in db.YardComments
                          where c.RouteMasterId == routeMasterId
                          select c;            

            foreach (var item in results)
            {
                var userObj = (from u in db.AspNetUsers
                                where u.Id == item.AspNetUsersId
                                select u).FirstOrDefault();

                commentList.Add(new YardComment
                {
                    AspNetUsersId = userObj.UserName,
                    Comment = item.Comment,
                    DateTime = item.DateTime
                });
            }

            //Traer el ingate comment
            var ingate = (from i in db.Ingates
                          where i.RouteMasterId == routeMasterId
                          select i).FirstOrDefault();

            var originalComment = (from c in db.Comments
                            where c.IngateId == ingate.Id
                            select c).FirstOrDefault();

            var originalUserObj = (from u in db.AspNetUsers
                           where u.Id == originalComment.AspNetUsersId
                           select u).FirstOrDefault();

            commentList.Add(new YardComment
            {
                AspNetUsersId = originalUserObj.UserName,
                Comment = originalComment.Comment1,
                DateTime = originalComment.DateTime
            });

            return commentList;
        }

        public string getDriverAssigned(long? preOutgateId)
        {
            SDSPEntities db = new SDSPEntities();
            var result = (from p in db.PreOutgates
                          join d in db.Drivers on p.DriverId equals d.Id
                          join c in db.Companies on d.IdCompany equals c.Id
                          where p.Id == preOutgateId
                          //select new { driver = d.Name+" - "+ c.Name }).First();
                          select new { driver = d.Name + " - " + c.Name }).First();
            return result.driver;
        }

        public List<RouteMaster> getExpected()
        {
            SDSPEntities db = new SDSPEntities();
            List<RouteMaster> expected = new List<RouteMaster>();
            DateTime today = DateTime.Now.Date;
            int period = getCurrentPeriod();
            var result = from r in db.RouteMasters
                         where
                         r.UnloadDate >= today &&
                         r.PeriodId == period &&
                         r.SName == "TMMBC YARD" &&
                         r.TrailerNumber != null &&
                         r.Priority != null &&
                        (r.RoutesMilestone.Equals("YARD ARRIVAL") || r.RoutesMilestone.Equals("drop")) &&
                        (r.PuRoute2 != "OTR Mexico" && r.PuRoute2 != "air")
                         select r;

            foreach (var item in result)
            {
                RouteMaster rmObj = new RouteMaster
                {
                    Id = item.Id,
                    UnloadDate = item.UnloadDate,
                    Priority = item.Priority,
                    Route = item.Route,
                    Run = item.Run,
                    Comments = item.Comments,
                    TrailerNumber = item.TrailerNumber,
                    Invoice = item.Invoice
                };

                expected.Add(rmObj);
            }
            return expected;
        }

        public List<RouteMaster> AuditTrailers(DateTime start, DateTime end, int type)
        {
            List<RouteMaster> trailersList = new List<RouteMaster>();

            SDSPEntities db = new SDSPEntities();

            if (type == 1)
            {
                //Faltantes
                for (DateTime date = start; date <= end; date = date.AddDays(1))
                {

                    var ingates = from i in db.Ingates
                                  where i.DateTime >= date
                                  select i;

                    var result = (from r in db.RouteMasters
                                  where
                                  (r.RegisterDate == date || r.PullAhead == date) &&
                                  r.SName == "TMMBC YARD" &&
                                  r.TrailerNumber != null &&
                                  r.Priority != null &&
                                  (r.RoutesMilestone.Equals("YARD ARRIVAL") || r.RoutesMilestone.Equals("drop")) &&
                                  (r.PuRoute2 != "OTR Mexico" && r.PuRoute2 != "air") &&
                                  !db.Ingates.Where(e => e.DateTime == date).Select(e => e.RouteMasterId).Contains(r.Id)
                                  select r);

                    foreach (var rm in result)
                    {
                        RouteMaster rmObj = new RouteMaster
                        {
                            Id = rm.Id,
                            Route = rm.Route,
                            Run = rm.Run,
                            UnloadDate = rm.UnloadDate,
                            Hazmat = rm.Hazmat,
                            Priority = rm.Priority,
                            TrailerNumber = rm.TrailerNumber,
                        };

                        trailersList.Add(rmObj);
                    }
                }
            }
            else
            {
                //Sobrantes
                for (DateTime date = start; date <= end; date = date.AddDays(1))
                {
                    var result = (from r in db.RouteMasters
                                  where
                                  r.UnloadDate >= date &&
                                  r.SName == "TMMBC YARD" &&
                                  r.TrailerNumber != null &&
                                  r.Priority != null &&
                                 (r.RoutesMilestone.Equals("YARD ARRIVAL") || r.RoutesMilestone.Equals("drop")) &&
                                 (r.PuRoute2 != "OTR Mexico" && r.PuRoute2 != "air")
                                  select r.Id).ToList();

                    var ingates = from i in db.Ingates
                                  where i.DateTime == date &&
                                  !result.Contains(i.Id)
                                  select i;

                    foreach (var item in ingates)
                    {
                        var rm = (from r in db.RouteMasters where r.Id == item.RouteMasterId select r).First();

                        RouteMaster rmObj = new RouteMaster
                        {
                            Id = rm.Id,
                            Route = rm.Route,
                            Run = rm.Run,
                            UnloadDate = rm.UnloadDate,
                            Hazmat = rm.Hazmat,
                            Priority = rm.Priority,
                            TrailerNumber = rm.TrailerNumber,
                        };

                        trailersList.Add(rmObj);
                    }
                }
            }

            return trailersList;
        }

        public List<IssueStatu> getIssueStatuses()
        {
            List<IssueStatu> statusList = new List<IssueStatu>();

            SDSPEntities db = new SDSPEntities();

            var result = from i in db.IssueStatus select i;

            foreach (var item in result)
            {
                IssueStatu issueStatusObj = new IssueStatu
                {
                    Id = item.Id,
                    Name = item.Name
                };

                statusList.Add(issueStatusObj);
            }

            return statusList;
        }

        public bool saveIssue(string issue, string people, string solution, int status, string user)
        {
            SDSPEntities db = new SDSPEntities();
            db.SaveIssue(issue, people, solution, status, DateTime.Now, user);

            return true;
        }


        public List<Issue> GetIssuesList()
        {
            List<Issue> issuesList = new List<Issue>();
            SDSPEntities db = new SDSPEntities();
            var lstIssues = from k in db.Issues select k;
            foreach (var item in lstIssues)
            {
                Issue issue = new Issue
                {
                    Id = item.Id,
                    Reason = item.Reason,
                    Involved = item.Involved,
                    IssueStatusId = item.IssueStatusId,
                    DateTime = item.DateTime,
                    Solution = item.Solution,                    
                };
                issuesList.Add(issue);
            }
            return issuesList;
        }

        public Issue getIssueRecord(int id)
        {
            SDSPEntities db = new SDSPEntities();
            var record = (from i in db.Issues where i.Id == id select i).First();

            return new Issue
            {
                Id = record.Id,
                Reason = record.Reason,
                Solution = record.Solution,
                Involved = record.Involved,
                IssueStatusId = record.IssueStatusId,
                DateTime = record.DateTime
            };
        }

        public bool updateIssue(string issue, string people, string solution, int status, string user, int issueId)
        {
            SDSPEntities db = new SDSPEntities();

            db.UpdateIssue(issue, people, solution, status, DateTime.Now, user, issueId);

            return true;
        }

        public List<RealTime_InTransit> getRouteMasterDatabyDate()
        {
            List<RouteMaster> rmList = new List<RouteMaster>();

            SDSPEntities db = new SDSPEntities();

            //int period = getCurrentPeriod();
            DateTime today = DateTime.Now.Date; 
            var getExpress = db.PriorityChangesLogs.Select(e => e).Where(e => e.DateTime > today.Date).Count();
            
            //Traer Prioridades del dia
            //Edit: 11/26 Solo consultar prioridades que aun no han sido despachadas (activas)
            //var result = from r in db.RouteMasters                            
            //                where 
            //                //!db.Outgates.Select(e => e.RouteMasterId).Contains(r.Id) &&
            //                (
            //                r.RegisterDate == today &&
            //                //r.PeriodId == period &&
            //                r.TrailerNumber != null &&
            //                r.Priority > 0 &&
            //                (r.RoutesMilestone.Equals("YARD ARRIVAL") || r.RoutesMilestone.Equals("drop")) &&
            //                (r.PuRoute2 != "OTR Mexico" && r.PuRoute2 != "air")
            //                )
            //                orderby r.Priority
            //                select r;

            //Cambie el query para que lo tome de la vista realtime
            var results = from r in db.RealTime_InTransit
                          select r;

            List<RealTime_InTransit> current = new List<RealTime_InTransit>();

            foreach (var item in results)
            {
                current.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName
                });
            }

            

            //foreach (var item in result)
            //{
            //    rmList.Add(
            //        new RouteMaster
            //        {
            //            Id = item.Id,
            //            TrailerNumber = item.TrailerNumber,
            //            Route = item.Route,
            //            Run = item.Run,
            //            RouteRun = item.RouteRun,
            //            Priority = item.Priority,
            //            ExpressPriority = item.ExpressPriority,
            //            Invoice = item.Invoice,
            //            UnloadDate = item.UnloadDate,
            //            PullAhead = item.PullAhead
            //        }
            //    );
            //}

            if (getExpress > 0){
                //Express
                current = current.OrderBy(x => x.ExpressPriority).ToList();
            } else {
                //Priority
                current = current.OrderBy(x => x.Priority).ToList();
            }

            return current;
        }

        public bool saveOrder(IDictionary<string, int> priorities)
        {
            SDSPEntities db = new SDSPEntities();
            int updatesCount = 0;
            foreach (var item in priorities)
            {
                int rmId = int.Parse(item.Key);

                var result = db.RouteMasters.SingleOrDefault(b => b.Id == rmId);
                if (result != null)
                {
                    result.ExpressPriority = item.Value;
                    updatesCount = db.SaveChanges();
                }
            }
            return updatesCount ==  priorities.Count();
        }

        public bool savePriorityComment(int routeMasterId, string comment, string user) {
            SDSPEntities db = new SDSPEntities();

            PriorityChangesLog obj = new PriorityChangesLog
            {
                Comment = comment,
                RouteMasterId = routeMasterId,
                DateTime = DateTime.Now,
                AspNetUsersId = user
            };

            db.PriorityChangesLogs.Add(obj);

            return db.SaveChanges() > 0;
        }

        public bool checkNextPriority(int routeMasterId) {
            SDSPEntities db = new SDSPEntities();

            DateTime today = DateTime.Parse(DateTime.Now.ToShortDateString());

            var checkifExpress = from y in db.PriorityChangesLogs
                                 where y.DateTime == today
                                 select y;

            if (checkifExpress.Count() > 0) {
                var result = from r in db.RouteMasters
                             where 
                             r.UnloadDate == today &&
                             !db.Outgates.Select(e => e.RouteMasterId).Contains(r.Id)
                             select db.RouteMasters.Max(x => x.ExpressPriority);

            } else {

            }
            return false;
        }

        public bool saveSkipReasons(int routeMasterId, string reason, string user)
        {
            SDSPEntities db = new SDSPEntities();

            PrioritySkip obj = new PrioritySkip {
                Reason = reason,
                RouteMasterId = routeMasterId,
                DateTime = DateTime.Now,
                AspNetUsersId = user
            };

            db.PrioritySkips.Add(obj);

            return db.SaveChanges() > 0;
        }

        //public ForecastVM getForecastData(DateTime date) {
        //    ForecastVM obj = new ForecastVM();
        //    DateTime datePlusOne = new DateTime();
        //    DateTime datePlusTwo = new DateTime();

        //    //date = new DateTime(2018, 10, 31, 0, 0, 0);

        //    if (date.DayOfWeek == DayOfWeek.Friday)
        //    {
        //        datePlusOne = date.AddDays(4);
        //        datePlusTwo = date.AddDays(5);
        //    }else if (date.DayOfWeek == DayOfWeek.Thursday)
        //    {
        //        datePlusOne = date.AddDays(3);
        //        datePlusTwo = date.AddDays(4);
        //    }            
        //    else if (date.DayOfWeek == DayOfWeek.Wednesday)
        //    {
        //        datePlusOne = date.AddDays(2);
        //        datePlusTwo = date.AddDays(5);
        //    }
        //    else{
        //        datePlusOne = date.AddDays(2);
        //        datePlusTwo = date.AddDays(3);
        //    }

        //    SDSPEntities db = new SDSPEntities();

        //    int period = getCurrentPeriod();            

        //    var results = from r in db.RealTime_InTransit
        //                  orderby r.Priority
        //                  select r;

        //    List<RealTime_InTransit> currentList = new List<RealTime_InTransit>();
        //    foreach (var item in results)
        //    {
        //        RealTime_InTransit rmObj = new RealTime_InTransit
        //        {
        //            Id = item.Id,
        //            TrailerNumber = item.TrailerNumber,
        //            UnloadDate = item.UnloadDate,
        //            Route = item.Route,
        //            Run = item.Run,
        //            Hazmat = item.Hazmat,
        //            Comments = item.Comments,
        //            Priority = item.Priority,
        //            Invoice = item.Invoice,
        //            ExpressPriority = item.ExpressPriority,
        //            PullAhead = item.PullAhead,
        //            ingatedriver = item.ingatedriver,
        //            ingateTime = item.ingateTime,
        //            ingatespot = item.ingatespot,
        //            predriver = item.predriver,
        //            preTime = item.preTime,
        //            outDriver = item.outDriver,
        //            outTime = item.outTime,
        //            PreIngateBCDate = item.PreIngateBCDate,
        //            PreIngateBCSpot = item.PreIngateBCSpot,
        //            IngateBcDateTime = item.IngateBcDateTime,
        //            IngateBCSpot = item.IngateBCSpot,
        //            DriverCode = item.DriverCode,
        //            DriverName = item.DriverName
        //        };

        //        currentList.Add(rmObj);
        //    }

        //    obj.current = currentList;

        //    var plusone = from b in db.RouteMasters
        //                  where
        //                  b.UnloadDate == datePlusOne  &&
        //                  b.PeriodId == period &&
        //                  b.PullAhead == null &&
        //                  b.SName == "TMMBC YARD" &&
        //                  b.Priority <= 0 &&
        //                  (b.RoutesMilestone.Equals("YARD ARRIVAL") || b.RoutesMilestone.Equals("drop")) &&
        //                  (b.PuRoute2 != "OTR Mexico" && b.PuRoute2 != "air")
        //                  select b;

        //    List<RouteMaster> plusOneList = new List<RouteMaster>();
        //    foreach (var item in plusone)
        //    {
        //        plusOneList.Add(new RouteMaster
        //        {
        //            Id = item.Id,
        //            Route = item.Route,
        //            Run = item.Run,
        //            TrailerNumber = item.TrailerNumber,
        //            UnloadDate = item.UnloadDate
        //        });
        //    }

        //    obj.plusone = plusOneList;

        //    var plustwo = from b in db.RouteMasters
        //                  where
        //                  b.UnloadDate == datePlusTwo &&
        //                  b.PeriodId == period &&
        //                  b.PullAhead == null &&
        //                  b.SName == "TMMBC YARD" &&
        //                  b.Priority <= 0 &&
        //                  (b.RoutesMilestone.Equals("YARD ARRIVAL") || b.RoutesMilestone.Equals("drop")) &&
        //                  (b.PuRoute2 != "OTR Mexico" && b.PuRoute2 != "air")
        //                  select b;

        //    List<RouteMaster> plusTwoList = new List<RouteMaster>();
        //    foreach (var item in plustwo)
        //    {                
        //        plusTwoList.Add(new RouteMaster
        //        {
        //            Id = item.Id,
        //            Route = item.Route,
        //            Run = item.Run,
        //            TrailerNumber = item.TrailerNumber,
        //            UnloadDate = item.UnloadDate
        //        });
        //    }

        //    obj.plustwo = plusTwoList;

        //    return obj;
        //}

        public ForecastVM getForecastData(DateTime date)
        {
            ForecastVM obj = new ForecastVM();

            SDSPEntities db = new SDSPEntities();
            
            var results = from r in db.RealTime_InTransit
                          orderby r.Priority
                          select r;

            List<RealTime_InTransit> currentList = new List<RealTime_InTransit>();
            foreach (var item in results)
            {
                RealTime_InTransit rmObj = new RealTime_InTransit
                {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = item.Comments,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    PullAhead = item.PullAhead,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName
                };

                currentList.Add(rmObj);
            }

            obj.current = currentList;

            var plusone = forecastDplus3();

            List<RealTime_InTransit> plusOneList = new List<RealTime_InTransit>();
            foreach (var item in plusone)
            {
                plusOneList.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    Route = item.Route,
                    Run = item.Run,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    Hazmat = item.Hazmat
            });
            }

            obj.plusone = plusOneList;

            var plustwo = forecastDplus4();

            List<RealTime_InTransit> plusTwoList = new List<RealTime_InTransit>();
            foreach (var item in plustwo)
            {
                plusTwoList.Add(new RealTime_InTransit
                {
                    Id = item.Id,
                    Route = item.Route,
                    Run = item.Run,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    Hazmat = item.Hazmat
                });
            }

            obj.plustwo = plusTwoList;

            return obj;
        }

        public RouteMaster updatePullAhead(int routeMasterId) {
            SDSPEntities db = new SDSPEntities();
            DateTime today = DateTime.Now.Date;

            //No se necesita revisar periodo, con que sea la prioridad mayor del dia
            //int period = getCurrentPeriod();
            int? maxPriority = (from t in db.RouteMasters
                                where
                                //t.PeriodId == period &&
                                (t.RegisterDate == today || t.PullAhead == today) &&
                                t.Priority > 0
                                orderby t.Priority descending
                                select t.Priority).FirstOrDefault();

            int? maxExpressPriority = (from t in db.RouteMasters
                                       where
                                       //t.PeriodId == period &&
                                       (t.RegisterDate == today || t.PullAhead == today) &&
                                       t.ExpressPriority > 0
                                       orderby t.ExpressPriority descending
                                       select t.ExpressPriority).FirstOrDefault();

            RouteMaster toChange = db.RouteMasters.SingleOrDefault(b => b.Id == routeMasterId);
            if (toChange != null)
            {
                toChange.Priority = (maxPriority + 1);
                toChange.ExpressPriority = (maxExpressPriority != null) ? maxExpressPriority + 1 : null;
                toChange.PullAhead = DateTime.Now.Date;
                db.SaveChanges();
            }

            return toChange;
        }
        
        public List<RealTime_InTransit> getinTransitStatus() {
            List<RealTime_InTransit> current = new List<RealTime_InTransit>();
            SDSPEntities db = new SDSPEntities();

            DateTime date = DateTime.Now.Date;

            var results = from r in db.RealTime_InTransit                         
                          orderby r.Priority
                          select r;

            foreach (var item in results)
            {
                string comment = "";
                if (item.ingateTime != null)
                {
                    var yardComments = (from y in db.YardComments
                                        where y.RouteMasterId == item.Id
                                        orderby y.Id descending
                                        select y).FirstOrDefault();

                    var ingate = (from i in db.Ingates
                                 where i.RouteMasterId == item.Id
                                 select i).FirstOrDefault();

                    var ingatecomment = (from c in db.Comments
                                         where c.IngateId == ingate.Id
                                         select c).FirstOrDefault();

                    comment = (yardComments != null) ? yardComments.Comment : ingatecomment.Comment1;
                }

                var dropObj = (from i in db.InTransits
                               where
                               //i.TrailerNumber == item.TrailerNumber &&
                               //i.Route == item.Route &&
                               //i.Origin == item.Run &&
                               //i.UnloadDate == item.UnloadDate
                               i.Priority == item.Priority &&
                               i.SystemUploadDate == item.RegisterDate
                               select i).FirstOrDefault();

                TimeSpan? dropTime = null;

                if (dropObj != null)
                {
                    dropTime = (dropObj.TMMBCArrives == null) ? (TimeSpan?)null : dropObj.TMMBCArrives.Value.TimeOfDay;
                }

                RealTime_InTransit obj = new RealTime_InTransit {
                    Id = item.Id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Run,
                    Hazmat = item.Hazmat,
                    Comments = comment,
                    Priority = item.Priority,
                    Invoice = item.Invoice,
                    ExpressPriority = item.ExpressPriority,
                    ingatedriver = item.ingatedriver,
                    ingateTime = item.ingateTime,
                    ingatespot = item.ingatespot,
                    predriver = item.predriver,
                    preTime = item.preTime,
                    outDriver = item.outDriver,
                    outTime = item.outTime,
                    PreIngateBCDate = item.PreIngateBCDate,
                    PreIngateBCSpot = item.PreIngateBCSpot,
                    IngateBcDateTime = item.IngateBcDateTime,
                    IngateBCSpot = item.IngateBCSpot,
                    DriverCode = item.DriverCode,
                    DriverName = item.DriverName,
                    ArrvTime = dropTime
                };

                current.Add(obj);
            }
            
            //Si hubo reacomodo de prioridades ordenarlo en base a eso.
            var getExpress = db.PriorityChangesLogs.Select(e => e).Where(e => e.DateTime > date).Count();
            if (getExpress > 0)
            {
                current = current.OrderBy(x => x.ExpressPriority).ToList();
            }

            return current;
        }

        public int savePeriod(float periodNumber, DateTime startDate)
        {
            SDSPEntities db = new SDSPEntities();

            var result = (from d in db.Periods where d.PeriodNumber == periodNumber && d.StartDate == startDate select d).FirstOrDefault();

            if (result != null)
            {
                return -1;
            }
            else {
                Period i = new Period
                {
                    PeriodNumber = periodNumber,
                    StartDate = startDate
                };
                db.Periods.Add(i);
                db.SaveChanges(); //Se agrega instruccion para guardar Registro en BD.  Lidia G. 12/21/2018

                return i.Id;
            }
        }

        public int getCurrentPeriod(){
            SDSPEntities db = new SDSPEntities();
            DateTime today = DateTime.Now;
            var result = (from d in db.Periods where d.StartDate <= today orderby d.StartDate descending select d).FirstOrDefault();
            return result.Id;
        }

        public RouteMaster saveSingleInTransit(string trailer, string route, string origin, string unloadDate, string hazmat, string pullahead, string comments){
            SDSPEntities db = new SDSPEntities();         

            trailer = trailer.Trim();
            trailer = trailer.ToUpper();

            //Revisar que ruta y secuencia vengan correctamente escritos
            if (route.ToLower().Contains('-') || route.Any(x => Char.IsWhiteSpace(x)))
            {
                route = new String((route.Where(Char.IsLetter).ToArray()));
                origin = new String((route.Where(Char.IsDigit).ToArray()));
            }

            string routeRun = (origin == "") ? route : route + "-" + origin;

            DateTime today = DateTime.Now.Date;

            DateTime unload = DateTime.Parse(unloadDate);

            //Buscamos si la nueva caja ya estaba en el sistema
            var nuevaCajaInTransit = (from r in db.RealTime_InTransit
                                      where r.TrailerNumber == trailer
                                      select r).FirstOrDefault();

            if (nuevaCajaInTransit == null)
            {
                int period = getCurrentPeriod();
                
                DateTime? pullAhead = (pullahead == "1") ? DateTime.Now.Date : (DateTime?)null;
                
                hazmat = (hazmat == "1") ? "HAZMAT" : "";
               
                //Get Max Priorities //No se necesita revisar periodo, con que sea la prioridad del dia
                int? maxPriority = (from t in db.RouteMasters
                                    where
                                    //t.PeriodId == period &&
                                    (t.RegisterDate == today || t.PullAhead == today) &&
                                    t.Priority > 0
                                    orderby t.Priority descending
                                    select t.Priority).FirstOrDefault();

                int? maxExpressPriority = (from t in db.RouteMasters
                                           where
                                           //t.PeriodId == period &&
                                           (t.RegisterDate == today || t.PullAhead == today) &&
                                           t.ExpressPriority > 0
                                           orderby t.ExpressPriority descending
                                           select t.ExpressPriority).FirstOrDefault();

                int? Priority = (maxPriority != null) ? maxPriority + 1 : null;
                int? ExpressPriority = (maxExpressPriority != null) ? maxExpressPriority + 1 : null;
                //Buscar si esta ya en la yarda de san diego
                var ingate = (from y in db.YardStatus
                               where y.TrailerNumber == trailer && y.Bound == 1
                               select y).FirstOrDefault();

                if (ingate != null)
                {
                    //Encontro el registro, solo falta actualizarle los datos  
                    RouteMaster result = db.RouteMasters.Find(ingate.RouteMasterId);
                    result.PullAhead = (pullahead == "y") ? DateTime.Now.Date : (DateTime?)null;
                    result.TrailerNumber = trailer;

                    //Se agregaron las siguientes lineas para que el registro sobreescriba los datos existentes con los ingresados por LLP
                    result.Route = route;
                    result.Run = origin;
                    result.RouteRun = routeRun;
                    result.UnloadDate = unload;
                    result.Priority = Priority;
                    result.ExpressPriority = ExpressPriority;
                    //Agregamos el periodo para hacer que la caja sea del periodo en curso
                    result.PeriodId = period;
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    result.Hazmat = hazmat;
                    //result.Invoice = f1;                
                    result.DataSourceId = 13;
                    result.RegisterDate = DateTime.Now.Date;
                    if (!string.IsNullOrEmpty(comments))
                    {
                        result.Comments = result.Comments + " | " + comments;
                    }
                    //result.Priority = Priority;
                    //result.ExpressPriority = ExpressPriority;
                    db.SaveChanges();
                    return result;                
                } else {                
                    //Si no la encuentra en el intransit se tiene que agregar un nuevo record                              
                    RouteMaster rmObj = new RouteMaster
                    {
                        TrailerNumber = trailer,
                        Route = route,
                        Run = origin,
                        RouteRun = routeRun,
                        //Invoice = f1,
                        DataSourceId = 5,
                        PeriodId = period,
                        UnloadDate = unload,
                        Comments = comments,
                        Hazmat = hazmat,
                        PullAhead = pullAhead,
                        Priority = Priority,
                        ExpressPriority = ExpressPriority,
                        RoutesMilestone = "YARD ARRIVAL",
                        PuRoute2 = "",
                        SName = "TMMBC YARD",
                        RegisterDate = DateTime.Now.Date
                    };

                    db.RouteMasters.Add(rmObj);
                    db.SaveChanges();
                    return rmObj;                
                }
            }
            else
            {
                //Si la encuentra en el intransit se deberia regresar un record vacio para indicar que no se registro nada
                return new RouteMaster();
            }
        }

        public RouteMaster editInTransit(long routeMasterId, string trailer, string user){
            SDSPEntities db = new SDSPEntities();

            var outgate = (from o in db.Outgates
                          where o.RouteMasterId == routeMasterId
                          select o).FirstOrDefault();
            
            //No se puede editar una caja una ves que ha salido de la yarda de SDSP
            if (outgate == null)
            {
                //Buscar si la caja original esta ya en la yarda de san diego 
                //var ingate = (from i in db.Ingates
                //              join r in db.RouteMasters on i.RouteMasterId equals r.Id
                //              where
                //               !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                //               r.Id == routeMasterId
                //              select i).FirstOrDefault();

                var ingate = (from y in db.YardStatus
                               where y.RouteMasterId == routeMasterId && y.Bound == 1
                               select y).FirstOrDefault();

                trailer = trailer.Trim();
                trailer = trailer.ToUpper();

                //Traer el record original
                RouteMaster original = db.RouteMasters.Find(routeMasterId);
                
                DateTime today = DateTime.Now.Date;

                //Buscar si la 2da caja esta en el sistema
                //var nuevaCajaInTransit = (from rt in db.RouteMasters
                //                          where
                //                          (rt.RegisterDate == today || rt.PullAhead >= today) &&
                //                          rt.Priority > 0 &&
                //                          rt.TrailerNumber == trailer
                //                          select rt).FirstOrDefault();

                var nuevaCajaInTransit = (from rt in db.RealTime_InTransit
                                          where rt.TrailerNumber == trailer
                                          select rt).FirstOrDefault();

                if (ingate != null)
                {
                    //Si hay un ingate activo
                    string invoice = original.Invoice;
                    int? priority = original.Priority;
                    int? express = original.ExpressPriority;

                    if (nuevaCajaInTransit != null)
                    {
                        //Si la nueva caja tambien esta en el intransit se deberia hacer unicamente el cambio de numero de caja entre la nueva y la vieja
                        //RouteMaster nuevaCajaObj = db.RouteMasters.Find(nuevaCajaInTransit.Id);

                        //Revisar si la nueva caja es un ingate activo
                        //var ingateNueva = (from i in db.Ingates
                        //                  where i.RouteMasterId == nuevaCajaInTransit.Id &&
                        //                  !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId)
                        //                  select i).FirstOrDefault();

                        var ingateNueva = (from y in db.YardStatus
                                       where y.RouteMasterId == nuevaCajaInTransit.Id && y.Bound == 1
                                       select y).FirstOrDefault();

                        if (ingateNueva != null) {
                            //Si ambas cajas estan en yarda en SD se debe cambiar el spot de ambas tambien
                            int? spotOriginal = ingate.YardSpotId;
                            int? spotNueva = ingateNueva.YardSpotId;

                            ingate.YardSpotId = spotNueva;
                            db.SaveChanges();

                            ingateNueva.YardSpotId = spotOriginal;
                            db.SaveChanges();
                        }                        

                        string originalTrailer = original.TrailerNumber;
                        nuevaCajaInTransit.TrailerNumber = originalTrailer;
                        db.SaveChanges();

                        //Editamos el numero de caja a la original
                        original.TrailerNumber = trailer;
                        db.SaveChanges();

                        //Despues de editar creamos el log ya que necesitamos el RouteMasterId del registro nuevo

                        //Log de la caja original
                        db.RouteMasterLogs.Add(new RouteMasterLog
                        {
                            AspNetUsersId = user,
                            DateTime = DateTime.Now,
                            OriginalTrailerNumber = original.TrailerNumber,
                            RouteMasterId = routeMasterId,
                            NewRecord = false,
                            NewTrailerNumber = nuevaCajaInTransit.TrailerNumber,                            
                            ChangeTypeId = 6
                        });

                        db.SaveChanges();

                        return original;
                    }
                    else
                    {
                        //Revisar si la nueva caja es un ingate activo
                        //var ingateNueva = (from i in db.Ingates
                        //                   join r in db.RouteMasters on i.RouteMasterId equals r.Id
                        //                   where
                        //                   !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                        //                   (
                        //                     r.TrailerNumber == trailer &&
                        //                     r.SName == "TMMBC YARD" &&
                        //                     i.Bound == 1 &&
                        //                     (r.RoutesMilestone.Equals("YARD ARRIVAL") || r.RoutesMilestone.Equals("drop")) &&
                        //                     (r.PuRoute2 != "OTR Mexico" && r.PuRoute2 != "air")
                        //                   )
                        //                   select i).FirstOrDefault();

                        

                        //Agregamos un nuevo registro con los datos
                        RouteMaster nuevo = new RouteMaster
                        {
                            TrailerNumber = trailer,
                            Route = original.Route,
                            Run = original.Run,
                            RouteRun = original.RouteRun,
                            Invoice = invoice,
                            DataSourceId = 15,
                            PeriodId = original.PeriodId,
                            UnloadDate = original.UnloadDate,
                            Comments = original.Comments,
                            Hazmat = original.Hazmat,
                            PullAhead = original.PullAhead,
                            Priority = priority,
                            ExpressPriority = express,
                            RoutesMilestone = "YARD ARRIVAL",
                            PuRoute2 = "",
                            SName = "TMMBC YARD",
                            RegisterDate = DateTime.Now.Date
                        };

                        //Le quitamos Y y Prioridad 
                        original.Invoice = "";
                        original.Priority = null;
                        original.ExpressPriority = null;
                        db.SaveChanges();

                        db.RouteMasters.Add(nuevo);
                        db.SaveChanges();

                        //Despues de editar creamos el log ya que necesitamos el RouteMasterId del registro nuevo
                        db.RouteMasterLogs.Add(new RouteMasterLog
                        {
                            AspNetUsersId = user,
                            DateTime = DateTime.Now,
                            OriginalTrailerNumber = original.TrailerNumber,
                            RouteMasterId = routeMasterId,
                            NewRecord = true,
                            NewTrailerNumber = trailer,
                            NewRouteMasterId = nuevo.Id,
                            ChangeTypeId = 6
                        });
                        db.SaveChanges();
                        return nuevo;
                    }
                }
                else
                {
                    //Si no hay ingate activo
                    //Revisamos si la nueva caja ya esta en la yarda de SD
                    //var ingateNueva = (from i in db.Ingates
                    //                   join r in db.RouteMasters on i.RouteMasterId equals r.Id
                    //                   where
                    //                   !db.Outgates.Select(e => e.RouteMasterId).Contains(i.RouteMasterId) &&
                    //                   (
                    //                     r.TrailerNumber == trailer &&
                    //                     r.SName == "TMMBC YARD" &&
                    //                     i.Bound == 1 &&
                    //                     (r.RoutesMilestone.Equals("YARD ARRIVAL") || r.RoutesMilestone.Equals("drop")) &&
                    //                     (r.PuRoute2 != "OTR Mexico" && r.PuRoute2 != "air")
                    //                   )
                    //                   select i).FirstOrDefault();

                    var ingateNueva = (from y in db.YardStatus
                                       where y.TrailerNumber == trailer && y.Bound == 1
                                       select y).FirstOrDefault();

                    //Antes de editar creamos el log               
                    db.RouteMasterLogs.Add(new RouteMasterLog
                    {
                        AspNetUsersId = user,
                        DateTime = DateTime.Now,
                        OriginalTrailerNumber = original.TrailerNumber,
                        RouteMasterId = routeMasterId,
                        NewRecord = false,
                        NewTrailerNumber = trailer,
                        ChangeTypeId = 6
                    });
                    db.SaveChanges();

                    //Revisamos si la nueva caja esta en el intransit
                    if (nuevaCajaInTransit != null)
                    {
                        //Si la nueva caja tambien esta en el intransit se deberia hacer unicamente el cambio de numero de caja entre la nueva y la vieja
                        RouteMaster nuevaCajaObj = db.RouteMasters.Find(nuevaCajaInTransit.Id);
                        string originalTrailer = original.TrailerNumber;
                        nuevaCajaObj.TrailerNumber = originalTrailer;
                        db.SaveChanges();
                    }
                    else
                    {

                        if (ingateNueva != null)
                        {
                            //Si la caja nueva ya esta en yarda
                            var yardcomments = from y in db.YardComments
                                               where y.RouteMasterId == ingateNueva.RouteMasterId
                                               select y;

                            //Editamos el routemasterid a los yardcomments que pueda tener el registro en yarda
                            foreach (var item in yardcomments)
                            {
                                YardComment obj = db.YardComments.Find(item.Id);
                                obj.RouteMasterId = routeMasterId;
                                db.SaveChanges();
                            }

                            //Editamos el ingate y le ponemos el routemasterid del registro intransit
                            ingateNueva.RouteMasterId = original.Id;
                            db.SaveChanges();
                        }

                    }

                    //Editamos el numero de caja a la original
                    original.TrailerNumber = trailer;
                    db.SaveChanges();

                    return original;
                }


                
            }
            
            return new RouteMaster();            
        }

        public RouteMaster updateInTransit(long routeMasterId, string trailer, string user)
        {
            SDSPEntities db = new SDSPEntities();

            var outgate = (from o in db.Outgates
                           where o.RouteMasterId == routeMasterId
                           select o).FirstOrDefault();

            //No se puede editar una caja una ves que ha salido de la yarda de SDSP
            if (outgate == null)
            {
                trailer = trailer.Trim();
                trailer = trailer.ToUpper();

                //Buscar si la caja original esta en la yarda de san diego 
                var ingateOriginal = (from i in db.YardStatus
                              where i.RouteMasterId == routeMasterId
                              select i).FirstOrDefault();

                var ingateNueva = (from i in db.YardStatus
                                   where i.TrailerNumber == trailer
                                   select i).FirstOrDefault();                

                //Traer el record original
                RouteMaster original = db.RouteMasters.Find(routeMasterId);

                DateTime today = DateTime.Now.Date;

                //Buscar si la 2da caja esta en el intransit
                var nuevaCajaInTransit = (from rt in db.RealTime_InTransit
                                          where rt.TrailerNumber == trailer
                                          select rt).FirstOrDefault();
                //if (nuevaCajaInTransit != null)
                //{
                //    //Casos 3,4,7,8
                //    //Ambas cajas estan en el intransit, debe usar el modulo UpdatePriorities
                //    return new RouteMaster { Id = -1 };
                //}

                if (ingateOriginal != null) {
                    //La caja original esta en la yarda y la caja nueva no esta en el intransit ni en la yarda
                    if (nuevaCajaInTransit == null && ingateNueva == null)
                    {
                        ///Caso 1
                        return EditingCase1(routeMasterId, trailer, user);
                    }
                    //La caja original ya esta en la yarda pero no en el intransit
                    else if (nuevaCajaInTransit == null && ingateNueva != null)
                    {
                        //Caso 2
                        return EditingCase2and6(routeMasterId, ingateNueva.RouteMasterId, user, 2);
                    } else if (nuevaCajaInTransit != null && ingateNueva != null) {
                        //Caso 3
                        return EditingCase3(ingateOriginal.IngateId, ingateNueva.IngateId, user);
                    }
                    else if (nuevaCajaInTransit != null && ingateNueva == null) {
                        //Caso 4
                        return EditingCase4(ingateOriginal.IngateId, nuevaCajaInTransit.Id, user);
                    }
                }
                else if (ingateOriginal == null) {                                        
                    if (nuevaCajaInTransit == null && ingateNueva == null)
                    {
                        //Caso 5
                        return EditingCase5(routeMasterId, trailer, user);
                    }
                    else if (nuevaCajaInTransit == null && ingateNueva != null)
                    {
                        //Caso 6
                        return EditingCase2and6(routeMasterId, ingateNueva.RouteMasterId, user,6);
                    } else if (nuevaCajaInTransit != null && ingateNueva != null)
                    {
                        //Caso 7
                        return EditingCase7(routeMasterId, ingateNueva.IngateId, user);
                    }
                    else if (nuevaCajaInTransit != null && ingateNueva == null)
                    {
                        //Caso 8
                        return EditingCase8(routeMasterId, nuevaCajaInTransit.Id, user);
                    }
                }
            }

            return new RouteMaster();
        }

        public RouteMaster EditingCase7(long? rmOriginal, long? ingateIdNueva, string user)
        {
            SDSPEntities db = new SDSPEntities();

            Ingate ingateNueva = db.Ingates.Find(ingateIdNueva);

            RouteMaster nueva = db.RouteMasters.Find(ingateNueva.RouteMasterId);
            RouteMaster original = db.RouteMasters.Find(rmOriginal);

            string cajaOriginal = original.TrailerNumber;
            string cajaNueva = nueva.TrailerNumber;
            string invoiceOriginal = original.Invoice;
            string invoiceNueva = nueva.Invoice;
            long? rmIdOriginal = original.Id;
            long? rmIdcajaNueva = nueva.Id;

            //Intercambiar numeros de caja e invoice (Y) a los registros en RouteMaster.
            original.TrailerNumber = cajaNueva;
            original.Invoice = invoiceNueva;
            db.SaveChanges();

            nueva.TrailerNumber = cajaOriginal;
            nueva.Invoice = invoiceOriginal;
            db.SaveChanges();


            //Intercambiar RouteMasterId a los YardComments del caja original
            var yardCommentsNueva = (from y in db.YardComments
                                        where y.RouteMasterId == rmIdcajaNueva
                                        select y.Id).ToList();

            if (yardCommentsNueva != null)
            {
                foreach (var item in yardCommentsNueva)
                {
                    YardComment obj = db.YardComments.Find(item);
                    obj.RouteMasterId = rmOriginal;
                    db.SaveChanges();
                }

            }

            //Cambiar numero de RouteMasterid al ingate.
            ingateNueva.RouteMasterId = rmOriginal;
            db.SaveChanges();

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = false,
                NewTrailerNumber = cajaNueva,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return original;
        }

        public RouteMaster EditingCase4(long? ingateIdOriginal, long? nuevaCajaInTransit, string user) {
            SDSPEntities db = new SDSPEntities();

            Ingate ingateOriginal = db.Ingates.Find(ingateIdOriginal);

            RouteMaster original = db.RouteMasters.Find(ingateOriginal.RouteMasterId);
            RouteMaster nueva = db.RouteMasters.Find(nuevaCajaInTransit);

            string cajaOriginal = original.TrailerNumber;
            string cajaNueva = nueva.TrailerNumber;
            string invoiceOriginal = original.Invoice;
            string invoiceNueva = nueva.Invoice;
            long? rmIdOriginal = original.Id;
            long? rmIdcajaNueva = nueva.Id;

            //Intercambiar numeros de caja e invoice (Y) a los registros en RouteMaster.
            original.TrailerNumber = cajaNueva;
            original.Invoice = invoiceNueva;
            db.SaveChanges();
            
            nueva.TrailerNumber = cajaOriginal;
            nueva.Invoice = invoiceOriginal;
            db.SaveChanges();

            //Intercambiar RouteMasterId a los YardComments del caja original
            var yardCommentsOriginal = (from y in db.YardComments
                                        where y.RouteMasterId == rmIdOriginal
                                        select y.Id).ToList();

            if (yardCommentsOriginal != null)
            {
                foreach (var item in yardCommentsOriginal)
                {
                    YardComment obj = db.YardComments.Find(item);
                    obj.RouteMasterId = rmIdcajaNueva;
                    db.SaveChanges();
                }

            }

            //Cambiar numero de RouteMasterid al ingate.
            ingateOriginal.RouteMasterId = rmIdcajaNueva;
            db.SaveChanges();

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = false,
                NewTrailerNumber = cajaNueva,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return original;
        }


        public RouteMaster EditingCase3(long? ingaIdOriginal, long? ingateIdNueva, string user){
            SDSPEntities db = new SDSPEntities();

            Ingate ingateOriginal = db.Ingates.Find(ingaIdOriginal);
            Ingate ingateNueva = db.Ingates.Find(ingateIdNueva);

            RouteMaster original = db.RouteMasters.Find(ingateOriginal.RouteMasterId);
            RouteMaster nueva = db.RouteMasters.Find(ingateNueva.RouteMasterId);

            string cajaOriginal = original.TrailerNumber;
            string cajaNueva = nueva.TrailerNumber;
            string invoiceOriginal = original.Invoice;
            string invoiceNueva = nueva.Invoice;
            long? rmIdOriginal = original.Id;
            long? rmIdcajaNueva = nueva.Id;

            //Intercambiar numeros de caja e invoice (Y) a los registros en RouteMaster.
            original.TrailerNumber = cajaNueva;
            original.Invoice = invoiceNueva;
            db.SaveChanges();

            nueva.TrailerNumber = cajaOriginal;
            nueva.Invoice = invoiceOriginal;
            db.SaveChanges();

            //Intercambiar RouteMasterId a los YardComments de ambas cajas
            var yardCommentsOriginal = (from y in db.YardComments
                                       where y.RouteMasterId == rmIdOriginal
                                       select y.Id).ToList();

            var yardCommentsNueva = (from y in db.YardComments
                                     where y.RouteMasterId == rmIdcajaNueva
                                     select y.Id).ToList();

            if (yardCommentsOriginal != null)
            {
                foreach (var item in yardCommentsOriginal)
                {
                    YardComment obj = db.YardComments.Find(item);
                    obj.RouteMasterId = rmIdcajaNueva;
                    db.SaveChanges();
                }
                
            }

            if (yardCommentsNueva != null)
            {
                foreach (var item in yardCommentsNueva)
                {
                    YardComment obj = db.YardComments.Find(item);
                    obj.RouteMasterId = rmIdOriginal;
                    db.SaveChanges();
                }
                db.SaveChanges();
            }

            //Intercambiar numeros de RouteMasterid  a los ingates.
            ingateOriginal.RouteMasterId = rmIdcajaNueva;
            db.SaveChanges();

            ingateNueva.RouteMasterId = rmIdOriginal;
            db.SaveChanges();           

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = false,
                NewTrailerNumber = cajaNueva,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return original;
        }

        public RouteMaster EditingCase8(long routeMasterId, long? nuevaRouteMasterId, string user){
            SDSPEntities db = new SDSPEntities();

            RouteMaster original = db.RouteMasters.Find(routeMasterId);
            RouteMaster nueva = db.RouteMasters.Find(nuevaRouteMasterId);

            string cajaOriginal = original.TrailerNumber;
            string cajaNueva = nueva.TrailerNumber;
            string invoiceOriginal = original.Invoice;
            string invoiceNueva = nueva.Invoice;
            long? rmIdOriginal = original.Id;
            long? rmIdcajaNueva = nueva.Id;

            //Intercambiar numeros de caja e invoice (Y) a los registros en RouteMaster.
            original.TrailerNumber = cajaNueva;
            original.Invoice = invoiceNueva;
            db.SaveChanges();

            nueva.TrailerNumber = cajaOriginal;
            nueva.Invoice = invoiceOriginal;
            db.SaveChanges();

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = false,
                NewTrailerNumber = cajaNueva,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return original;
        }

        public bool wasFileUploaded(int type) {
            SDSPEntities db = new SDSPEntities();

            DateTime today = DateTime.Now.Date;

            var uploaded = (from f in db.Files where
                            f.UploadDate >= today &&
                            f.FileTypeId == type
                            select f).FirstOrDefault();

            return (uploaded != null);
        }

        public RouteMaster EditingCase1(long routeMasterId, string trailer, string user) {
            SDSPEntities db = new SDSPEntities();

            RouteMaster original = db.RouteMasters.Find(routeMasterId);

            int? priority = original.Priority;
            int? express = original.ExpressPriority;

            //A la caja original le quitamos invoice y Prioridades
            original.Priority = null;
            original.ExpressPriority = null;
            original.Invoice = null;
            db.SaveChanges();

            RouteMaster newObj = original;
            newObj.TrailerNumber = trailer;
            newObj.Priority = priority;
            newObj.ExpressPriority = express;
            newObj.DataSourceId = 15;

            db.RouteMasters.Add(newObj);
            db.SaveChanges();

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = true,
                NewTrailerNumber = trailer,
                NewRouteMasterId = newObj.Id,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return newObj;
        }

        public RouteMaster EditingCase2and6(long routeMasterId, long? nuevaRouteMasterId, string user, int type)
        {
            SDSPEntities db = new SDSPEntities();

            RouteMaster original = db.RouteMasters.Find(routeMasterId);
            int? priority = original.Priority;
            int? express = original.ExpressPriority;
            string route = original.Route;
            string run = original.Run;
            string routerun = original.RouteRun;
            string invoice = original.Invoice;
            DateTime? unload = original.UnloadDate;
            DateTime? pullAhead = original.PullAhead;

            //A la caja original le quitamos invoice y Prioridades
            original.Priority = null;
            original.ExpressPriority = null;
            original.Invoice = null;
            db.SaveChanges();

            int datasource = (type == 2) ? 16 : 18;
            
            //La nueva caja toma los datos que le quitamos a la caja original
            RouteMaster nueva = db.RouteMasters.Find(nuevaRouteMasterId);
            nueva.Priority = priority;
            nueva.ExpressPriority = express;
            nueva.RegisterDate = DateTime.Now.Date;
            nueva.DataSourceId = datasource;
            nueva.Route = route;
            nueva.Run = run;
            nueva.RouteRun = routerun;
            nueva.UnloadDate = unload;
            nueva.PullAhead = pullAhead;
            nueva.Invoice = invoice;
            db.SaveChanges();

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = true,
                NewTrailerNumber = nueva.TrailerNumber,
                NewRouteMasterId = nueva.Id,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return nueva;
        }

        public RouteMaster EditingCase5(long routeMasterId, string trailer, string user)
        {
            SDSPEntities db = new SDSPEntities();

            RouteMaster original = db.RouteMasters.Find(routeMasterId);

            original.TrailerNumber = trailer;
            db.SaveChanges();

            //creamos el log               
            db.RouteMasterLogs.Add(new RouteMasterLog
            {
                AspNetUsersId = user,
                DateTime = DateTime.Now,
                OriginalTrailerNumber = original.TrailerNumber,
                RouteMasterId = original.Id,
                NewRecord = false,
                NewTrailerNumber = trailer,
                ChangeTypeId = 6
            });
            db.SaveChanges();

            return original;
        }


        public RouteMaster simpleEditIntransit(long routeMasterId, string trailer, string route, string run, string user)
        {
            //Medodo para Editar datos de un registro del Intransit, no se revisa nada, simplemente se edita
            SDSPEntities db = new SDSPEntities();

            var outgate = (from o in db.Outgates
                           where o.RouteMasterId == routeMasterId
                           select o).FirstOrDefault();

            trailer = trailer.Trim();
            trailer = trailer.ToUpper();

            route.Trim();
            run.Trim();

            //Revisar que ruta y secuencia vengan correctamente escritos
            if (route.ToLower().Contains('-') || route.Any(x => Char.IsWhiteSpace(x)))
            {
                route = new String((route.Where(Char.IsLetter).ToArray()));
                run = new String((route.Where(Char.IsDigit).ToArray()));
            }

            string routeRun = (run == "") ? route : route + "-" + run;

            //No se puede editar una caja una ves que ha salido de la yarda de SDSP
            if (outgate == null)
            {
                DateTime today = DateTime.Now.Date;
                //Buscar si la 2da caja esta en el sistema
                var nuevaCajaInTransit = (from rt in db.RealTime_InTransit
                                          where rt.TrailerNumber == trailer
                                          select rt).FirstOrDefault();

                if (nuevaCajaInTransit == null)
                {
                    //Traer el record original
                    RouteMaster original = db.RouteMasters.Find(routeMasterId);

                    //Antes de editar creamos el log               
                    db.RouteMasterLogs.Add(new RouteMasterLog
                    {
                        AspNetUsersId = user,
                        DateTime = DateTime.Now,
                        OriginalTrailerNumber = original.TrailerNumber,
                        RouteMasterId = routeMasterId,
                        NewRecord = false,
                        NewTrailerNumber = trailer,
                        ChangeTypeId = 5
                    });
                    db.SaveChanges();

                    //Editamos el numero de caja
                    original.TrailerNumber = trailer;
                    original.Route = route.ToUpper();
                    original.Run = run.ToUpper();
                    original.RouteRun = routeRun.ToUpper();
                    db.SaveChanges();
                    return original;
                }
                else {
                    //regresamos -1 para manejar el error en addForecast
                    return new RouteMaster {
                        Id = -1
                    };
                }                
            }

            return new RouteMaster();            
        }

        public List<IngatesReport> IngatesReport(DateTime start, DateTime end) {
            SDSPEntities db = new SDSPEntities();
            List<IngatesReport> iReport = new List<IngatesReport>();

            var result = from ir in db.IngatesReports
                         where ir.IngateDateTime >= start && ir.IngateDateTime <= end
                         orderby ir.IngateDateTime
                         select ir;

            foreach (var item in result) {
                IngatesReport iObj = new IngatesReport {
                    YardSpotId = item.YardSpotId,
                    Id = item.Id,
                    IngateRouteMasterId = item.IngateRouteMasterId,
                    IngateDriverId = item.IngateDriverId,
                    IngateDateTime = item.IngateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    TrailerStatusName = item.TrailerStatusName,
                    Bound = item.Bound
                };

                iReport.Add(iObj);
            }

            return iReport;
        }

        public List<OutgatesReport> OutgatesReport(DateTime start, DateTime end)
        {
            SDSPEntities db = new SDSPEntities();
            List<OutgatesReport> oReport = new List<OutgatesReport>();

            var result = from ir in db.OutgatesReports
                         
                         where ir.OutgateDateTime >= start && ir.OutgateDateTime <= end
                         orderby ir.OutgateDateTime
                         select ir;

            foreach (var item in result)
            {
                OutgatesReport oObj = new OutgatesReport
                {
                    Id = item.Id,
                    OutgateRouteMasterId = item.OutgateRouteMasterId,
                    OutgateDriverId = item.OutgateDriverId,
                    OutgateDateTime = item.OutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound
                };

                oReport.Add(oObj);
            }
            return oReport;
        }

        public List<TrailerStatu> getTrailerStatus()
        {
            List<TrailerStatu> statusList = new List<TrailerStatu>();

            SDSPEntities db = new SDSPEntities();

            var result = from i in db.TrailerStatus select i;

            foreach (var item in result)
            {
                TrailerStatu TrailerStatusObj = new TrailerStatu
                {
                    Id = item.Id,
                    Status = item.Status
                };

                statusList.Add(TrailerStatusObj);
            }

            return statusList;
        }

        public int deleteIngate(long? id, string user) {
            SDSPEntities db = new SDSPEntities();
            int response = 0;

            Ingate ingate = db.Ingates.Find(id);
            long? rMId = ingate.RouteMasterId;

            //Revisamos si la caja ya salio de SDSP
            var outgate = (from o in db.Outgates
                          where o.RouteMasterId == rMId
                          select o).FirstOrDefault();

            if (outgate == null) {
                //Si la caja aun no sale de SDSP procedemos a borrar el ingate

                var commentId = (from c in db.Comments where c.IngateId == id select c).FirstOrDefault();

                if (commentId != null)
                {
                    long? cId = commentId.Id;
                    Comment comment = db.Comments.Find(cId);
                    //Se borra el comentario del ingate
                    db.Comments.Remove(comment);
                    db.SaveChanges();
                }
                
                int? yardspot = ingate.YardSpotId;
                
                //Se borra el ingate
                db.Ingates.Remove(ingate);
                response = (db.SaveChanges() > 0) ? 1 : 0;

                YardSpot spotObj = db.YardSpots.Find(yardspot);

                //revisamos si en yarda hay otra caja en el mismo spot, si hay otra caja, el spot no se puede liberar
                var hasTrailer = (from y in db.YardStatus
                                  where y.Id == yardspot && y.IngateId != null
                                  select y).FirstOrDefault();

                //Si hay otro ingate activo para ese spot no se libera el spot
                if (hasTrailer == null)
                {
                    //Se libera el spot, solo si no hay otro ingate activo en el
                    spotObj.SpotStatusId = 2;
                    db.SaveChanges();
                }

                //Revisamos si hay una asignacion para la caja.
                var hook = (from p in db.PreOutgates 
                            where p.RouteMasterId == rMId
                            select p).FirstOrDefault();

                if (hook != null) {
                    //Si hay una asignacion para la caja, la tenemos que borrar tambien
                        response = (deleteHook(hook.Id, user) == 1) ? 2 : 0;
                }

                //Despues de borrar creamos el log
                IngateLog logObj = new IngateLog {
                    IngateId = ingate.Id,
                    OriginalAspNetUsersId = ingate.AspNetUsersId,
                    OriginalDateTime = ingate.DateTime,
                    RouteMasterId = ingate.RouteMasterId,
                    DriverId = ingate.DriverId,
                    TrailerStatusId = ingate.TrailerStatusId,
                    Bound = ingate.Bound,
                    ChangeTypeId = 2,
                    DateTime = DateTime.Now,
                    AspNetUsersId = user,
                    YardSpotId = ingate.YardSpotId
                };

                db.IngateLogs.Add(logObj);
                db.SaveChanges();
            }else
            {
                response = -1;
            }

            return response;
        }

        public List<PreoutgateReport> PreoutgatesReport()
        {
            SDSPEntities db = new SDSPEntities();
            List<PreoutgateReport> pReport = new List<PreoutgateReport>();

            var result = from ir in db.PreoutgateReports
                             //where ir.IngateDateTime >= start && ir.IngateDateTime <= end
                         orderby ir.PreoutgateDateTime
                         select ir;

            foreach (var item in result)
            {
                PreoutgateReport iObj = new PreoutgateReport
                {
                    YardSpotId = item.YardSpotId,
                    PreoutgateId = item.PreoutgateId,
                    PreoutgateRouteMasterId = item.PreoutgateRouteMasterId,
                    PreoutgateDriverId = item.PreoutgateDriverId,
                    PreoutgateDateTime = item.PreoutgateDateTime,
                    CompanyName = item.CompanyName,
                    DriverName = item.DriverName,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    Run = item.Run,
                    UnloadDate = item.UnloadDate,
                    Bound = item.Bound,
                    TrailerStatusName = item.TrailerStatusName
                };
                pReport.Add(iObj);
            }

            return pReport;
        }

        public int deleteHook(long? id, string user)
        {
            SDSPEntities db = new SDSPEntities();

            PreOutgate hook = db.PreOutgates.Find(id);

            var outgate = (from o in db.Outgates
                           where o.RouteMasterId == hook.RouteMasterId
                           select o).FirstOrDefault();

            if (outgate == null)
            {
                //Antes de borrar salvamos log
                PreOutgateLog logObj = new PreOutgateLog
                {
                    PreOutgateId = hook.Id,
                    OriginalAspNetUsersId = hook.AspNetUsersId,
                    OriginalDateTime = hook.DateTime,
                    ChangeTypeId = 2,
                    DriverId = hook.DriverId,
                    RouteMasterId = hook.RouteMasterId,
                    DateTime = DateTime.Now,
                    AspNetUsersId = user
                };

                db.PreOutgateLogs.Add(logObj);

                int response = 0;

                if (db.SaveChanges() > 0)
                {
                    db.PreOutgates.Remove(hook);
                    response = (db.SaveChanges() > 0) ? 1 : 0;
                }

                return response;
            }
            else {
                return -1;
            }
        }

        public bool deleteDispatch(long? id, string user)
        {
            bool response = false;
            SDSPEntities db = new SDSPEntities();

            Outgate outgate = db.Outgates.Find(id);

            //Se deberia volver a poner el spot en uso?
            var ingate = (from i in db.Ingates
                          where i.RouteMasterId == outgate.RouteMasterId
                          select i).FirstOrDefault();

            YardSpot oldSpot = db.YardSpots.Find(ingate.YardSpotId);
            if (oldSpot.SpotStatusId == 2)
            {
                //Si el spot sigue vacio, solamente se pone en uso de nuevo antes de borrar el outgate
                oldSpot.SpotStatusId = 3;
                db.SaveChanges();
            }
            else {
                //Si el spot ya no esta disponible hay que regresar false para que el usuario pueda escoger un spot libre de la yarda
                return response;
            }

            //El spot estaba libre, entonces podemos borrar el outgate

            //Antes de borrar creamos un log
            OutgateLog logObj = new OutgateLog
            {
                OutgateId = outgate.Id,
                OriginalAspNetUsersId = outgate.AspNetUsersId,
                OriginalDateTime = outgate.DateTime,
                ChangeTypeId = 2,
                DriverId = outgate.DriverId,
                RouteMasterId = outgate.RouteMasterId,
                DateTime = DateTime.Now,
                AspNetUsersId = user,
            };

            db.OutgateLogs.Add(logObj);

            if (db.SaveChanges() > 0)
            {
                //Aqui se borra el outgate
                db.Outgates.Remove(outgate);
                response = (db.SaveChanges() > 0) ? true : false;
            }            
            
            return response;
        }

        public List<UploadedFile> getFiles(DateTime startDate, DateTime endDate) {
            List<UploadedFile> fileList = new List<UploadedFile>();
            SDSPEntities db = new SDSPEntities();

            var files = from f in db.UploadedFiles
                        where
                        f.UploadDate >= startDate &&
                         f.UploadDate <= endDate
                        select f;

            foreach (var item in files) {
                UploadedFile file = new UploadedFile {
                    UploadDate = item.UploadDate,
                    Name = item.Name,
                    FileName = item.FileName,
                    Id = item.Id 
                };
                fileList.Add(file);
            }

            endDate = endDate.AddDays(1);

            return fileList;
        }

        public bool deactivateCompany(long id, string user)
        {
            bool response = false;
            SDSPEntities db = new SDSPEntities();

            Company company = db.Companies.Find(id);

            //Antes de desactivar creamos un log
            CompaniesLog logObj = new CompaniesLog
            {
                Name = company.Name,
                CompanyId = (int)company.Id,
                ChangeTypeId = 4,
                DateTime = DateTime.Now,
                AspNetUsersId = user,
            };

            db.CompaniesLogs.Add(logObj);

            if (db.SaveChanges() > 0)
            {
                company.Active = false;
                response = (db.SaveChanges() > 0) ? true : false;
            }

            return response;
        }

        public bool deactivateDriver(long id, string user)
        {
            bool response = false;
            SDSPEntities db = new SDSPEntities();

            Driver driver = db.Drivers.Find(id);

            //Antes de desactivar creamos un log
            DriversLog logObj = new DriversLog
            {
                Name = driver.Name,
                CompanyId = (int)driver.IdCompany,
                DriverId = (int)driver.Id,
                DriverCode = driver.DriverCode,
                DateTime = DateTime.Now,
                ChangeTypeId = 4,                               
                AspNetUsersId = user,
            };

            db.DriversLogs.Add(logObj);

            if (db.SaveChanges() > 0)
            {
                driver.Active = false;
                response = (db.SaveChanges() > 0) ? true : false;
            }

            return response;
        }

        public Driver getDriverInfo(long id) {
            SDSPEntities db = new SDSPEntities();
            var result = db.Drivers.Find(id);

            Driver driver = new Driver {
                Name = result.Name,
                DriverCode = result.DriverCode,
                IdCompany = result.IdCompany,
                Id = result.Id                                
            };

            return driver;
        }

        public Company getCompanyInfo(long id) {
            SDSPEntities db = new SDSPEntities();
            var result = db.Companies.Find(id);

            Company company = new Company{
                Id = result.Id,
                Name = result.Name,                
            };

            return company;
        }

        public bool editDriver(string name, string code, long id, int company, string user)
        {
            bool response = false;
            SDSPEntities db = new SDSPEntities();

            Driver driver = db.Drivers.Find(id);

            string driverCode = (code == "") ? null : code;

            //Antes de desactivar creamos un log
            DriversLog logObj = new DriversLog
            {
                Name = driver.Name,
                CompanyId = (int)driver.IdCompany,
                DriverId = (int)driver.Id,
                DriverCode = driver.DriverCode,
                DateTime = DateTime.Now,
                ChangeTypeId = 1,
                AspNetUsersId = user,
            };

            db.DriversLogs.Add(logObj);

            if (db.SaveChanges() > 0)
            {
                driver.Name = name;
                driver.DriverCode = driverCode;
                driver.IdCompany = company;

                response = (db.SaveChanges() > 0) ? true : false;
            }

            return response;
        }


        public bool editCompany(string name, long id,string user)
        {
            bool response = false;
            SDSPEntities db = new SDSPEntities();

            Company company = db.Companies.Find(id);

            //Antes de desactivar creamos un log
            CompaniesLog logObj = new CompaniesLog
            {
                CompanyId = (int)id,
                Name = company.Name,
                DateTime = DateTime.Now,
                ChangeTypeId = 1,
                AspNetUsersId = user,
            };

            db.CompaniesLogs.Add(logObj);

            if (db.SaveChanges() > 0)
            {
                company.Name = name;

                response = (db.SaveChanges() > 0) ? true : false;
            }

            return response;
        }

        //public int loadInventory(DataTable inventoryData, string fileName, string user)
        //{
        //    File oFile = new File
        //    {
        //        FileName = fileName,
        //        UploadDate = DateTime.Now,
        //        FileTypeId = 5,
        //        AspNetUsersId = user
        //    };

        //    SDSPEntities db = new SDSPEntities();
        //    db.Files.Add(oFile);
        //    db.SaveChanges();           

        //    int updatedcounts = 0;
        //    foreach (DataRow row in inventoryData.Rows)
        //    {
        //        int spot = 0;
        //        int.TryParse(row["Spot"].ToString(), out spot);
        //        //int priority = 0;
        //        //int.TryParse(row["Priority"].ToString(), out priority);
        //        int bound = (row["Bound"].ToString() == "SB") ? 1 : 2;
        //        int status = 0;
        //        if (row["Status"].ToString() == "L") { status = 1; }
        //        if (row["Status"].ToString() == "E") { status = 2; }
        //        if (row["Status"].ToString() == "R") { status = 3; }
        //        DateTime ATA = DateTime.Parse(row["ATA"].ToString());
        //        string searchRoute = new String((row["Route"].ToString().Where(Char.IsLetter).ToArray()));
        //        string searchSeq = new String((row["Route"].ToString().Where(Char.IsDigit).ToArray()));
        //        string TrailerNumber = row["TrailerNumber"].ToString();
        //        if (bound == 2)
        //        {
        //            long? id = saveManualIngate("277f8edd-f696-419f-b5b9-8a2581ba1ee2", spot, 987, TrailerNumber, searchRoute, searchSeq, "", "From Inventory", bound, status);
        //            SaveIngateComment("From inventory", id, "277f8edd-f696-419f-b5b9-8a2581ba1ee2");
        //        }
        //        else
        //        {
        //            var result = db.RouteMasters.FirstOrDefault(
        //                b =>
        //                b.Route == searchRoute &&
        //                b.TrailerNumber == TrailerNumber &&
        //                b.Priority > 0 &&
        //                b.SName == "TMMBC YARD" &&
        //                (b.RoutesMilestone.Equals("YARD ARRIVAL") || b.RoutesMilestone.Equals("drop")) &&
        //                (b.PuRoute2 != "OTR Mexico" && b.PuRoute2 != "air") &&
        //                //Seleccionando la fecha de descarga maxima para el TrailerNumber
        //                b.UnloadDate == (from r in db.RouteMasters
        //                                 where r.TrailerNumber == TrailerNumber &&
        //                                 b.SName == "TMMBC YARD" &&
        //                                (b.RoutesMilestone.Equals("YARD ARRIVAL") || b.RoutesMilestone.Equals("drop")) &&
        //                                (b.PuRoute2 != "OTR Mexico" && b.PuRoute2 != "air")
        //                                 orderby r.UnloadDate descending
        //                                 select r.UnloadDate).FirstOrDefault()
        //            );
        //            if (result != null)
        //            {
        //                //Si la encuentra deberia ingresar un ingate con esa caja
        //                //El ingate se esta haciendo con el usuario Admin y el Driver: Carlos Montoya
        //                long? id = SaveIngate(result.Id, "277f8edd-f696-419f-b5b9-8a2581ba1ee2", spot, 987, bound, status);
        //                SaveIngateComment("From inventory", id, "277f8edd-f696-419f-b5b9-8a2581ba1ee2");
        //                //updatedcounts += db.SaveChanges();
        //            }
        //            else
        //            {
        //                //Si no lo encuentra deberia ingresar un manual ingate
        //                long? id = saveManualIngate("277f8edd-f696-419f-b5b9-8a2581ba1ee2", spot, 987, TrailerNumber, searchRoute, searchSeq, "", "From Inventory", bound, status);
        //                SaveIngateComment("From inventory", id, "277f8edd-f696-419f-b5b9-8a2581ba1ee2");
        //            }
        //        }
        //    }
        //    return updatedcounts;
        //}

        public List<TrailerHistory> getTrailerHistory(string trailerNumber, DateTime start, DateTime end) {
            List<TrailerHistory> lTrailerHistory = new List<TrailerHistory>();

            SDSPEntities db = new SDSPEntities();
            //Obtenemos los Ids para el trailer
            List<long> ids = (from r in db.RouteMasters
                              where r.TrailerNumber.Equals(trailerNumber)
                              select r.Id).ToList();
            //Get ingates
            var ingates = from i in db.Ingates
                          //join t in db.TrailerStatus on i.TrailerStatusId equals t.Id
                          join r in db.RouteMasters on i.RouteMasterId equals r.Id
                          join d in db.Drivers on i.DriverId equals d.Id
                          join c in db.Companies on d.IdCompany equals c.Id    
                          join t in db.TrailerStatus on i.TrailerStatusId equals t.Id                      
                          where ids.Contains((long)i.RouteMasterId) &&
                          (i.DateTime >= start && i.DateTime <=end)
                          select new { Ingate = i, RouteMaster = r, Driver = d, Company = c, TrailerStatus = t};
            //Get hooks
            var preoutgates = from i in db.PreOutgates
                          join r in db.RouteMasters on i.RouteMasterId equals r.Id
                          join d in db.Drivers on i.DriverId equals d.Id
                          join c in db.Companies on d.IdCompany equals c.Id
                          where ids.Contains((long)i.RouteMasterId) &&
                          (i.DateTime >= start && i.DateTime <= end)
                          select new { Hook = i, RouteMaster = r, Driver = d, Company = c };
            //Get Outgates
            var outgates = from o in db.Outgates
                           join r in db.RouteMasters on o.RouteMasterId equals r.Id
                           join d in db.Drivers on o.DriverId equals d.Id
                           join c in db.Companies on d.IdCompany equals c.Id
                           where ids.Contains((long)o.RouteMasterId) &&
                           (o.DateTime >= start && o.DateTime <= end)
                           select new { Outgate = o, RouteMaster = r, Driver = d, Company = c };

            foreach (var item in ingates) {
                lTrailerHistory.Add(new TrailerHistory {
                    RouteMasterId = item.RouteMaster.Id,
                    DateTime = (DateTime)item.Ingate.DateTime,
                    Route = item.RouteMaster.Route,
                    Driver = item.Driver.Name + " - " + item.Company.Name,
                    Spot = (int)item.Ingate.YardSpotId,
                    Type = 1,
                    Hazmat = item.RouteMaster.Hazmat,
                    Bound = item.Ingate.Bound,
                    TrailerStatus = item.TrailerStatus.Status
                });
            }

            foreach (var item in preoutgates)
            {
                lTrailerHistory.Add(new TrailerHistory
                {
                    RouteMasterId = item.RouteMaster.Id,
                    DateTime = (DateTime)item.Hook.DateTime,
                    Route = item.RouteMaster.Route,
                    Driver = item.Driver.Name + " - " + item.Company.Name,
                    Spot = null,
                    Type = 2
                });
            }

            foreach (var item in outgates)
            {
                lTrailerHistory.Add(new TrailerHistory
                {
                    RouteMasterId = item.RouteMaster.Id,
                    DateTime = (DateTime)item.Outgate.DateTime,
                    Route = item.RouteMaster.Route,
                    Driver = item.Driver.Name + " - " + item.Company.Name,
                    Spot = null,
                    Type = 3
                });
            }

            lTrailerHistory = lTrailerHistory.OrderBy(x => x.DateTime).ToList();

            return lTrailerHistory;
        }

        public bool changeSpot(long? routemasterid, int newSpot, string user) {
            SDSPEntities db = new SDSPEntities();

            //Obtener el spot actual
            var ingate = (from i in db.Ingates
                         where i.RouteMasterId == routemasterid
                         select i).FirstOrDefault();

            if (ingate != null) {
                YardSpot oldSpot = db.YardSpots.Find(ingate.YardSpotId);
                //Poner como ocupado el spot nuevo, pero solo si esta libre (con status = 2)
                YardSpot newspot = db.YardSpots.Find(newSpot);
                if (newspot.SpotStatusId == 2) {
                    //Liberar spot actual
                    oldSpot.SpotStatusId = 2;
                    db.SaveChanges();

                    //Ocupar spot nuevo
                    newspot.SpotStatusId = 3;
                    db.SaveChanges();

                    //Cambiar el spot al ingate

                    //Antes de cambiar el spot al ingate creamos un log
                    IngateLog logObj = new IngateLog
                    {
                        IngateId = ingate.Id,
                        OriginalAspNetUsersId = ingate.AspNetUsersId,
                        OriginalDateTime = ingate.DateTime,
                        RouteMasterId = ingate.RouteMasterId,
                        DriverId = ingate.DriverId,
                        TrailerStatusId = ingate.TrailerStatusId,
                        Bound = ingate.Bound,
                        ChangeTypeId = 1,
                        DateTime = DateTime.Now,
                        AspNetUsersId = user,
                        YardSpotId = ingate.YardSpotId
                    };

                    db.IngateLogs.Add(logObj);
                    db.SaveChanges();

                    ingate.YardSpotId = newspot.Id;
                    db.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public bool changeDispatchSpot(long? routemasterid, int newSpot, string user)
        {
            SDSPEntities db = new SDSPEntities();
            bool response = false;
            //Obtener el spot actual
            var ingate = (from i in db.Ingates
                          where i.RouteMasterId == routemasterid
                          select i).FirstOrDefault();

            if (ingate != null)
            {
                YardSpot oldSpot = db.YardSpots.Find(ingate.YardSpotId);
                //Poner como ocupado el spot nuevo, pero solo si esta libre (con status = 2)
                YardSpot newspot = db.YardSpots.Find(newSpot);
                if (newspot.SpotStatusId == 2)
                {
                    //Liberar spot actual
                    oldSpot.SpotStatusId = 2;
                    db.SaveChanges();

                    //Ocupar spot nuevo
                    newspot.SpotStatusId = 3;
                    db.SaveChanges();
                    
                    //Antes de cambiar el spot al ingate creamos un log
                    IngateLog logObj = new IngateLog
                    {
                        IngateId = ingate.Id,
                        OriginalAspNetUsersId = ingate.AspNetUsersId,
                        OriginalDateTime = ingate.DateTime,
                        RouteMasterId = ingate.RouteMasterId,
                        DriverId = ingate.DriverId,
                        TrailerStatusId = ingate.TrailerStatusId,
                        Bound = ingate.Bound,
                        ChangeTypeId = 1,
                        DateTime = DateTime.Now,
                        AspNetUsersId = user,
                        YardSpotId = ingate.YardSpotId
                    };

                    db.IngateLogs.Add(logObj);
                    db.SaveChanges();

                    //Cambiar el spot al ingate
                    ingate.YardSpotId = newspot.Id;
                    db.SaveChanges();
                    
                    //Antes de borrar dispatch creamos un log
                    Outgate outgate = (from o in db.Outgates
                                      where o.RouteMasterId ==  ingate.RouteMasterId
                                      select o).FirstOrDefault();

                    OutgateLog outgateLogObj = new OutgateLog
                    {
                        OutgateId = outgate.Id,
                        OriginalAspNetUsersId = outgate.AspNetUsersId,
                        OriginalDateTime = outgate.DateTime,
                        ChangeTypeId = 2,
                        DriverId = outgate.DriverId,
                        RouteMasterId = outgate.RouteMasterId,
                        DateTime = DateTime.Now,
                        AspNetUsersId = user,
                    };

                    db.OutgateLogs.Add(outgateLogObj);

                    if (db.SaveChanges() > 0)
                    {
                        //Borrar el dispatch
                        db.Outgates.Remove(outgate);
                        response = (db.SaveChanges() > 0);
                    }
                    return response;
                }
            }
            return response;
        }

        public List<YardStatu> getYardStatus() {
            SDSPEntities db = new SDSPEntities();
            List<YardStatu> lstYard = new List<YardStatu>();

            var getstatus = from y in db.YardStatus
                            orderby y.Id
                            select y;

            foreach (var item in getstatus){
                string comment = "";
                if (item.IngateId != null)
                {
                    var yardComments = (from y in db.YardComments
                                        where y.RouteMasterId == item.RouteMasterId
                                        orderby y.Id descending
                                        select y).FirstOrDefault();

                    var ingatecomment = (from c in db.Comments
                                         where c.IngateId == item.IngateId
                                         select c).FirstOrDefault();

                    comment = (yardComments != null) ? yardComments.Comment : ingatecomment.Comment1;
                }
                lstYard.Add(new YardStatu {
                    Id = item.Id,
                    IngateDateTime = item.IngateDateTime,
                    IngateId = item.IngateId,
                    Bound = item.Bound,
                    TrailerNumber = item.TrailerNumber,
                    Route = item.Route,
                    RouteMasterId = item.RouteMasterId,
                    Hazmat = item.Hazmat,
                    Status = item.Status,
                    SpotStatusId = item.SpotStatusId,
                    Priority = item.Priority,
                    Run= item.Run,
                    Comment = comment
                });
            }
            
            return lstYard;
        }

        public RouteMaster removePullAhead(long routeMasterId) {
            SDSPEntities db = new SDSPEntities();

            RouteMaster obj = db.RouteMasters.Find(routeMasterId);

            //Primero se revisar el numero de prioridad
            DateTime today = DateTime.Now.Date;
            int? maxPriority = (from t in db.RouteMasters
                                where
                                //t.PeriodId == period &&
                                (t.RegisterDate == today || t.PullAhead == today) &&
                                t.Priority > 0
                                orderby t.Priority descending
                                select t.Priority).FirstOrDefault();

            //Si no es la maxima prioridad se tienen que recorrer las prioridades para que no haya prioridades faltantes.
            if(obj.Priority < maxPriority)
            {
                var mayores = from r in db.RouteMasters
                              where
                              r.Priority > obj.Priority &&
                              r.RegisterDate == today
                              select r;

                foreach (var item in mayores) {
                    item.Priority = item.Priority - 1;
                    item.ExpressPriority = item.ExpressPriority - 1;
                    db.SaveChanges();
                }
            }

            //Se quita el pull ahead, y la prioridad
            obj.PullAhead = null;
            obj.Priority = null;
            obj.ExpressPriority = null;

            db.SaveChanges();

            return obj;
        }

        public List<RealTime_InTransit> forecastDplus3()
        {
            SDSPEntities db = new SDSPEntities();
            //int period = getCurrentPeriod();
            DateTime date = DateTime.Now.Date;

            var plusone = from i in db.InTransits
                            where
                            i.SystemUploadDate == date &&
                            i.FlagOrder == 2
                            select i;

            List<RealTime_InTransit> plusOneList = new List<RealTime_InTransit>();
            foreach (var item in plusone)
            {
                string routeRun = (item.Origin == "") ? item.Route : item.Route + "-" + item.Origin;
                //Buscamos la caja en RouteMaster
                var rmObj = (from r in db.RouteMasters
                            where r.RegisterDate == date &&
                            r.TrailerNumber == item.TrailerNumber &&
                            r.Route == item.Route
                            //r.Priority == 0
                            select r).FirstOrDefault();

                long id = 0;

                if (rmObj != null)
                {
                    id = (rmObj.Id > 0) ? rmObj.Id : 0;
                }

                RealTime_InTransit obj = new RealTime_InTransit
                {
                    Id = id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Origin,
                    RouteRun = routeRun,
                    Hazmat = item.Hazmat,
                };

                //Checamos si la caja ya esta en yarda en SD
                var ingate = (from y in db.YardStatus
                              where
                              y.TrailerNumber == item.TrailerNumber &&
                              y.Route == item.Route &&
                              y.Bound == 1
                              select y).FirstOrDefault();

                if (ingate != null) {
                    obj.ingateTime = ingate.IngateDateTime;
                    obj.ingatespot = ingate.YardSpotId;
                }
                plusOneList.Add(obj);
            }
            
            return plusOneList;
        }

        public List<RealTime_InTransit> forecastDplus4()
        {
            SDSPEntities db = new SDSPEntities();
            //int period = getCurrentPeriod();
            DateTime date = DateTime.Now.Date;

            var plustwo = from i in db.InTransits
                          where
                          i.SystemUploadDate == date &&
                          i.FlagOrder == 3
                          select i;

            List <RealTime_InTransit> plusTwoList = new List<RealTime_InTransit>();
            foreach (var item in plustwo)
            {
                string routeRun = (item.Origin == "") ? item.Route : item.Route + "-" + item.Origin;
                //Buscamos la caja en RouteMaster
                var rmObj = (from r in db.RouteMasters
                             where r.RegisterDate == date &&
                             r.TrailerNumber == item.TrailerNumber &&
                             r.Route == item.Route 
                             //r.Priority == 0
                             select r).FirstOrDefault();

                long id = 0;

                if (rmObj != null)
                {
                    id = (rmObj.Id > 0) ? rmObj.Id : 0;
                }
                RealTime_InTransit obj = new RealTime_InTransit
                {
                    Id = id,
                    TrailerNumber = item.TrailerNumber,
                    UnloadDate = item.UnloadDate,
                    Route = item.Route,
                    Run = item.Origin,
                    RouteRun = routeRun,
                    Hazmat = item.Hazmat,
                };

                //Checamos si la caja ya esta en yarda en SD
                var ingate = (from y in db.YardStatus
                              where
                              y.TrailerNumber == item.TrailerNumber &&
                              y.Route == item.Route &&
                              y.Bound == 1
                              select y).FirstOrDefault();

                if (ingate != null)
                {
                    obj.ingateTime = ingate.IngateDateTime;
                    obj.ingatespot = ingate.YardSpotId;
                }
                plusTwoList.Add(obj);
            }

            return plusTwoList;
        }

    }

}
