﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfSDSP
{

    public class TrailerHistory {
        public long? RouteMasterId { get; set; }
        public string Route { get; set; }
        public DateTime DateTime { get; set; }
        public String Driver { get; set; }
        public int? Spot { get; set; }
        public int Type { get; set; }
        public string TrailerStatus { get; set; }
        public int? Bound { get; set; }
        public string Hazmat { get; set; }
    }

    public class Inventory
    {
        public int Spot { get; set; }
        public string Status { get; set; }
        public string Bound { get; set; }
        public string TrailerNumber { get; set; }
        public string Route { get; set; }
        public string Run { get; set; }
        public DateTime ATA { get; set; }
        public int Priority { get; set; }
        public int Days { get; set; }
        public int Hours { get; set; }
    }

    public class SpotsVM
    {
        public int? yardSpotId { get; set; }
        public string trailerNumber { get; set; }
    }
    public class PrintDropVM
    {
        public Driver Driver { get; set; }
        public Company Company { get; set; }
        public Ingate Ingate { get; set; }
        public RouteMaster RouteMaster { get; set; }
    }
    public class IngateTrailersVM
    {
        public Ingate Ingate { get; set; }
        public RouteMaster RouteMaster { get; set; }
        public Comment Comment { get; set; }
    }

    public class PrintHookVM
    {
        public PreOutgate PreOutgate { get; set; }
        public Driver Driver { get; set; }
        public Company Company { get; set; }
        public Ingate Ingate { get; set; }
        public RouteMaster RouteMaster { get; set; }
    }

    public class PrintHooKTotalVM
    {
        public PrintHookVM PrintHookVM { get; set; }
        public PrintDropVM PrintDropVM { get; set; }
    }

    public class DriverCompanyVM
    {
        public Driver Driver { get; set; }
        public Company Company { get; set; }
    }

    public class ForecastVM {
        //public List<RouteMaster> current { get; set; }
        public List<RealTime_InTransit> current { get; set; }
        public List<RealTime_InTransit> plusone { get; set; }
        public List<RealTime_InTransit> plustwo { get; set; }
    }


    public class RealTimeInTransit {
        public List<RouteMaster> inTransit { get; set; }
        public List<Ingate> ingates { get; set; }
        public List<Outgate> outgates { get; set; }
        public List<PreOutgate> preOutgates { get; set; }
    }
}