﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfSDSP
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISDSPService" in both code and config file together.
    [ServiceContract]
    public interface ISDSPService
    {
        [OperationContract]
        List<IngatesReport> GetIngatesList(DateTime? date);

        [OperationContract]
        List<PreOutgate> GetPreoutgatesList();

        [OperationContract]
        long? SavePreOutGate(long? routeMasterId, long? driverId, string user);

        [OperationContract]
        long? SaveIngate(long? RouteMasterId, string AspNetUsersId, int? YardSpotId, long? DriverId, int? bound, int? trailerStatus);

        [OperationContract]
        List<DriverCompanyVM> GetDriverCompanyList(int type);

        [OperationContract]
        List<Company> GetCompaniesList();

        [OperationContract]
        Driver SaveDriver(string driverName, string companyId, string driverCode, string user);

        [OperationContract]
        Company SaveCompany(string companyName, string user);

        [OperationContract]
        bool SaveIngateComment(string comment, long? ingateId, string user);

        [OperationContract]
        RouteMaster getSingleRouteMasterData(long routeMasterId);

        [OperationContract]
        List<RouteMaster> getRouteMasterDatabyTrailerNumber(string trailer);

        [OperationContract]
        PrintHookVM printHook(long? preOutGateid);

        [OperationContract]
        PrintDropVM printDrop(long? ingateId);

        [OperationContract]
        List<IngateTrailersVM> GetIngateTrailers(int tipo);

        [OperationContract]
        List<PrintHookVM> GetPreoutgateTrailers();

        [OperationContract]
        List<RouteMaster> saveDispatches(IDictionary<string, int> trailers, string user);

        [OperationContract]
        long updateAssignedDriver(int preoutgateId, int newDriver, string user);

        [OperationContract]
        int saveInTransitBulk(DataTable inTransitData, string fileName, string user);

        [OperationContract]
        bool saveRouteMaster(DataTable routeMasterData, string fileName, string user);

        [OperationContract]
        int saveDTR(DataTable DTRData, string fileName, string user);
        [OperationContract]
        List<SpotsVM> saveBrokerFile(DataTable BrokerData, string fileName, string user);

        [OperationContract]
        long? saveManualIngate(string AspNetUsersId, int? YardSpotId, string hazmat, long? DriverId, string trailerNumber, string route, string sequence, string invoice, string comments, int? bound, int? trailerStatus);

        [OperationContract]
        bool blockspots(int spot, int status, string comment, string user);

        [OperationContract]
        List<YardSpot> getYardSpots(int status);

        [OperationContract]
        SpotsVM updateSingleInvoice(string invoice, long routeMasterId, string user);

        [OperationContract]
        string getReason(int spot);

        [OperationContract]
        List<PrintHookVM> getactiveIngates();

        [OperationContract]
        bool saveYardComment(string comment, long routeMasterId, string user);

        [OperationContract]
        List<YardComment> getYardComments(long routeMasterId);

        [OperationContract]
        string getDriverAssigned(long? preOutgateId);

        [OperationContract]
        List<RouteMaster> getExpected();

        [OperationContract]
        List<RouteMaster> AuditTrailers(DateTime start, DateTime end, int type);

        [OperationContract]
        List<IssueStatu> getIssueStatuses();

        [OperationContract]
        bool saveIssue(string issue, string people, string solution, int status, string user);

        [OperationContract]
        List<Issue> GetIssuesList();

        [OperationContract]
        Issue getIssueRecord(int id);

        [OperationContract]
        bool updateIssue(string issue, string people, string solution, int status, string user, int issueId);

        [OperationContract]
        List<RealTime_InTransit> getRouteMasterDatabyDate();

        [OperationContract]
        bool saveOrder(IDictionary<string, int> priorities);

        [OperationContract]
        bool savePriorityComment(int routeMasterId, string comment, string user);

        [OperationContract]
        bool checkNextPriority(int routeMasterId);

        [OperationContract]
        bool saveSkipReasons(int routeMasterId, string reason, string user);

        [OperationContract]
        ForecastVM getForecastData(DateTime date);

        [OperationContract]
        RouteMaster updatePullAhead(int routeMasterId);

        [OperationContract]
        List<RealTime_InTransit> getinTransitStatus();

        [OperationContract]
        int savePeriod(float periodNumber, DateTime startDate);

        [OperationContract]
        RouteMaster saveSingleInTransit(string trailer, string route, string origin, string unloadDate, string hazmat, string pullahead, string comments);

        [OperationContract]
        List<IngatesReport> IngatesReport(DateTime start, DateTime end);

        [OperationContract]
        List<OutgatesReport> OutgatesReport(DateTime start, DateTime end);

        [OperationContract]
        List<TrailerStatu> getTrailerStatus();

        [OperationContract]
        List<RouteMaster> searchTrailer(string trailer);

        [OperationContract]
        int deleteIngate(long? id, string user);

        [OperationContract]
        List<YardSpot> getYardSpotsbyIngate(int ingateYardSpot);

        [OperationContract]
        IngateTrailersVM getIngatebyId(long? ingateId);

        [OperationContract]
        List<PreoutgateReport> PreoutgatesReport();

        [OperationContract]
        int deleteHook(long? id, string user);

        [OperationContract]
        bool deleteDispatch(long? id, string user);

        [OperationContract]
        List<UploadedFile> getFiles(DateTime startDate, DateTime endDate);

        [OperationContract]
        bool deactivateDriver(long id, string user);

        [OperationContract]
        bool deactivateCompany(long id, string user);

        [OperationContract]
        Driver getDriverInfo(long id);

        [OperationContract]
        Company getCompanyInfo(long id);

        [OperationContract]
        bool editDriver(string name, string code, long id, int company, string user);

        [OperationContract]
        bool editCompany(string name, long id, string user);

        //[OperationContract]
        //int loadInventory(DataTable inventoryData, string fileName, string user);

        [OperationContract]
        List<TrailerHistory> getTrailerHistory(string trailerNumber, DateTime start, DateTime end);

        [OperationContract]
        Northbound getNorthInfo(long routeMasterId);

        [OperationContract]
        bool checkspot(int? YardSpotID);

        [OperationContract]
        bool changeSpot(long? routemasterid, int newSpot, string user);

        [OperationContract]
        List<YardStatu> getYardStatus();

        [OperationContract]
        bool changeDispatchSpot(long? routemasterid, int newSpot, string user);

        [OperationContract]        
        RouteMaster editInTransit(long routeMasterId, string trailer, string user);

        [OperationContract]
        RouteMaster simpleEditIntransit(long routeMasterId, string trailer, string route, string run, string user);

        [OperationContract]
        RouteMaster removePullAhead(long routeMasterId);

        [OperationContract]
        RouteMaster updateInTransit(long routeMasterId, string trailer, string user);

        [OperationContract]
        bool wasFileUploaded(int type);

        [OperationContract]
        List<RealTime_InTransit> forecastDplus3();

        [OperationContract]
        List<RealTime_InTransit> forecastDplus4();
    }
}
