//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfSDSP
{
    using System;
    using System.Collections.Generic;
    
    public partial class Broker
    {
        public long Id { get; set; }
        public string Route { get; set; }
        public string Sequence { get; set; }
        public Nullable<System.DateTime> UnloadDate { get; set; }
        public string Invoice { get; set; }
        public string TrailerNumber { get; set; }
    }
}
